/*
 * @license
 * CRUD, PWA and Synchronization tested
 * Copyright 2020 optima solution.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */
'use strict';

// Update cache names any time any of the cached files change.
const CACHE_NAME = 'static-cache-v3';
const DATA_CACHE_NAME = 'data-cache-v3';

// Add list of files to cache here.
const FILES_TO_CACHE = [
    '/',
    'scripts/app.js',
    'scripts/install.js',
    'styles/inline.css',
    'images/add.svg',
    'images/refresh.svg',
    'offline.html',
];

self.addEventListener('install', (evt) => {
  console.log('[ServiceWorker] Install');
  // Precache static resources here.
  evt.waitUntil(
      caches.open(CACHE_NAME).then((cache) => {
        console.log('[ServiceWorker] Pre-caching offline page');
        return cache.addAll(FILES_TO_CACHE);
      })
  );

  self.skipWaiting();
});

self.addEventListener('activate', (evt) => {
  console.log('[ServiceWorker] Activate');
  // Remove previous cached data from disk.
      evt.waitUntil(
        caches.keys().then((keyList) => {
          return Promise.all(keyList.map((key) => {
            if (key !== CACHE_NAME && key !== DATA_CACHE_NAME) {
              console.log('[ServiceWorker] Removing old cache', key);
              return caches.delete(key);
            }
          }));
        })
    );

  self.clients.claim();
});


self.addEventListener('fetch', (event) => {
  // Do not cache read.php dynamically. Network first for read.php and then save into cache with the updated version. 
    if(event.request.url.includes('read.php')||
    event.request.url.includes('index.php')||
    event.request.url.includes('welcome.php')||
    event.request.url.includes('md.php')||
    event.request.url.includes('about.php')||
    event.request.url.includes('message.php')||
    event.request.url.includes('product.php')||
    event.request.url.includes('productdetails.php')||
    event.request.url.includes('category_write.php')||
    event.request.url.includes('product_write.php')||
    event.request.url.includes('carousel_write.php')||
    event.request.url.includes('category_c.php')||
    event.request.url.includes('product_c.php')||
    event.request.url.includes('carousel_c.php')||   
    event.request.url.includes('registration.php')||
    event.request.url.includes('login.php')||
    event.request.url.includes('productdetails.php')||
    event.request.url.includes('logout.php')){
    // console.log('for read page only');
        event.respondWith(
            fetch(event.request).then(fetchRes=>{
                // console.log('fetch from network',event.request.url);
                let fetchResClone = fetchRes.clone();
                caches.open(DATA_CACHE_NAME).then((cache) => {
                    // console.log('delete cache:',event.request.url);
                    cache.delete(event.request.url,fetchResClone).then((fetchResCache)=>{
                    // console.log('caching ', event.request.url);
                    cache.put(event.request.url, fetchResClone);
                    })
                })
                return fetchRes;
            }).catch(function() {
                //   console.log('fetch from cache',event.request.url);
                  return caches.match(event.request);
                })
        )   
        return;
    }
    //for page other than read.php
    else{
        //   console.log('for other page');
          event.respondWith(
            caches.match(event.request).then((resp) => {
                // console.log('fetch handling for other pages');
              return resp || fetch(event.request).then((response) => {
                let responseClone = response.clone();
                caches.open(DATA_CACHE_NAME).then((cache) => {
                //   console.log('fetch even handling:',event.request,responseClone.url);
                  cache.put(event.request, responseClone);
                });
        
                return response;
              });
            }).catch(() => {
                // console.log('requested url not found', event.request.url);
                return caches.match('offline.html');
            })
          );
    }
 });


self.onsync = function(event) {
    if(event.tag == 'item-sync') {
      console.log('item syn request');
      event.waitUntil(syncIt('itemDB','itemTbl'));
    }
    else if(event.tag == 'product-sync'){
      // console.log('product syn request');
      event.waitUntil(syncIt('productDB','productTbl'));
    }
    else if(event.tag == 'category-sync'){
      console.log('category syn request');
      event.waitUntil(syncIt('categoryDB','categoryTbl'));
    }
    else if(event.tag == 'carousel-sync'){
      console.log('carousel syn request');
      event.waitUntil(syncIt('carouselDB','carouselTbl'));
    }

}

// down here are the other functions to go get the indexeddb data and also post to our server

function syncIt(vardb,tbl) {
    return getIndexedDB(vardb,tbl)
    .then((response)=>{
        // console.log('insert proocess');
        sendToServer(response,vardb,tbl);
      })
    .catch(function(err) {
        return err;
    })
}

function getIndexedDB(vardb,tbl) {
    return new Promise(function(resolve, reject) {
      // console.log('db:',vardb,' tbl: ',tbl);
        var db = indexedDB.open(vardb);
        db.onsuccess = function(event) {
            this.result.transaction(tbl).objectStore(tbl).getAll().onsuccess = function(event) {
                resolve(event.target.result);
            }
        }
        db.onerror = function(err) {
            reject(err);
        }
    });
}

function sendToServer(response,vardb,tbl) {
    // console.log('res',response,'db',vardb,'tbl',tbl,JSON.stringify(response));
    var urlink='';
    if(vardb=='productDB')
      {
        urlink='crud/product_c.php';
      }
    else if  (vardb=='categoryDB')
    {
        urlink='crud/category_c.php';   
      }
    else {
        urlink='crud/carousel_c.php';
      }

      console.log('url:',urlink);
    return fetch(urlink, {
      method: 'POST',
      headers: {
            "Content-type": "application/json; charset=UTF-8"
        },
      body: JSON.stringify(response)
    }).then(function(rez2) {
        return rez2.text();
    }).then(function(result){
      console.log('response from server:', result,' error:',result[0]);
      openData(vardb,tbl, result); // need to be restructured
      // openData('productDB','productTbl'); // need to be restructured
      // location.reload();
    }).catch(function(err) {
        return err;
    })
}

function openData(vardb,tbl, resulttxt){
// Let us open our database
var DBOpenRequest = indexedDB.open(vardb);
console.log('db opened',vardb,' tbl: ',tbl);
DBOpenRequest.onsuccess = function(event) {
  // note.innerHTML += '<li>Database initialised.</li>';
    
  // store the result of opening the database in the db variable.
  // This is used a lot below
  var db = DBOpenRequest.result;
  // console.log('preparing for clearing');  
  // Clear all the data form the object store

  // open a read/write db transaction, ready for clearing the data
  var transaction = db.transaction(tbl, "readwrite");

  // report on the success of the transaction completing, when everything is done
  transaction.oncomplete = function(event) {
    // note.innerHTML += '<li>Transaction completed.</li>';
  };

  transaction.onerror = function(event) {
    // note.innerHTML += '<li>Transaction not opened due to error: ' + transaction.error + '</li>';
  };

  // create an object store on the transaction
  var objectStore = transaction.objectStore(tbl);

  // Make a request to clear all the data out of the object store
  var objectStoreRequest = objectStore.clear();

  objectStoreRequest.onsuccess = function(event) {
    // report the success of our request
    // note.innerHTML += '<li>Request successful.</li>';
    console.log('db cleared');
    //start
  const channel = new BroadcastChannel('sw-messages');
  let txt = JSON.parse(resulttxt);
  // let txt2 = JSON.parse(txt);
  console.log(txt[0].msg)
  channel.postMessage(txt[0].msg);

    //end
  };
}
}