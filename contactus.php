<?php include 'header.php' ?>
<div class="row black center-align white-text">
    <h4>Contact us</h4>
    <div class="col l12 m12 s12">
        <h5>Gulshan Branch</h5><br/>
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3650.7367521562605!2d90.4136431144566!3d23.792386793089047!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3755c71297aaeb7d%3A0x7cce4c02d87c303e!2sPink%20City!5e0!3m2!1sen!2sbd!4v1586317010224!5m2!1sen!2sbd" width="500" height="500" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0">
        </iframe>
        <h6>Pink City (2nd Floor), <br/>Gulshan, Dhaka</h6><br/>
    </div>
    <div class="col l12 m12 s12">
        <h5>Jamuna Branch</h5><br/>
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3650.1651959785727!2d90.42080137716151!3d23.812724043386332!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3755c62fb95f16c1%3A0xb333248370356dee!2sJamuna%20Future%20Park!5e0!3m2!1sen!2sbd!4v1586317187198!5m2!1sen!2sbd" width="500" height="500" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0">
        </iframe>
        <h6>Jamuna Future Park (2nd Floor), <br> Bashundhara R/A, Dhaka</h6><br/>
    </div>
</div>
<script src="scripts/app.js"></script>
    <script src="scripts/install.js"></script>
    </main>

<?php include 'endfooter.php';?>