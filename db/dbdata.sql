-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 25, 2020 at 04:57 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

--
-- Database: `goldenworld`
--

--
-- Truncate table before insert `0_carousel`
--

TRUNCATE TABLE `0_carousel`;
--
-- Dumping data for table `0_carousel`
--

INSERT INTO `0_carousel` (`id`, `name`, `image`, `heading`, `description`, `created_at`, `pagename`, `updated_at`, `created_by`, `updated_by`, `status`) VALUES
(1, 'Branches of Golden World', 'Branches of Golden World.jpeg', 'Branches of Golden World', 'Branches of Golden World', '2020-07-20 22:23:42', 'home', NULL, '1', NULL, 'show'),
(2, 'Products of Golden World - 1 ', 'Products of Golden World - 1 .jpeg', 'Products of Golden World - 1 ', 'Products of Golden World - 1', '2020-07-20 22:24:51', 'home', NULL, '1', NULL, 'show'),
(3, 'Products of Golden World - 2', 'Products of Golden World - 2.jpeg', 'Products of Golden World - 2', 'Products of Golden World - 2', '2020-07-20 22:25:09', 'home', NULL, '1', NULL, 'show'),
(4, 'Jamuna Future Park Branch Opening', 'Jamuna Future Park Branch Opening.jpeg', 'Jamuna Future Park Branch Opening', 'Jamuna Future Park Branch Opening', '2020-07-20 22:26:18', 'home', NULL, '1', NULL, 'show'),
(5, 'Jamuna Future Park Branch Opening', 'Jamuna Future Park Branch Opening.jpeg', 'Jamuna Future Park Branch Opening', 'Jamuna Future Park Branch Opening', '2020-07-20 22:27:01', 'about', NULL, '1', NULL, 'show'),
(6, 'Gold Import License', 'Gold Import License.jpeg', 'Gold Import License', 'Gold Import License', '2020-07-20 22:28:02', 'about', NULL, '1', NULL, 'show'),
(7, 'Gold Import License', 'Gold Import License.jpeg', 'Gold Import License', 'Gold Import License', '2020-07-20 22:28:31', 'md', NULL, '1', NULL, 'show'),
(8, 'Womens day Mesage', 'Womens day Mesage.jpeg', 'Womens day Mesage', 'Womens day Mesage', '2020-07-20 22:29:06', 'md', NULL, '1', NULL, 'show'),
(9, 'Historical chart of gold price', 'Historical chart of gold price.png', 'Historical chart of gold price', 'Historical chart of gold price', '2020-07-20 22:29:46', 'message', NULL, '1', NULL, 'show'),
(10, 'History of gold price', 'History of gold price.png', 'History of gold price', 'History of gold price', '2020-07-20 22:30:02', 'message', NULL, '1', NULL, 'show');

--
-- Truncate table before insert `0_catagories`
--

TRUNCATE TABLE `0_catagories`;
--
-- Dumping data for table `0_catagories`
--

INSERT INTO `0_catagories` (`id`, `main_catagory_id`, `name`, `image`, `description`, `created_at`, `updated_at`) VALUES
(1, 1, 'Ear Ring', 'Ear Ring_cat.jpeg', 'Ear Ring', '2020-07-20 22:31:27', '2020-07-20 22:31:27'),
(2, 1, 'Ring', 'Ring_cat.jpeg', 'Ring', '2020-07-20 22:31:56', '2020-07-20 22:31:56'),
(3, 1, 'Bange', 'Bange_cat.jpeg', 'Bange', '2020-07-21 01:17:26', '2020-07-21 01:17:26'),
(4, 1, 'Locket', 'Locket_cat.jpeg', 'Locket', '2020-07-20 22:33:06', '2020-07-20 22:33:06'),
(5, 1, 'Necklace', 'Necklace_cat.jpeg', 'Necklace', '2020-07-20 22:33:32', '2020-07-20 22:33:32'),
(6, -1, 'Engagement', 'Engagement_cat.jpeg', 'Engagement', '2020-07-20 22:35:44', '2020-07-20 22:35:44'),
(7, -1, 'Festivals', 'Festivals_cat.jpeg', 'Festivals', '2020-07-20 22:35:55', '2020-07-20 22:35:55'),
(8, -1, 'Wedding', 'Wedding_cat.jpeg', 'Wedding', '2020-07-20 22:36:08', '2020-07-20 22:36:08'),
(9, -1, 'Parties', 'Parties_cat.jpeg', 'Parties', '2020-07-20 22:36:19', '2020-07-20 22:36:19'),
(10, -1, 'Trendz', 'Trendz_cat.jpeg', 'Trendz', '2020-07-20 22:36:42', '2020-07-20 22:36:42'),
(11, 1, 'Nose Pin', 'Nose Pin_cat.jpeg', 'Nose Pin', '2020-07-21 01:21:35', '2020-07-21 01:21:35');

--
-- Truncate table before insert `0_products`
--

TRUNCATE TABLE `0_products`;
--
-- Truncate table before insert `0_ratings`
--

TRUNCATE TABLE `0_ratings`;
--
-- Truncate table before insert `0_users`
--

TRUNCATE TABLE `0_users`;
--
-- Dumping data for table `0_users`
--

INSERT INTO `0_users` (`id`, `name`, `email`, `role`, `website`, `facebook`, `image`, `phone`, `address`, `location`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`, `birth_day`, `marriage_day`, `auth_token`) VALUES
(1, '      rashedul islam', 'rashed@goldenworldltd.com', 'admin', '      www.blilbd.com', '      fb/masum', '01911491237.jpg', ' 01911491237', '      Aftabnagar, Dhaka', '      Dhaka', '0000-00-00 00:00:00', 'e10adc3949ba59abbe56e057f20f883e', 'rashed@goldenworldltd.com', NULL, '2020-07-19 10:30:59', '1979-11-21', '2020-06-19', 'Qm1bMnituBzk');

--
-- Truncate table before insert `addresses`
--

TRUNCATE TABLE `addresses`;
--
-- Truncate table before insert `districts`
--

TRUNCATE TABLE `districts`;
--
-- Truncate table before insert `measurment_units`
--

TRUNCATE TABLE `measurment_units`;
--
-- Truncate table before insert `orders`
--

TRUNCATE TABLE `orders`;
--
-- Truncate table before insert `order_details`
--

TRUNCATE TABLE `order_details`;
--
-- Truncate table before insert `order_statuses`
--

TRUNCATE TABLE `order_statuses`;
--
-- Truncate table before insert `password_resets`
--

TRUNCATE TABLE `password_resets`;
--
-- Truncate table before insert `payment_methods`
--

TRUNCATE TABLE `payment_methods`;
--
-- Truncate table before insert `product_variations`
--

TRUNCATE TABLE `product_variations`;
--
-- Truncate table before insert `transections`
--

TRUNCATE TABLE `transections`;COMMIT;
