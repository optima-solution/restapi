DROP TABLE IF EXISTS `0_chart_class`;
CREATE TABLE `0_chart_class` (
  `cid` int(10) NOT NULL,
  `class_name` varchar(60) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `inactive` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


INSERT INTO `0_chart_class` (`cid`, `class_name`, `inactive`) VALUES
(1, 'Assets', 0),
(2, 'Liabilities', 0),
(3, 'Income', 0),
(4, 'Costs', 0);

ALTER TABLE `0_chart_class`
  ADD PRIMARY KEY (`cid`);

ALTER TABLE `0_chart_class`
  MODIFY `cid` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
COMMIT;

DROP TABLE IF EXISTS `0_chart_master`;
CREATE TABLE `0_chart_master` (
  `account_code` int(15) NOT NULL,
  `account_code2` varchar(15) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `account_name` varchar(60) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `account_type` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `inactive` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


ALTER TABLE `0_chart_master`
  ADD PRIMARY KEY (`account_code`);


ALTER TABLE `0_chart_master`
  MODIFY `account_code` int(15) NOT NULL AUTO_INCREMENT;


DROP TABLE IF EXISTS `0_chart_types`;
CREATE TABLE `0_chart_types` (
  `id` int(10) NOT NULL,
  `name` varchar(60) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `class_id` varchar(3) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `parent` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT '-1',
  `inactive` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


ALTER TABLE `0_chart_types`
  ADD PRIMARY KEY (`id`);


ALTER TABLE `0_chart_types`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
COMMIT;



DROP TABLE IF EXISTS 0_journal;
CREATE TABLE `0_journal` (
  `jid` int(10) NOT NULL,
  `account_code` int(10) NOT NULL,
  `debit` decimal(15,2) NOT NULL,
  `credit` decimal(15,2) NOT NULL,
  `transaction_ref` int(10) NOT NULL,
  `transaction_date` datetime NOT NULL,
  `particulars` varchar(300) NOT NULL,
  `Journal_status` int(11) NOT NULL,
  `branch` int(11) NOT NULL,
  `posted_by` int(11) NOT NULL,
  `posting_date` datetime NOT NULL,
  `checked_by` int(11) NOT NULL,
  `check_date` datetime NOT NULL,
  `approved_by` int(11) NOT NULL,
  `approved_date` datetime NOT NULL,
  `approval_status` int(11) NOT NULL,
  `doc_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


ALTER TABLE `0_journal`
  ADD PRIMARY KEY (`jid`);

ALTER TABLE `0_journal`
  MODIFY `jid` int(10) NOT NULL AUTO_INCREMENT;
COMMIT;


DROP TABLE IF EXISTS 0_gl;
CREATE TABLE `0_gl` (
  `gid` int(10) NOT NULL,
  `total_debit` decimal(15,2) NOT NULL,
  `total_credit` decimal(15,2) NOT NULL,
  `transaction_ref` int(10) NOT NULL,
  `transaction_date` datetime NOT NULL,
  `Memo` varchar(300) NOT NULL,
  `gl_status` int(11) NOT NULL,
  `branch` int(11) NOT NULL,
  `posted_by` int(11) NOT NULL,
  `posting_date` datetime NOT NULL,
  `checked_by` int(11) NOT NULL,
  `check_date` datetime NOT NULL,
  `approved_by` int(11) NOT NULL,
  `approved_date` datetime NOT NULL,
  `approval_status` int(11) NOT NULL,
  `doc_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


ALTER TABLE `0_gl`
  ADD PRIMARY KEY (`gid`);

ALTER TABLE `0_gl`
  MODIFY `gid` int(10) NOT NULL AUTO_INCREMENT;
COMMIT;