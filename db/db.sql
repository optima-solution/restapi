-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 21, 2021 at 03:56 PM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.1.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

--
-- Database: `goldenworld_ecommerce`
--
CREATE DATABASE IF NOT EXISTS `goldenworld_ecommerce` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `goldenworld_ecommerce`;

-- --------------------------------------------------------

--
-- Table structure for table `0_carousel`
--

DROP TABLE IF EXISTS `0_carousel`;
CREATE TABLE `0_carousel` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `heading` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `pagename` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updated_by` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `0_carousel`
--

INSERT INTO `0_carousel` (`id`, `name`, `image`, `heading`, `description`, `created_at`, `pagename`, `updated_at`, `created_by`, `updated_by`, `status`) VALUES
(1, 'Jamuna Future Park outside', 'Jamuna Future Park outside.jpeg', 'Jamuna Future Park outside', 'Jamuna Future Park outside', '2020-07-20 22:23:42', 'home', '2020-07-28 21:27:56', '1', '1', 'show'),
(2, 'Beauty with Golden World', 'Products of Golden World - 1 .jpeg', 'Beauty with Golden World', 'Beauty with Golden World', '2020-07-20 22:24:51', 'home', '2020-07-28 20:14:20', '1', '1', 'show'),
(3, 'Pink City Inside', 'Pink City Inside.jpeg', 'Pink City Inside', 'Pink City Inside', '2020-07-20 22:25:09', 'home', '2020-07-28 21:24:20', '1', '1', 'show'),
(4, 'Jamuna Future Park Branch Opening', 'Jamuna Future Park Branch Opening.jpeg', 'Jamuna Future Park Branch Opening', 'Jamuna Future Park Branch Opening', '2020-07-20 22:26:18', 'home', NULL, '1', NULL, 'show'),
(5, 'Jamuna Future Park Branch Opening', 'Jamuna Future Park Branch Opening.jpeg', 'Jamuna Future Park Branch Opening', 'Jamuna Future Park Branch Opening', '2020-07-20 22:27:01', 'about', NULL, '1', NULL, 'show'),
(6, 'Gold Import License', 'Gold Import License.jpeg', 'Gold Import License', 'Gold Import License', '2020-07-20 22:28:02', 'about', NULL, '1', NULL, 'show'),
(7, 'Gold Import License', 'Gold Import License.jpeg', 'Gold Import License', 'Gold Import License', '2020-07-20 22:28:31', 'md', NULL, '1', NULL, 'show'),
(8, 'Womens day Mesage', 'Womens day Mesage.jpeg', 'Womens day Mesage', 'Womens day Mesage', '2020-07-20 22:29:06', 'md', NULL, '1', NULL, 'show'),
(9, 'Historical chart of gold price', 'Historical chart of gold price.png', 'Historical chart of gold price', 'Historical chart of gold price', '2020-07-20 22:29:46', 'message', NULL, '1', NULL, 'show'),
(10, 'History of gold price', 'History of gold price.png', 'History of gold price', 'History of gold price', '2020-07-20 22:30:02', 'message', NULL, '1', NULL, 'show'),
(11, 'MD', 'MD.jpeg', 'Managing Director', 'Managing Director', '2020-07-25 05:29:11', 'message', NULL, '1', NULL, 'show'),
(12, 'Golden World Family', 'Golden World Family.jpeg', 'Golden World Family', 'Golden World Family', '2020-07-28 20:08:04', 'home', '2020-07-28 20:12:06', '1', '1', 'show'),
(13, 'Reception with Golden World', 'Reception with Golden World.jpeg', 'Reception with Golden World', 'Reception with Golden World', '2020-07-28 20:08:43', 'home', NULL, '1', NULL, 'show'),
(14, 'Pink City Outside', 'Pink City Outside.jpeg', 'Pink City Outside', 'Pink City Outside', '2020-07-28 21:19:47', 'home', '2020-07-28 21:30:05', '1', '1', 'show'),
(15, 'Jamuna Future Park', 'Jamuna Future Park.jpeg', 'Jamuna Future Park', 'Jamuna Future Park', '2020-07-28 21:20:23', 'home', '2020-07-28 21:29:04', '1', '1', 'show'),
(16, 'Eid Ul Azha', 'Eid-ul-Azha.jpeg', 'Eid Ul Azha', 'Eid Ul Azha', '2020-07-28 21:20:23', 'home', '2020-07-28 21:29:04', '1', '1', 'show');

-- --------------------------------------------------------

--
-- Table structure for table `0_catagories`
--

DROP TABLE IF EXISTS `0_catagories`;
CREATE TABLE `0_catagories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `main_catagory_id` bigint(20) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `0_catagories`
--

INSERT INTO `0_catagories` (`id`, `main_catagory_id`, `name`, `image`, `description`, `created_at`, `updated_at`) VALUES
(1, 1, 'Ear Ring', 'Ear Ring_cat.jpeg', 'Ear Ring', '2020-07-20 16:31:27', '2020-07-20 16:31:27'),
(2, 1, 'Ring', 'Ring_cat.jpeg', 'Ring', '2020-07-20 16:31:56', '2020-07-20 16:31:56'),
(3, 1, 'Bange', 'Bange_cat.jpeg', 'Bange', '2020-07-20 19:17:26', '2020-07-20 19:17:26'),
(4, 1, 'Locket', 'Locket_cat.jpeg', 'Locket', '2020-07-20 16:33:06', '2020-07-20 16:33:06'),
(5, 1, 'Necklace', 'Necklace_cat.jpeg', 'Necklace', '2020-07-20 16:33:32', '2020-07-20 16:33:32'),
(6, -1, 'Engagement', 'Engagement_cat.jpeg', 'Engagement', '2020-07-20 16:35:44', '2020-07-20 16:35:44'),
(7, -1, 'Festivals', 'Festivals_cat.jpeg', 'Festivals', '2020-07-20 16:35:55', '2020-07-20 16:35:55'),
(8, -1, 'Wedding', 'Wedding_cat.jpeg', 'Wedding', '2020-07-20 16:36:08', '2020-07-20 16:36:08'),
(9, -1, 'Parties', 'Parties_cat.jpeg', 'Parties', '2020-07-20 16:36:19', '2020-07-20 16:36:19'),
(10, -1, 'Trendz', 'Trendz_cat.jpeg', 'Trendz', '2020-07-20 16:36:42', '2020-07-20 16:36:42'),
(11, 1, 'Nose Pin', 'Nose Pin_cat.jpeg', 'Nose Pin', '2020-07-20 19:21:35', '2020-07-20 19:21:35'),
(12, 1, 'Taj', 'Taj_cat.jpeg', 'Taj', '2021-04-09 02:32:32', '2021-04-09 02:32:32');

-- --------------------------------------------------------

--
-- Table structure for table `0_chatbot`
--

DROP TABLE IF EXISTS `0_chatbot`;
CREATE TABLE `0_chatbot` (
  `id` int(10) NOT NULL,
  `userid` int(10) NOT NULL,
  `queries` text DEFAULT NULL,
  `replies` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `0_chatbot`
--

INSERT INTO `0_chatbot` (`id`, `userid`, `queries`, `replies`) VALUES
(1, 1, 'Test', 'Replied');

-- --------------------------------------------------------

--
-- Table structure for table `0_chathistory`
--

DROP TABLE IF EXISTS `0_chathistory`;
CREATE TABLE `0_chathistory` (
  `id` int(10) NOT NULL,
  `userid` int(10) DEFAULT NULL,
  `auth_token` varchar(10) NOT NULL,
  `queries` text DEFAULT NULL,
  `replies` text DEFAULT NULL,
  `name_cell` varchar(300) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `0_goldprice`
--

DROP TABLE IF EXISTS `0_goldprice`;
CREATE TABLE `0_goldprice` (
  `id` int(10) NOT NULL,
  `karat` varchar(20) DEFAULT NULL,
  `price` decimal(20,2) DEFAULT NULL,
  `created_at` date DEFAULT NULL,
  `updated_at` date DEFAULT NULL,
  `updated_by` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `0_goldprice`
--

INSERT INTO `0_goldprice` (`id`, `karat`, `price`, `created_at`, `updated_at`, `updated_by`) VALUES
(1, '18kt', '5220.00', NULL, NULL, NULL),
(2, '21kt', '5970.00', NULL, NULL, NULL),
(3, '22kt', '6240.00', NULL, NULL, NULL),
(4, 'sonation', '4335.00', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `0_products`
--

DROP TABLE IF EXISTS `0_products`;
CREATE TABLE `0_products` (
  `id` int(20) NOT NULL,
  `code` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `product_type` int(11) NOT NULL,
  `category` int(10) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `original_price` decimal(15,0) NOT NULL,
  `discount_amt` decimal(15,0) NOT NULL,
  `discount_per` float NOT NULL,
  `net_price` decimal(15,0) NOT NULL,
  `status` varchar(255) DEFAULT NULL,
  `image1` varchar(255) DEFAULT NULL,
  `image2` varchar(255) DEFAULT NULL,
  `image3` varchar(255) DEFAULT NULL,
  `image4` varchar(255) DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `creation_date` datetime DEFAULT NULL,
  `last_updated_by` varchar(255) DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `alt_category` int(10) DEFAULT NULL,
  `purchase_price` decimal(15,2) DEFAULT NULL,
  `other_cost` decimal(15,2) DEFAULT NULL,
  `current_stock` int(10) DEFAULT NULL,
  `is_display` tinyint(1) DEFAULT NULL,
  `display_order` int(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `0_products`
--

INSERT INTO `0_products` (`id`, `code`, `name`, `product_type`, `category`, `image`, `description`, `original_price`, `discount_amt`, `discount_per`, `net_price`, `status`, `image1`, `image2`, `image3`, `image4`, `created_by`, `creation_date`, `last_updated_by`, `last_update_date`, `alt_category`, `purchase_price`, `other_cost`, `current_stock`, `is_display`, `display_order`) VALUES
(1, 'NKL 10001', 'Necklace', 1, 5, '1_0.jpeg', 'Necklace', '0', '0', 0, '0', '1', 'noimg.png', 'noimg.png', 'noimg.png', 'noimg.png', 'Admin', '2020-07-29 02:34:29', 'Admin', '2020-10-26 08:08:20', 6, NULL, NULL, NULL, 1, 10),
(2, 'NKL 10002', 'Necklace', 1, 5, '2_0.jpeg', 'Necklace', '0', '0', 0, '0', '1', 'noimg.png', 'noimg.png', 'noimg.png', 'noimg.png', 'Admin', '2020-07-29 02:35:39', 'Admin', '2020-07-29 02:35:39', 6, NULL, NULL, NULL, 1, NULL),
(3, 'NKL 10003', 'Necklace', 1, 5, '3_0.jpeg', 'Necklace', '0', '0', 0, '0', '1', 'noimg.png', 'noimg.png', 'noimg.png', 'noimg.png', 'Admin', '2020-07-29 02:36:15', 'Admin', '2020-10-26 05:45:02', 7, NULL, NULL, NULL, 1, 1),
(4, 'NKL 10004', 'Necklace', 1, 5, '4_0.jpeg', 'Necklace', '0', '0', 0, '0', '1', 'noimg.png', 'noimg.png', 'noimg.png', 'noimg.png', 'Admin', '2020-07-29 02:36:45', 'Admin', '2020-10-26 05:42:58', 8, NULL, NULL, NULL, 1, 0),
(5, 'RNG 10001', 'Ring', 2, 2, '5_0.jpeg', '', '0', '0', 0, '0', '1', 'noimg.png', 'noimg.png', 'noimg.png', 'noimg.png', 'Admin', '2020-07-29 03:35:15', 'Admin', '2020-07-29 03:35:15', 6, NULL, NULL, NULL, 1, NULL),
(6, 'LKT 10001', 'Locket', 1, 4, '6_0.jpeg', 'Locket', '0', '0', 0, '0', '1', 'noimg.png', 'noimg.png', 'noimg.png', 'noimg.png', 'Admin', '2020-07-29 03:37:40', 'Admin', '2020-07-29 03:37:40', 7, NULL, NULL, NULL, 1, NULL),
(7, 'ERN 10001', 'Ear Ring', 1, 1, '7_0.jpeg', 'Ear Ring', '0', '0', 0, '0', '1', 'noimg.png', 'noimg.png', 'noimg.png', 'noimg.png', 'Admin', '2020-07-29 03:39:21', 'Admin', '2020-07-29 03:39:21', 9, NULL, NULL, NULL, 1, NULL),
(8, 'BCL10001', 'Bracelet', 1, 3, '8_0.jpeg', 'Bracelet', '0', '0', 0, '0', '1', 'noimg.png', 'noimg.png', 'noimg.png', 'noimg.png', 'Admin', '2020-07-31 06:44:16', 'Admin', '2020-07-31 06:44:16', 7, NULL, NULL, NULL, 1, NULL),
(9, 'BCL10001', 'Bracelet', 1, 3, '9_0.jpeg', 'Bracelet', '0', '0', 0, '0', '1', 'noimg.png', 'noimg.png', 'noimg.png', 'noimg.png', 'Admin', '2020-07-31 06:44:35', 'Admin', '2020-07-31 06:44:35', 7, NULL, NULL, NULL, 1, NULL),
(10, 'BCL10001', 'Bracelet', 1, 3, '10_0.jpeg', 'Bracelet', '0', '0', 0, '0', '1', 'noimg.png', 'noimg.png', 'noimg.png', 'noimg.png', 'Admin', '2020-07-31 06:44:49', 'Admin', '2020-07-31 06:44:49', 7, NULL, NULL, NULL, 1, NULL),
(11, 'BCL10001', 'Bracelet', 1, 3, '11_0.jpeg', 'Bracelet', '0', '0', 0, '0', '1', 'noimg.png', 'noimg.png', 'noimg.png', 'noimg.png', 'Admin', '2020-07-31 06:44:59', 'Admin', '2020-07-31 06:44:59', 7, NULL, NULL, NULL, 1, NULL),
(12, 'BCL10001', 'Bracelet', 1, 3, '12_0.jpeg', 'Bracelet', '0', '0', 0, '0', '1', 'noimg.png', 'noimg.png', 'noimg.png', 'noimg.png', 'Admin', '2020-07-31 06:45:19', 'Admin', '2020-07-31 06:45:19', 7, NULL, NULL, NULL, 1, NULL),
(13, 'BCL10001', 'Bracelet', 1, 3, '13_0.jpeg', 'Bracelet', '0', '0', 0, '0', '1', 'noimg.png', 'noimg.png', 'noimg.png', 'noimg.png', 'Admin', '2020-07-31 06:45:35', 'Admin', '2020-07-31 06:45:35', 7, NULL, NULL, NULL, 1, NULL),
(14, 'BCL10001', 'Bracelet', 1, 3, '14_0.jpeg', 'Bracelet', '0', '0', 0, '0', '1', 'noimg.png', 'noimg.png', 'noimg.png', 'noimg.png', 'Admin', '2020-07-31 06:45:44', 'Admin', '2020-07-31 06:45:44', 7, NULL, NULL, NULL, 1, NULL),
(15, 'BCL10001', 'Bracelet', 1, 3, '15_0.jpeg', 'Bracelet', '0', '0', 0, '0', '1', 'noimg.png', 'noimg.png', 'noimg.png', 'noimg.png', 'Admin', '2020-07-31 06:45:55', 'Admin', '2020-07-31 06:45:55', 7, NULL, NULL, NULL, 1, NULL),
(16, 'BCL10001', 'Bracelet', 1, 3, '16_0.jpeg', 'Bracelet', '0', '0', 0, '0', '1', 'noimg.png', 'noimg.png', 'noimg.png', 'noimg.png', 'Admin', '2020-07-31 06:46:06', 'Admin', '2020-07-31 06:46:06', 7, NULL, NULL, NULL, 1, NULL),
(17, 'BCL10001', 'Bracelet', 1, 3, '17_0.jpeg', 'Bracelet', '0', '0', 0, '0', '1', 'noimg.png', 'noimg.png', 'noimg.png', 'noimg.png', 'Admin', '2020-07-31 06:46:21', 'Admin', '2020-07-31 06:46:21', 7, NULL, NULL, NULL, 1, NULL),
(18, 'BCL10001', 'Bracelet', 1, 3, '18_0.jpeg', 'Bracelet', '0', '0', 0, '0', '1', 'noimg.png', 'noimg.png', 'noimg.png', 'noimg.png', 'Admin', '2020-07-31 06:46:29', 'Admin', '2020-07-31 06:46:29', 7, NULL, NULL, NULL, 1, NULL),
(19, 'BCL10001', 'Bracelet', 1, 3, '19_0.jpeg', 'Bracelet', '0', '0', 0, '0', '1', 'noimg.png', 'noimg.png', 'noimg.png', 'noimg.png', 'Admin', '2020-07-31 06:46:37', 'Admin', '2020-07-31 06:46:37', 7, NULL, NULL, NULL, 1, NULL),
(20, 'BCL10001', 'Bracelet', 1, 3, '20_0.jpeg', 'Bracelet', '0', '0', 0, '0', '1', 'noimg.png', 'noimg.png', 'noimg.png', 'noimg.png', 'Admin', '2020-07-31 06:46:45', 'Admin', '2020-07-31 06:46:45', 7, NULL, NULL, NULL, 1, NULL),
(21, 'BCL10001', 'Bracelet', 1, 3, '21_0.jpeg', 'Bracelet', '0', '0', 0, '0', '1', 'noimg.png', 'noimg.png', 'noimg.png', 'noimg.png', 'Admin', '2020-07-31 06:46:53', 'Admin', '2020-07-31 06:46:53', 7, NULL, NULL, NULL, 1, NULL),
(22, 'BCL10001', 'Bracelet', 1, 3, '22_0.jpeg', 'Bracelet', '0', '0', 0, '0', '1', 'noimg.png', 'noimg.png', 'noimg.png', 'noimg.png', 'Admin', '2020-07-31 06:46:59', 'Admin', '2020-07-31 06:46:59', 7, NULL, NULL, NULL, 1, NULL),
(23, 'BCL10001', 'Bracelet', 1, 3, '23_0.jpeg', 'Bracelet', '0', '0', 0, '0', '1', 'noimg.png', 'noimg.png', 'noimg.png', 'noimg.png', 'Admin', '2020-07-31 06:47:21', 'Admin', '2020-07-31 06:47:21', 7, NULL, NULL, NULL, 1, NULL),
(24, 'BCL10001', 'Bracelet', 1, 3, '24_0.jpeg', 'Bracelet', '0', '0', 0, '0', '1', 'noimg.png', 'noimg.png', 'noimg.png', 'noimg.png', 'Admin', '2020-07-31 06:47:37', 'Admin', '2020-07-31 06:47:37', 7, NULL, NULL, NULL, 1, NULL),
(25, 'BCL10001', 'Bracelet', 1, 3, '25_0.jpeg', 'Bracelet', '0', '0', 0, '0', '1', 'noimg.png', 'noimg.png', 'noimg.png', 'noimg.png', 'Admin', '2020-07-31 06:47:52', 'Admin', '2020-07-31 06:47:52', 7, NULL, NULL, NULL, 1, NULL),
(26, 'RNG', 'RING', 1, 2, '26_0.jpeg', 'RING', '0', '0', 0, '0', '1', 'noimg.png', 'noimg.png', 'noimg.png', 'noimg.png', 'Admin', '2020-07-31 06:53:05', 'Admin', '2020-07-31 06:53:05', 8, NULL, NULL, NULL, 1, NULL),
(27, 'RNG', 'RING', 1, 2, '27_0.jpeg', 'RING', '0', '0', 0, '0', '1', 'noimg.png', 'noimg.png', 'noimg.png', 'noimg.png', 'Admin', '2020-07-31 06:53:24', 'Admin', '2020-07-31 06:53:24', 8, NULL, NULL, NULL, 1, NULL),
(28, 'RNG', 'RING', 1, 2, '28_0.jpeg', 'RING', '0', '0', 0, '0', '1', 'noimg.png', 'noimg.png', 'noimg.png', 'noimg.png', 'Admin', '2020-07-31 06:53:41', 'Admin', '2020-07-31 06:53:41', 8, NULL, NULL, NULL, 1, NULL),
(29, 'RNG', 'RING', 1, 2, '29_0.jpeg', 'RING', '0', '0', 0, '0', '1', 'noimg.png', 'noimg.png', 'noimg.png', 'noimg.png', 'Admin', '2020-07-31 06:53:54', 'Admin', '2020-07-31 06:53:54', 8, NULL, NULL, NULL, 1, NULL),
(30, 'RNG', 'RING', 1, 2, '30_0.jpeg', 'RING', '0', '0', 0, '0', '1', 'noimg.png', 'noimg.png', 'noimg.png', 'noimg.png', 'Admin', '2020-07-31 06:54:04', 'Admin', '2020-07-31 06:54:04', 8, NULL, NULL, NULL, 1, NULL),
(31, 'RNG', 'RING', 1, 2, '31_0.jpeg', 'RING', '0', '0', 0, '0', '1', 'noimg.png', 'noimg.png', 'noimg.png', 'noimg.png', 'Admin', '2020-07-31 06:54:15', 'Admin', '2020-07-31 06:54:15', 8, NULL, NULL, NULL, 1, NULL),
(32, 'RNG', 'RING', 1, 2, '32_0.jpeg', 'RING', '0', '0', 0, '0', '1', 'noimg.png', 'noimg.png', 'noimg.png', 'noimg.png', 'Admin', '2020-07-31 06:54:28', 'Admin', '2020-07-31 06:54:28', 8, NULL, NULL, NULL, 1, NULL),
(33, 'RNG', 'RING', 1, 2, '33_0.jpeg', 'RING', '0', '0', 0, '0', '1', 'noimg.png', 'noimg.png', 'noimg.png', 'noimg.png', 'Admin', '2020-07-31 06:54:40', 'Admin', '2020-07-31 06:54:40', 8, NULL, NULL, NULL, 1, NULL),
(34, 'RNG', 'RING', 1, 2, '34_0.jpeg', 'RING', '0', '0', 0, '0', '1', 'noimg.png', 'noimg.png', 'noimg.png', 'noimg.png', 'Admin', '2020-07-31 06:54:49', 'Admin', '2020-07-31 06:54:49', 8, NULL, NULL, NULL, 1, NULL),
(35, 'RNG', 'RING', 1, 2, '35_0.jpeg', 'RING', '0', '0', 0, '0', '1', 'noimg.png', 'noimg.png', 'noimg.png', 'noimg.png', 'Admin', '2020-07-31 06:55:00', 'Admin', '2020-07-31 06:55:00', 8, NULL, NULL, NULL, 1, NULL),
(36, 'nkl123', 'Necklace', 1, 5, '36_0.jpeg', 'Antique set', '0', '0', 0, '0', '3', 'noimg.png', 'noimg.png', 'noimg.png', 'noimg.png', 'Admin', '2020-10-26 09:23:32', 'Admin', '2020-10-26 10:04:07', 8, NULL, NULL, NULL, 1, 0),
(37, '0', '0', 1, 5, '37_0.jpeg', 'Bridal Necklace', '0', '0', 0, '0', '1', 'noimg.png', 'noimg.png', 'noimg.png', 'noimg.png', 'Admin', '2020-10-26 09:40:33', 'Admin', '2020-10-26 10:08:00', 8, NULL, NULL, NULL, 0, 8),
(38, '0', '0', 1, 5, '38_0.jpeg', 'Bridal Necklace', '0', '0', 0, '0', '3', 'noimg.png', 'noimg.png', 'noimg.png', 'noimg.png', 'Admin', '2020-10-26 09:44:44', 'Admin', '2020-10-26 10:09:35', 8, NULL, NULL, NULL, 0, 8),
(39, 'NKL 1212', 'Necklace', 1, 5, '39_0.jpeg', 'Bridal Necklace', '0', '0', 0, '0', '3', 'noimg.png', 'noimg.png', 'noimg.png', 'noimg.png', 'Admin', '2020-10-26 09:44:54', 'Admin', '2020-10-26 10:04:59', 8, NULL, NULL, NULL, 1, 8),
(40, '0', '0', 1, 5, 'noimg.png', 'Bridal Necklace', '0', '0', 0, '0', '1', 'noimg.png', 'noimg.png', 'noimg.png', 'noimg.png', 'Admin', '2020-10-26 10:07:44', 'Admin', '2020-10-26 10:07:44', 8, NULL, NULL, NULL, 0, 8),
(41, '0', '0', 1, 5, 'noimg.png', 'Bridal Necklace', '0', '0', 0, '0', '1', 'noimg.png', 'noimg.png', 'noimg.png', 'noimg.png', 'Admin', '2020-10-26 10:07:48', 'Admin', '2020-10-26 10:07:48', 8, NULL, NULL, NULL, 0, 8),
(42, '0', '0', 1, 5, 'noimg.png', 'Bridal Necklace', '0', '0', 0, '0', '1', 'noimg.png', 'noimg.png', 'noimg.png', 'noimg.png', 'Admin', '2020-10-26 10:07:48', 'Admin', '2020-10-26 10:07:48', 8, NULL, NULL, NULL, 0, 8),
(43, '0', '0', 1, 5, 'noimg.png', 'Bridal Necklace', '0', '0', 0, '0', '1', 'noimg.png', 'noimg.png', 'noimg.png', 'noimg.png', 'Admin', '2020-10-26 10:07:48', 'Admin', '2020-10-26 10:07:48', 8, NULL, NULL, NULL, 0, 8),
(44, '0', '0', 1, 5, 'noimg.png', 'Bridal Necklace', '0', '0', 0, '0', '1', 'noimg.png', 'noimg.png', 'noimg.png', 'noimg.png', 'Admin', '2020-10-26 10:07:54', 'Admin', '2020-10-26 10:07:54', 8, NULL, NULL, NULL, 0, 8),
(45, 'Necklace1212', 'Necklace', 1, 5, '45_0.jpeg', 'Dubai Neckalce', '0', '0', 0, '0', '1', 'noimg.png', 'noimg.png', 'noimg.png', 'noimg.png', 'Admin', '2020-10-26 10:15:33', 'Admin', '2020-10-26 10:15:33', 8, NULL, NULL, NULL, 1, 4),
(46, 'NKL 12122', 'Necklace', 1, 5, '46_0.jpeg', '', '0', '0', 0, '0', '2', 'noimg.png', 'noimg.png', 'noimg.png', 'noimg.png', 'Admin', '2020-10-26 10:21:38', 'Admin', '2020-10-26 10:21:38', 8, NULL, NULL, NULL, 1, 4),
(47, 'NKL0001', 'Necklace', 1, 5, '47_0.jpeg', 'Bridal Necklace', '0', '0', 0, '0', '1', 'noimg.png', 'noimg.png', 'noimg.png', 'noimg.png', 'Admin', '2020-10-26 10:24:03', 'Admin', '2020-10-26 10:24:03', 8, NULL, NULL, NULL, 1, 5),
(48, 'NKL 0002', 'Necklace', 1, 5, '48_0.jpeg', 'Necklace', '0', '0', 0, '0', '1', 'noimg.png', 'noimg.png', 'noimg.png', 'noimg.png', 'Admin', '2020-10-26 10:26:33', 'Admin', '2020-10-26 10:26:33', 10, NULL, NULL, NULL, 1, 1),
(49, 'NKL 0005', 'Necklace', 1, 5, '49_0.jpeg', 'Bridal Necklace', '0', '0', 0, '0', '1', 'noimg.png', 'noimg.png', 'noimg.png', 'noimg.png', 'Admin', '2020-10-26 11:13:03', 'Admin', '2020-10-26 11:13:03', 8, NULL, NULL, NULL, 1, 12),
(50, 'NKL 0006', 'Necklace', 1, 5, '50_0.jpeg', '0', '0', '0', 0, '0', '2', 'noimg.png', 'noimg.png', 'noimg.png', 'noimg.png', 'Admin', '2020-10-26 11:15:35', 'Admin', '2020-10-26 11:15:35', 8, NULL, NULL, NULL, 1, 13),
(51, 'NKL 0007', 'Necklace', 1, 5, '51_0.jpeg', 'Antique Necklace', '0', '0', 0, '0', '2', 'noimg.png', 'noimg.png', 'noimg.png', 'noimg.png', 'Admin', '2020-10-26 11:18:14', 'Admin', '2020-10-26 11:18:14', 6, NULL, NULL, NULL, 1, 15),
(52, 'NKL 0008', 'Necklace', 1, 5, '52_0.jpeg', 'Necklace', '0', '0', 0, '0', '1', 'noimg.png', 'noimg.png', 'noimg.png', 'noimg.png', 'Admin', '2020-10-26 11:19:34', 'Admin', '2020-10-26 11:19:34', 8, NULL, NULL, NULL, 1, 14),
(53, 'NKL 0008', 'Necklace', 1, 5, '53_0.jpeg', 'Antique Necklace', '0', '0', 0, '0', '2', 'noimg.png', 'noimg.png', 'noimg.png', 'noimg.png', 'Admin', '2020-10-26 11:21:24', 'Admin', '2020-10-26 11:21:24', 10, NULL, NULL, NULL, 1, 15),
(54, 'NKL 0008', 'Necklace', 1, 5, '54_0.jpeg', 'Bridal Necklace', '0', '0', 0, '0', '1', 'noimg.png', 'noimg.png', 'noimg.png', 'noimg.png', 'Admin', '2020-10-26 11:23:28', 'Admin', '2020-10-26 11:23:28', 7, NULL, NULL, NULL, 1, 15),
(55, 'NKL 0010', 'Necklace', 1, 5, '55_0.jpeg', 'Sitahar type Square shape Locket', '0', '0', 0, '0', '1', 'noimg.png', 'noimg.png', 'noimg.png', 'noimg.png', 'Admin', '2020-10-26 11:25:29', 'Admin', '2020-10-26 11:31:58', 8, NULL, NULL, NULL, 1, 17),
(56, 'NKL 0011', 'Necklace', 1, 5, '56_0.jpeg', 'Antique Kanthschik', '0', '0', 0, '0', '1', 'noimg.png', 'noimg.png', 'noimg.png', 'noimg.png', 'Admin', '2020-10-26 11:27:52', 'Admin', '2020-10-26 11:32:51', 7, NULL, NULL, NULL, 1, 15),
(57, 'NKL 0012', 'Necklace', 1, 5, '57_0.jpeg', 'Necklace', '0', '0', 0, '0', '2', 'noimg.png', 'noimg.png', 'noimg.png', 'noimg.png', 'Admin', '2020-10-26 11:30:27', 'Admin', '2020-10-26 11:30:27', 9, NULL, NULL, NULL, 1, 15),
(58, 'NKL 0014', 'Necklace', 1, 5, '58_0.jpeg', 'Necklace', '0', '0', 0, '0', '3', 'noimg.png', 'noimg.png', 'noimg.png', 'noimg.png', 'Admin', '2020-10-26 11:34:16', 'Admin', '2020-10-26 11:34:16', 8, NULL, NULL, NULL, 1, 15),
(59, 'NKL 0015', 'Necklace', 1, 5, '59_0.jpeg', 'Bridal Necklace', '0', '0', 0, '0', '1', 'noimg.png', 'noimg.png', 'noimg.png', 'noimg.png', 'Admin', '2020-10-26 11:53:35', 'Admin', '2020-10-26 11:53:35', 8, NULL, NULL, NULL, 1, 15),
(60, 'NKL 0016', 'Necklace', 1, 5, '60_0.jpeg', 'Bridal Necklace', '0', '0', 0, '0', '1', 'noimg.png', 'noimg.png', 'noimg.png', 'noimg.png', 'Admin', '2020-10-26 11:55:23', 'Admin', '2020-10-26 11:55:23', 7, NULL, NULL, NULL, 1, 16),
(61, 'NKL 0017', 'Necklace', 1, 5, '61_0.jpeg', 'Sitahar', '0', '0', 0, '0', '1', 'noimg.png', 'noimg.png', 'noimg.png', 'noimg.png', 'Admin', '2020-10-26 11:57:54', 'Admin', '2020-10-26 11:57:54', 8, NULL, NULL, NULL, 1, 17),
(62, 'NKL 0018', 'Necklace', 1, 5, '62_0.jpeg', 'Bridal Necklace', '0', '0', 0, '0', '1', 'noimg.png', 'noimg.png', 'noimg.png', 'noimg.png', 'Admin', '2020-10-26 11:59:04', 'Admin', '2020-10-26 11:59:04', 8, NULL, NULL, NULL, 1, 17),
(63, 'NKL 0019', 'Necklace', 1, 5, '63_0.jpeg', 'Bridal Necklace', '0', '0', 0, '0', '2', 'noimg.png', 'noimg.png', 'noimg.png', 'noimg.png', 'Admin', '2020-10-26 12:00:24', 'Admin', '2020-10-26 12:00:24', 8, NULL, NULL, NULL, 1, 15),
(64, 'NKL 0020', 'Necklace', 1, 5, '64_0.jpeg', 'Bridal Necklace', '0', '0', 0, '0', '1', 'noimg.png', 'noimg.png', 'noimg.png', 'noimg.png', 'Admin', '2020-10-26 12:01:50', 'Admin', '2020-10-26 12:01:50', 8, NULL, NULL, NULL, 1, 15),
(65, 'NKL 0021', 'Necklace', 1, 5, '65_0.jpeg', 'Sitahar Neckalce', '0', '0', 0, '0', '3', 'noimg.png', 'noimg.png', 'noimg.png', 'noimg.png', 'Admin', '2020-10-26 12:02:49', 'Admin', '2020-10-26 12:02:49', 6, NULL, NULL, NULL, 1, 16),
(66, 'NKL 0023', 'Necklace', 1, 5, '66_0.jpeg', 'Necklace', '0', '0', 0, '0', '3', 'noimg.png', 'noimg.png', 'noimg.png', 'noimg.png', 'Admin', '2020-10-26 12:05:28', 'Admin', '2020-10-26 12:05:28', 7, NULL, NULL, NULL, 1, 15),
(67, 'NKL 0024', 'Necklace', 1, 5, '67_0.jpeg', 'Necklace', '0', '0', 0, '0', '1', 'noimg.png', 'noimg.png', 'noimg.png', 'noimg.png', 'Admin', '2020-10-26 12:06:54', 'Admin', '2020-10-26 12:06:54', 7, NULL, NULL, NULL, 1, 15),
(68, 'NKL 0025', 'Necklace', 1, 5, '68_0.jpeg', 'Necklace', '0', '0', 0, '0', '1', 'noimg.png', 'noimg.png', 'noimg.png', 'noimg.png', 'Admin', '2020-10-26 12:08:13', 'Admin', '2020-10-26 12:08:13', 8, NULL, NULL, NULL, 1, 15),
(69, 'NKL 0026', 'Necklace', 1, 5, '69_0.jpeg', 'Necklace', '0', '0', 0, '0', '3', 'noimg.png', 'noimg.png', 'noimg.png', 'noimg.png', 'Admin', '2020-10-26 12:09:12', 'Admin', '2020-10-26 12:09:12', 7, NULL, NULL, NULL, 1, 15),
(70, 'NKL 0026', 'Necklace', 1, 5, '70_0.jpeg', 'Necklace', '0', '0', 0, '0', '3', 'noimg.png', 'noimg.png', 'noimg.png', 'noimg.png', 'Admin', '2020-10-26 12:10:43', 'Admin', '2020-10-26 12:10:43', 8, NULL, NULL, NULL, 1, 15),
(71, 'NKL 0028', 'Necklace', 1, 5, '71_0.jpeg', 'Necklace', '0', '0', 0, '0', '3', 'noimg.png', 'noimg.png', 'noimg.png', 'noimg.png', 'Admin', '2020-10-26 12:12:10', 'Admin', '2020-10-26 12:16:38', 8, NULL, NULL, NULL, 1, 15),
(72, 'NKL 0029', 'Necklace', 1, 5, '72_0.jpeg', 'Necklace', '0', '0', 0, '0', '3', 'noimg.png', 'noimg.png', 'noimg.png', 'noimg.png', 'Admin', '2020-10-26 12:13:36', 'Admin', '2020-10-26 12:13:36', 10, NULL, NULL, NULL, 1, 15),
(73, 'NKL 0028', 'Necklace', 1, 5, 'noimg.png', 'Necklace', '0', '0', 0, '0', '3', 'noimg.png', 'noimg.png', 'noimg.png', 'noimg.png', 'Admin', '2020-10-26 12:15:16', 'Admin', '2020-10-26 12:16:22', 8, NULL, NULL, NULL, 0, 18),
(74, 'Ring10100', 'Finger Ring', 1, 2, '74_0.jpeg', 'Finger Ring', '0', '0', 0, '0', '1', 'noimg.png', 'noimg.png', 'noimg.png', 'noimg.png', 'Admin', '2021-04-09 06:54:58', 'Admin', '2021-04-09 06:54:58', 6, NULL, NULL, NULL, 1, 0),
(75, 'Ring10101', 'Finger Ring', 1, 2, '75_0.jpeg', 'Finger Ring', '0', '0', 0, '0', '1', 'noimg.png', 'noimg.png', 'noimg.png', 'noimg.png', 'Admin', '2021-04-09 06:55:29', 'Admin', '2021-04-09 06:55:29', 6, NULL, NULL, NULL, 1, 0),
(76, 'Ring10102', 'Finger Ring', 1, 2, '76_0.jpeg', 'Finger Ring', '0', '0', 0, '0', '1', 'noimg.png', 'noimg.png', 'noimg.png', 'noimg.png', 'Admin', '2021-04-09 06:55:42', 'Admin', '2021-04-09 06:55:42', 6, NULL, NULL, NULL, 1, 0),
(77, 'Ring10103', 'Finger Ring', 1, 2, '77_0.jpeg', 'Finger Ring', '0', '0', 0, '0', '1', 'noimg.png', 'noimg.png', 'noimg.png', 'noimg.png', 'Admin', '2021-04-09 06:55:59', 'Admin', '2021-04-09 06:55:59', 6, NULL, NULL, NULL, 1, 0),
(78, 'locket10100', 'Locket', 1, 4, '78_0.jpeg', 'Locket', '0', '0', 0, '0', '1', 'noimg.png', 'noimg.png', 'noimg.png', 'noimg.png', 'Admin', '2021-04-09 07:02:45', 'Admin', '2021-04-09 07:02:45', 7, NULL, NULL, NULL, 1, 0),
(79, 'locket10101', 'Locket', 1, 4, '79_0.jpeg', 'Locket', '0', '0', 0, '0', '1', 'noimg.png', 'noimg.png', 'noimg.png', 'noimg.png', 'Admin', '2021-04-09 07:02:57', 'Admin', '2021-04-09 07:02:57', 7, NULL, NULL, NULL, 1, 0),
(80, 'locket10102', 'Locket', 1, 4, '80_0.jpeg', 'Locket', '0', '0', 0, '0', '1', 'noimg.png', 'noimg.png', 'noimg.png', 'noimg.png', 'Admin', '2021-04-09 07:03:05', 'Admin', '2021-04-09 07:03:05', 7, NULL, NULL, NULL, 1, 0),
(81, 'locket10103', 'Locket', 1, 4, '81_0.jpeg', 'Locket', '0', '0', 0, '0', '1', 'noimg.png', 'noimg.png', 'noimg.png', 'noimg.png', 'Admin', '2021-04-09 07:03:16', 'Admin', '2021-04-09 07:03:16', 7, NULL, NULL, NULL, 1, 0),
(82, 'Locket10104', 'Locket', 1, 4, '82_0.jpeg', 'Locket', '0', '0', 0, '0', '1', 'noimg.png', 'noimg.png', 'noimg.png', 'noimg.png', 'Admin', '2021-04-09 08:14:59', 'Admin', '2021-04-09 08:14:59', 7, NULL, NULL, NULL, 1, 0),
(83, 'Locket10105', 'Locket', 1, 4, '83_0.jpeg', 'Locket', '0', '0', 0, '0', '1', 'noimg.png', 'noimg.png', 'noimg.png', 'noimg.png', 'Admin', '2021-04-09 08:19:24', 'Admin', '2021-04-09 08:19:24', 7, NULL, NULL, NULL, 1, 0),
(84, 'Locket10106', 'Locket', 1, 4, '84_0.jpeg', 'Locket', '0', '0', 0, '0', '1', 'noimg.png', 'noimg.png', 'noimg.png', 'noimg.png', 'Admin', '2021-04-09 08:39:34', 'Admin', '2021-04-09 08:39:34', 7, NULL, NULL, NULL, 1, 0),
(85, 'Taj10001', 'Taj', 1, 12, '85_0.jpeg', 'Locket', '0', '0', 0, '0', '1', 'noimg.png', 'noimg.png', 'noimg.png', 'noimg.png', 'Admin', '2021-04-09 08:41:42', 'Admin', '2021-04-09 08:41:42', 8, NULL, NULL, NULL, 1, 0),
(86, 'Taj10002', 'Taj', 1, 12, '86_0.jpeg', 'Locket', '0', '0', 0, '0', '1', 'noimg.png', 'noimg.png', 'noimg.png', 'noimg.png', 'Admin', '2021-04-09 08:45:07', 'Admin', '2021-04-09 08:45:07', 8, NULL, NULL, NULL, 1, 0),
(87, 'Taj10003', 'Taj', 1, 12, '87_0.jpeg', 'Locket', '0', '0', 0, '0', '1', 'noimg.png', 'noimg.png', 'noimg.png', 'noimg.png', 'Admin', '2021-04-09 08:45:17', 'Admin', '2021-04-09 08:45:17', 8, NULL, NULL, NULL, 1, 0),
(88, 'Taj100004', 'Taj', 1, 12, '88_0.jpeg', 'Taj', '0', '0', 0, '0', '1', 'noimg.png', 'noimg.png', 'noimg.png', 'noimg.png', 'Admin', '2021-04-10 03:57:07', 'Admin', '2021-04-10 03:57:07', 8, NULL, NULL, NULL, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `0_ratings`
--

DROP TABLE IF EXISTS `0_ratings`;
CREATE TABLE `0_ratings` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `product_id` bigint(20) NOT NULL,
  `rating` double(8,2) NOT NULL,
  `comments` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `anonymous` tinyint(1) NOT NULL DEFAULT 0 COMMENT '0=no, 1=yes',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `0_ratings`
--

INSERT INTO `0_ratings` (`id`, `user_id`, `product_id`, `rating`, `comments`, `anonymous`, `created_at`, `updated_at`) VALUES
(1, 1, 3, 5.00, 'Excellent Design', 0, '2020-07-25 06:08:11', '2020-07-25 06:08:11');

-- --------------------------------------------------------

--
-- Table structure for table `0_users`
--

DROP TABLE IF EXISTS `0_users`;
CREATE TABLE `0_users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `website` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `facebook` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `location` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `birth_day` date DEFAULT NULL,
  `marriage_day` date DEFAULT NULL,
  `auth_token` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `0_users`
--

INSERT INTO `0_users` (`id`, `name`, `email`, `role`, `website`, `facebook`, `image`, `phone`, `address`, `location`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`, `birth_day`, `marriage_day`, `auth_token`) VALUES
(1, '      rashedul islam', 'rashed@goldenworldltd.com', 'admin', '      www.blilbd.com', '      fb/masum', '01911491237.jpg', ' 01911491237', '      Aftabnagar, Dhaka', '      Dhaka', '0000-00-00 00:00:00', 'e10adc3949ba59abbe56e057f20f883e', 'rashed@goldenworldltd.com', NULL, '2020-07-19 10:30:59', '1979-11-21', '2020-06-19', 'qhUDgXjpItTj'),
(2, ' Sumon', 'sumon@gmail.com', 'user', ' ', ' ', '01767676761.jpg', '01767676761', ' Dhaka ', ' ', '2020-08-14 22:10:58', 'e10adc3949ba59abbe56e057f20f883e', 'sumon@gmail.com', '2020-08-14 22:10:58', '2020-08-14 22:10:58', NULL, NULL, 'SraVIJ7KePmI');

-- --------------------------------------------------------

--
-- Table structure for table `addresses`
--

DROP TABLE IF EXISTS `addresses`;
CREATE TABLE `addresses` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `type` tinyint(1) NOT NULL COMMENT '0=billing, 1=shipping',
  `status` tinyint(1) NOT NULL COMMENT '1=active',
  `address_line_1` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address_line_2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `district_id` bigint(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `districts`
--

DROP TABLE IF EXISTS `districts`;
CREATE TABLE `districts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `measurment_units`
--

DROP TABLE IF EXISTS `measurment_units`;
CREATE TABLE `measurment_units` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'KG, Piece',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
CREATE TABLE `orders` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `transection_id` bigint(20) NOT NULL,
  `shipping_address_id` bigint(20) NOT NULL,
  `billing_address_id` bigint(20) NOT NULL,
  `order_status_id` bigint(20) NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `buyer_comment` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `seller_comment` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `admin_comment` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `total_amount` double(8,2) NOT NULL,
  `discount` double(8,2) NOT NULL,
  `shipping_charge` double(8,2) NOT NULL,
  `vat` double(8,2) NOT NULL,
  `service_charge` double(8,2) NOT NULL,
  `total_payable` double(8,2) NOT NULL,
  `applied_coupon` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `order_details`
--

DROP TABLE IF EXISTS `order_details`;
CREATE TABLE `order_details` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `variation_id` bigint(20) NOT NULL,
  `product_id` bigint(20) NOT NULL,
  `price` double(8,2) NOT NULL,
  `sale_price` double(8,2) NOT NULL,
  `buying_price` double(8,2) NOT NULL,
  `quantity` double(8,2) NOT NULL,
  `total_price` double(8,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `order_statuses`
--

DROP TABLE IF EXISTS `order_statuses`;
CREATE TABLE `order_statuses` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'accepted, processing',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `0_carousel`
--
ALTER TABLE `0_carousel`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `0_catagories`
--
ALTER TABLE `0_catagories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `0_chatbot`
--
ALTER TABLE `0_chatbot`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `0_chathistory`
--
ALTER TABLE `0_chathistory`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `0_goldprice`
--
ALTER TABLE `0_goldprice`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `0_products`
--
ALTER TABLE `0_products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `0_ratings`
--
ALTER TABLE `0_ratings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `0_users`
--
ALTER TABLE `0_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `addresses`
--
ALTER TABLE `addresses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `districts`
--
ALTER TABLE `districts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `measurment_units`
--
ALTER TABLE `measurment_units`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_details`
--
ALTER TABLE `order_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_statuses`
--
ALTER TABLE `order_statuses`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `0_carousel`
--
ALTER TABLE `0_carousel`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `0_catagories`
--
ALTER TABLE `0_catagories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `0_chatbot`
--
ALTER TABLE `0_chatbot`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `0_chathistory`
--
ALTER TABLE `0_chathistory`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `0_goldprice`
--
ALTER TABLE `0_goldprice`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `0_products`
--
ALTER TABLE `0_products`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=89;

--
-- AUTO_INCREMENT for table `0_ratings`
--
ALTER TABLE `0_ratings`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `0_users`
--
ALTER TABLE `0_users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `addresses`
--
ALTER TABLE `addresses`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `districts`
--
ALTER TABLE `districts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `measurment_units`
--
ALTER TABLE `measurment_units`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `order_details`
--
ALTER TABLE `order_details`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `order_statuses`
--
ALTER TABLE `order_statuses`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;
