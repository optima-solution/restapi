<?php
header("Access-Control-Allow-Origin: *");
// header("Content-Type: application/json; charset=UTF-8");
header("Content-Type: application/json;");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
 
include_once '../config/Database.php';

function isDate($value) 
{
    if (!$value or strlen($value)<3) {
        return false;
    }

    try {
        new \DateTime($value);
        return true;
    } catch (\Exception $e) {
        return false;
    }
}

$json = file_get_contents('php://input');
$data = json_decode($json,true); // convert to php array
$id=0;

if (isset($_GET['FunctionType'])) {
	$FunctionType = $_GET['FunctionType'];
	if (isset($_GET['id'])){
		$id=$_GET['id'];
	}
	if ($FunctionType=='read'){
		user_read($id);
	}
	$data=false;//marking data false not execute insert function
}  
// echo "data:".$data;
// foreach ($_POST['items'] as  $value) {
if($data)
{
	foreach ($data as $key => $value) {
	    // $msg .= 'name: ' . $value['name'] . ' -> description: ' . $value['description'] . PHP_EOL;
		// print_r($value);
		if ($value['FunctionType']=='read'){
			if ($value['id']){$id=$value['id'];}
			user_read($id);
		}
		else if ($value['FunctionType']=='user_insert'){
			user_insert($value);
		}
		else if ($value['FunctionType']=='user_update'){
			user_update($value);
		}
	}
}
function user_insert($value){
	// echo 'initialzed';
	$err = array();
	$res_det = array();
	$database = new Database();
	$db = $database->getConnection();

	$name=addslashes($value['name']);
	$email=addslashes(trim($value['email']));
	$role=addslashes($value['role']);
	$website=addslashes($value['website']);
	$facebook=addslashes($value['facebook']);
	$phone=addslashes(trim($value['phone']));
	$address=addslashes($value['address']);
	$location=addslashes($value['location']);
	$email_verified_at=date('Y-m-d H:i:s');
	$password=$value['password'];
	$remember_token=addslashes($value['remember_token']);
	$created_at=date('Y-m-d H:i:s');
	$updated_at=date('Y-m-d H:i:s');
    $birth_day= (isDate($value['birth_day'])==true)?"'".$value['birth_day']."'":'NULL';
	$marriage_day= (isDate($value['marriage_day'])==true)?"'".$value['marriage_day']."'":'NULL';
	$imgdata=$value['image'];
	if ($imgdata){
				list($type, $imgdata) = explode(';', $imgdata);
				// print_r('type: '.substr($type,strripos($type,'/')-strlen($type)+1));
				$type=substr($type,strripos($type,'/')-strlen($type)+1);
				list(, $imgdata)      = explode(',', $imgdata);
				$imgdata = base64_decode($imgdata);
				// print_r('processed data:'.$imgdata);
				file_put_contents('../images/uploads/users/'.addslashes($value['phone']).'.'.$type, $imgdata);
				$image=addslashes($value['phone']).'.'.$type;
			}
			else{
				$image='noimg.png';
			}	
	 $sql = "INSERT INTO 0_users (`name` , `email` , `role` , `website` , `facebook` , `image` , `phone` , `address` , `location` , `email_verified_at` , `password` , `remember_token` , `created_at` , `updated_at`, `birth_day` , `marriage_day` )
	    VALUES('".$name."', '".$email."', '".$role."', '".$website."', '".$facebook."', '".$image."', '".$phone."', '".$address."', '".$location."', '".$email_verified_at."', '".$password."', '".$remember_token."', '".$created_at."', '".$updated_at."', ".$birth_day.", ".$marriage_day.")";

	    // exit($msg);
    if ($db->query($sql) === TRUE) {
			array_push($err,false);
			array_push($res_det, 'Successfully registered for id' . $db->insert_id.' name'.$name);
			$nd=array("errors"=>false,
				"msg"=>'Successfully registered for id' . $db->insert_id.' name'.$name);

    	}
    else
    {
    	array_push($err,true);
		array_push($res_det, 'Not Successfull' );
		$nd=array("errors"=>true,
				"msg"=>'Not Successfull');
    }
	// $res = array($err,$res_det);
	// $res['msg']=$res_det;
	// $res['error']=$err;
	$res["response"] = array();
	array_push($res["response"], $nd);
	echo json_encode($res);
	http_response_code(200);
    return $res;
} //end of insert function

function user_read($id){
	$database = new Database();
	$conn = $database->getConnection();
	if ($id==0){
		$sql="SELECT * FROM `0_users`";
	}
	else {
		$sql="SELECT * FROM `0_users` where id='".$id."'";
	}
	
	$result = $conn->query($sql);
	$users = array();
	$res = array('error' => false);
	while ($row = $result->fetch_assoc()){
		array_push($users, $row);
		// echo 'row:'.$row;
	}
	$res['users'] = $users;
 	$conn -> close();
	echo json_encode($res);
	return json_encode($res);
}	

function user_update($value){

	$err=array();
	$msg=array();
	$database = new Database();
	$db = $database->getConnection();
	$loginID=addslashes($value['loginID']);
	$id=addslashes($value['id']);
	$name=addslashes($value['name']);
	$email=$value['email'];
	$role=addslashes($value['role']);
	$website=addslashes($value['website']);
	$facebook=addslashes($value['facebook']);
	$phone=$value['phone'];
	$address=addslashes($value['address']);
	$location=addslashes($value['location']);
	$email_verified_at=date('Y-m-d H:i:s');
	$password=addslashes($value['password']);
	$remember_token=addslashes($value['remember_token']);
	// $created_at=date('Y-m-d H:i:s');
	$birth_day= validateDateTime(addslashes($value['birth_day']),'Y-m-d')?(addslashes($value['birth_day'])):null ;
	$marriage_day= validateDateTime(addslashes($value['marriage_day']),'Y-m-d')?(addslashes($value['marriage_day'])):null ;
	$imgdata=$value['image'];
	if ($imgdata){
				list($type, $imgdata) = explode(';', $imgdata);
				// print_r('type: '.substr($type,strripos($type,'/')-strlen($type)+1));
				$type=substr($type,strripos($type,'/')-strlen($type)+1);
				list(, $imgdata)      = explode(',', $imgdata);
				$imgdata = base64_decode($imgdata);
				// print_r('processed data:'.$imgdata);
				file_put_contents('../images/uploads/users/'.addslashes($value['phone']).'.'.$type, $imgdata);
				$image=addslashes($value['phone']).'.'.$type;
				$sql = "UPDATE 0_users set `name` ='{$name}',`email` ='{$email}',`role` ='{$role}',`website` ='{$website}',`facebook` ='{$facebook}',`image` ='{$image}',`phone` ='{$phone}',`address` ='{$address}',`location` ='{$location}',`email_verified_at` ='{$email_verified_at}',`password` ='{$password}',`remember_token` ='{$remember_token}',`updated_at` ='{$updated_at}',`birth_day` ='{$birth_day}',`marriage_day` ='{$marriage_day}' WHERE id='{$loginID}'";
			}
			else{
				 $sql = "UPDATE 0_users set `name` ='{$name}',`email` ='{$email}',`role` ='{$role}',`website` ='{$website}',`facebook` ='{$facebook}',`phone` ='{$phone}',`address` ='{$address}',`location` ='{$location}',`email_verified_at` ='{$email_verified_at}',`password` ='{$password}',`remember_token` ='{$remember_token}',`updated_at` ='{$updated_at}',`birth_day` ='{$birth_day}',`marriage_day` ='{$marriage_day}' WHERE id='{$loginID}'";
			}	
	

	    // exit($msg);
    if ($db->query($sql) === TRUE) {
    		array_push($err,false);
    		array_push($msg,"Successful for updating id:".$db->insert_id.' sql:'.$sql.PHP_EOL);
			$nd=array("errors"=>false,
			"msg"=>'Successfully updated for ' .$sql.$name);
		}
    else
    {
    	array_push($err,true);
		array_push($msg,"Not Successful for ".$sql.PHP_EOL);
		$nd=array("errors"=>true,
			"msg"=>'Not Successful for ' . $sql);
    }
    // $res = array($err,$msg);
    // echo json_encode($res);
	// return $res;
	$res["response"] = array();
	array_push($res["response"], $nd);
	echo json_encode($res);
	http_response_code(200);
} //end of insert function
?>