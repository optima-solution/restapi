<?php
header("Access-Control-Allow-Origin: *");
// header("Content-Type: application/json; charset=UTF-8");
header("Content-Type: application/json;");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

include_once '../config/Database.php';

$json = file_get_contents('php://input');
$data = json_decode($json, true); // convert to php array
$id = 0;
// echo 'main_catagory_id='.$main_catagory_id;
// function type for determining Create/Read/Update/Delete Function
if (isset($_GET['FunctionType']))
{
    $FunctionType = $_GET['FunctionType'];
        if (isset($_GET['id']))
    {
        $id = $_GET['id'];
    }
    if ($FunctionType == 'read')
    {
        gold_price_read($id);
    }
    if (isset($_GET['auth_token']))
    {
        $auth_token = $_GET['auth_token'];
	}
	if (isset($_GET['loginID']))
    {
        $loginID = $_GET['loginID'];
    }
    if ($FunctionType == 'delete')
    {
        gold_price_delete($id,$auth_token,$loginID);
    }

    $data = false; //marking data false not execute insert function
    
}
// echo "data:".$data;
// foreach ($_POST['items'] as  $value) {
if ($data)
{
    foreach ($data as $key => $value)
    {
        // $msg .= 'name: ' . $value['name'] . ' -> description: ' . $value['description'] . PHP_EOL;
        // print_r($value);
        if ($value['FunctionType'] == 'read')
        {
            // if ($value['id'])
            // {
            //     $id = $value['id'];
            // }
            gold_price_read($id);
        }
        else if ($value['FunctionType'] == 'gold_price_insert')
        {
            echo json_encode(gold_price_insert($value));
        }
        else if ($value['FunctionType'] == 'gold_price_update')
        {
           echo json_encode(gold_price_update($value));
        }
    }
}
function gold_price_insert($value)
{

    // $err=array();
    // $msg=array();
    $res_det = array();
    $database = new Database();
    $db = $database->getConnection();

    $karat = addslashes($value['karat']);
    $price = addslashes($value['price']);
    $created_at = date('Y-m-d H:i:s');
    $updated_at = date('Y-m-d H:i:s');
    $loginid = addslashes($value['loginId']);
	$auth_token = addslashes($value['auth_token']);
    //credential check start
    $sql = "SELECT * FROM 0_users WHERE id = '" . $loginid . "' and auth_token = '" . $auth_token . "'";
    $result = $db->query($sql);
    if ($result->num_rows > 0)
    { //if have proper token
        
        $sql = "INSERT INTO 0_goldprice (`karat`, `price`, `created_at`, `updated_at`, `updated_by`)
			VALUES('" . $karat . "', '" . $price . "', '" .$created_at . "', '" . $updated_at . "', '".$loginid."')";

        // exit($msg);
        if ($db->query($sql) === true)
        {
            // array_push($err,false);
            // array_push($msg,"Successful for inserting id:".$db->insert_id.' sql:'.$sql.PHP_EOL);
            // array_push($res_det, 'Successfully inserted id ' . $db->insert_id);
            array_push($res_det, array('msg'=>'Successfully inserted for ' . $db->insert_id));
        }
        else
        {
            // array_push($err,true);
            // array_push($msg,"Not Successful for ".$sql.PHP_EOL);
            // array_push($res_det, 'Not Successfull ' . $sql);
            array_push($res_det, array('msg'=>'Error found and not successful for ' . $sql));
        }

    } //end proper authentication
    else // if donot have proper authentication
    {
        array_push($res_det,array('msg'=>'You do not have proper credential'));
    }
    // echo json_encode($res_det);
    return $res_det;
} //end of insert function

function gold_price_read($id)
{
    $database = new Database();
    $conn = $database->getConnection();
        if ($id==0){
		$sql="SELECT * FROM `0_goldprice`";
	}
	else {
		$sql="SELECT * FROM `0_goldprice` where id='".$id."'";
	}
	
	$result = $conn->query($sql);
	$gold_price = array();
	$res = array('error' => false);
	while ($row = $result->fetch_assoc()){
		array_push($gold_price, $row);
		// echo 'row:'.$row;
	}
	$res['gold_price'] = $gold_price;
 	$conn -> close();
	echo json_encode($res);
	return json_encode($res);
}
function gold_price_update($value)
{
    $res_det = array();
    $database = new Database();
    $db = $database->getConnection();
	$id = addslashes($value['id']);
    $karat = addslashes($value['karat']);
    $price = addslashes($value['price']);
    // $created_at = date('Y-m-d H:i:s');
    $updated_at = date('Y-m-d H:i:s');
    $loginid = addslashes($value['loginId']);
	$auth_token = addslashes($value['auth_token']);


    //credential check start
    $sql = "SELECT * FROM 0_users WHERE id = '" . $loginid . "' and auth_token = '" . $auth_token . "'";
    $result = $db->query($sql);
    if ($result->num_rows > 0)
    { //if have proper token
       
            $sql = "UPDATE 0_goldprice set `karat`='{$karat}',`price`='{$price}',   `updated_at`='{$updated_at}' WHERE id='{$id}'";

        // exit($msg);
        if ($db->query($sql) === true)
        {
            // $res_det=array("Error"=>"false",
            // 	"Msg"=>"Successful for updating id:".$db->insert_id.' sql:'.$sql);
            // $res_det=array('Sucessfully Updated');
            // array_push($response,$res_det);
            array_push($res_det, array('error'=>false,'msg'=>'Successfully updated for ' . $id));
        } 
        else 
        {
            // $res_det=array("Error"=>false,"Msg"=>"Not Successful for ".$sql);
            // $res_det=array('Error found and not successful');
            // array_push($response,$res_det);
            array_push($res_det, array('error'=>true,'msg'=>'Error found and not successful for ' . $id));
        }
    } //end proper authentication
    else // if donot have proper authentication
    {
        array_push($res_det,array('error'=>true,'msg'=>'You do not have proper credential'));
    }
    // echo json_encode($res_det);
    return $res_det;
} //end of update function

function gold_price_delete($id,$auth_token,$loginID){
    $res_det = array();
    $database = new Database();
    $db = $database->getConnection();

    $sql = "SELECT * FROM 0_users WHERE id = '" . $loginID . "' and auth_token = '" . $auth_token . "'";
    $result = $db->query($sql);
    if ($result->num_rows > 0)
    { //if have proper token
        $sql="DELETE FROM 0_goldprice WHERE id='".$id."'";
        if ($db->query($sql) === true)
        {
            // array_push($err,false);
            // array_push($msg,"Successful for inserting id:".$db->insert_id.' sql:'.$sql.PHP_EOL);
            array_push($res_det, 'Successfully deleted id ' .$id);
        }
        else
        {
            // array_push($err,true);
            // array_push($msg,"Not Successful for ".$sql.PHP_EOL);
            array_push($res_det, 'Not Successfull ' . $sql);
        }

    }
    else // if donot have proper authentication
    {
        array_push($res_det, 'You do not have proper credential');
    }
    echo json_encode($res_det);
    return json_encode($res_det);
}
?>
