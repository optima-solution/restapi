<?php
header("Access-Control-Allow-Origin: *");
// header("Content-Type: application/json; charset=UTF-8");
header("Content-Type: application/json;");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
 
include_once '../config/Database.php';

$json = file_get_contents('php://input');
$data = json_decode($json,true); // convert to php array
$id=0;

$stat='%';
$pagename='%';

if (isset($_GET['FunctionType'])) {
	$FunctionType = $_GET['FunctionType'];
	if (isset($_GET['id'])){
		$id=$_GET['id'];
	}
	if (isset($_GET['status'])){
		$stat=$_GET['status'];		
	}
	if (isset($_GET['pagename'])){
		$pagename=$_GET['pagename'];		
	}
	if ($FunctionType=='read'){
		carousel_read($id,$stat,$pagename);
	}
	if (isset($_GET['auth_token']))
    {
        $auth_token = $_GET['auth_token'];
	}
	if (isset($_GET['loginID']))
    {
        $loginID = $_GET['loginID'];
	}
	if ($FunctionType == 'delete')
    {
        carousel_delete($id,$auth_token,$loginID);
    }
	$data=false;//marking data false not execute insert function
}  

// echo "data:".$data;
// foreach ($_POST['items'] as  $value) {
if($data)
{
	foreach ($data as $key => $value) {
	    // $msg .= 'name: ' . $value['name'] . ' -> description: ' . $value['description'] . PHP_EOL;
		// print_r($value);
		if ($value['FunctionType']=='read'){
			if ($value['id']){$id=$value['id'];}
			carousel_read($id,$stat,$pagename);
		}
		else if ($value['FunctionType']=='carousel_insert'){
			echo json_encode(carousel_insert($value));
		}
		else if ($value['FunctionType']=='carousel_update'){
			echo json_encode(carousel_update($value));
		}
	}
}


function carousel_insert($value){
	// echo 'initialzed';
	$res_det = array();
	$database = new Database();
	$db = $database->getConnection();
    $name=addslashes($value['name']);
	$heading=addslashes($value['heading']);
    $description=addslashes(trim($value['description']));
    $created_at=date('Y-m-d H:i:s');
	$updated_at=date('Y-m-d H:i:s');
   
    $imgdata=$value['image'];
    $loginid = addslashes($value['loginId']);
    $auth_token = addslashes($value['auth_token']);
    $created_by=$loginid;
    // $updated_by=addslashes(trim($value['updated_by']));
	$status=addslashes(trim($value['status']));
	$pagename=addslashes(trim($value['pagename']));
	if ($imgdata){
				list($type, $imgdata) = explode(';', $imgdata);
				// print_r('type: '.substr($type,strripos($type,'/')-strlen($type)+1));
				$type=substr($type,strripos($type,'/')-strlen($type)+1);
				list(, $imgdata)      = explode(',', $imgdata);
				$imgdata = base64_decode($imgdata);
				// print_r('processed data:'.$imgdata);
				file_put_contents('../images/uploads/carousel/'.addslashes($value['name']).'.'.$type, $imgdata);
				$image=addslashes($value['name']).'.'.$type;
			}
			else{
				$image='noimg.png';
			}	
    
    //credential check start
    $sql = "SELECT * FROM 0_users WHERE id = '" . $loginid . "' and auth_token = '" . $auth_token . "'";
    $result = $db->query($sql);
    if ($result->num_rows > 0)
    { //if have proper token
        $sql = "INSERT INTO 0_carousel (`name`,`image`,`heading`,`description`,`created_at`,  `created_by`,`status`,`pagename`)
            VALUES('".$name."', '".$image."', '".$heading."', '".$description."', '".$created_at."', '".$created_by."', '".$status."', '".$pagename."')";

            // exit($msg);
        if ($db->query($sql) === TRUE) {
			array_push($res_det, array('error'=>false,'msg'=>'Successfully inserted for ' . $db->insert_id));

            }
        else
        {
            array_push($res_det, array('error'=>true,'msg'=>'Error found and not successful'));
		}
		// print_r($sql);
    }
    else // if donot have proper authentication
    {
		array_push($res_det,array('error'=>true,'msg'=>'You do not have proper credential'));
    }
    $db -> close();
    // return $res_det;
	// $res = array($err,$res_det);
	// $res['msg']=$res_det;
	// $res['error']=$err;
	http_response_code(200);
    return $res_det;
} //end of insert function



function carousel_read($id,$stat,$pagename){
	$database = new Database();
	$conn = $database->getConnection();
	$sql= "SELECT * FROM `0_carousel`";
	$where = array();
	if ( $id <> 0 ) {
		$where[] = " id = '".$id."'";
	}
	if (strlen($stat)>0 ) {
		$where[] = "status like '%".$stat."%'";
	}
	if ( strlen($pagename)>0 ) {
		$where[] = "pagename like '%".$pagename."%'";
	}

	if ( sizeof($where) > 0 ) {
		$sql .= ' WHERE '.implode(' AND ', $where);   
	}
	$result = $conn->query($sql);
	$carousels = array();
	$res = array('error' => false);
	while ($row = $result->fetch_assoc()){
		array_push($carousels, $row);
		// echo 'row:'.$row;
	}
	$res['carousels'] = $carousels;
 	$conn -> close();
	echo json_encode($res);
	return json_encode($res);
}	

function carousel_update($value){
	$res_det = array();
	$database = new Database();
	$db = $database->getConnection();
	$id=addslashes($value['id']);
	$name=addslashes($value['name']);
	$heading=addslashes($value['heading']);
    $description=addslashes(trim($value['description']));
    // $created_at=date('Y-m-d H:i:s');
	$updated_at=date('Y-m-d H:i:s');
    // $created_by=addslashes(trim($value['created_by']));
    
	$status=addslashes(trim($value['status']));
	$pagename=addslashes(trim($value['pagename']));
    $loginid = addslashes($value['loginId']);
    $updated_by=$loginid;
	$auth_token = addslashes($value['auth_token']);
    $sql = "SELECT * FROM 0_users WHERE id = '" . $loginid . "' and auth_token = '" . $auth_token . "'";
    $result = $db->query($sql);
    if ($result->num_rows > 0)
    { //if have proper token
        $imgdata=$value['image'];
        if ($imgdata){
                    list($type, $imgdata) = explode(';', $imgdata);
                    // print_r('type: '.substr($type,strripos($type,'/')-strlen($type)+1));
                    $type=substr($type,strripos($type,'/')-strlen($type)+1);
                    list(, $imgdata)      = explode(',', $imgdata);
                    $imgdata = base64_decode($imgdata);
                    // print_r('processed data:'.$imgdata);
                    file_put_contents('../images/uploads/carousel/'.addslashes($value['name']).'.'.$type, $imgdata);
                    $image=addslashes($value['name']).'.'.$type;
                    $sql = "UPDATE 0_carousel set `name` ='{$name}',`heading` ='{$heading}',`description` ='{$description}',`image` ='{$image}',`updated_at` ='{$updated_at}',`updated_by` ='{$updated_by}',`status` ='{$status}',`pagename` ='{$pagename}' WHERE id='{$id}'";
                }
                else{
                    $sql = "UPDATE 0_carousel set `name` ='{$name}',`heading` ='{$heading}',`description` ='{$description}',`updated_at` ='{$updated_at}',`updated_by` ='{$updated_by}',`status` ='{$status}',`pagename` ='{$pagename}' WHERE id='{$id}'";
                }	
        

            // exit($msg);
        if ($db->query($sql) === TRUE) {
			array_push($res_det, array('error'=>false,'msg'=>'Successfully updated for ' . $id));
            }
        else
        {
			array_push($res_det, array('error'=>true,'msg'=>'Error found and not successful'));
        }
    } //Authentication ends here
	else // if donot have proper authentication
	{
		array_push($res_det,array('error'=>true,'msg'=>'You do not have proper credential'));
	}
	return $res_det;
	http_response_code(200);
} //end of insert function

function carousel_delete($id,$auth_token,$loginID){
    $res_det = array();
    $database = new Database();
    $db = $database->getConnection();

    $sql = "SELECT * FROM 0_users WHERE id = '" . $loginID . "' and auth_token = '" . $auth_token . "'";
    $result = $db->query($sql);
    if ($result->num_rows > 0)
    { //if have proper token
        $sql="DELETE FROM 0_carousel WHERE id='".$id."'";
        if ($db->query($sql) === true)
        {
            // array_push($err,false);
            // array_push($msg,"Successful for inserting id:".$db->insert_id.' sql:'.$sql.PHP_EOL);
            array_push($res_det, 'Successfully deleted id ' .$id);
        }
        else
        {
            // array_push($err,true);
            // array_push($msg,"Not Successful for ".$sql.PHP_EOL);
            array_push($res_det, 'Not Successfull ' . $sql);
        }

    }
    else // if donot have proper authentication
    {
        array_push($res_det, 'You do not have proper credential');
	}
	$db -> close();
    echo json_encode($res_det);
    return json_encode($res_det);
}

?>