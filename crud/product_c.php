<?php
header("Access-Control-Allow-Origin: *");
// header("Content-Type: application/json; charset=UTF-8");
header("Content-Type: application/json;");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
 
include_once '../config/Database.php';

$json = file_get_contents('php://input');
// echo 'json:'.$json;
$data = json_decode($json,true); // convert to php array
// echo 'stringified:'.$data;
$id = 0;
$category = 0;
$alt_category = 0;
$product_type = 0;
$price=0;
$limit=0;
if (isset($_GET['FunctionType']))
{
    $FunctionType = $_GET['FunctionType'];
    if (isset($_GET['id']))
    {
        $id = $_GET['id'];
    }
    if (isset($_GET['category']))
    {
        $category = $_GET['category'];
	}
	if (isset($_GET['alt_category']))
    {
        $alt_category = $_GET['alt_category'];
	}
	if (isset($_GET['price']))
    {
        $price = $_GET['price'];
	}
	if (isset($_GET['product_type']))
    {
        $product_type = $_GET['product_type'];
	}
	if (isset($_GET['limit']))
    {
        $limit = $_GET['limit'];
	}
    if ($FunctionType == 'read')
    {
        product_read($id, $category,$alt_category,$price,$product_type,$limit);
	}
	if (isset($_GET['auth_token']))
    {
        $auth_token = $_GET['auth_token'];
	}
	if (isset($_GET['loginID']))
    {
        $loginID = $_GET['loginID'];
    }
    if ($FunctionType == 'delete')
    {
        product_delete($id,$auth_token,$loginID);
    }
    $data = false; //marking data false not execute insert function
    
}  
// foreach ($_POST['items'] as  $value) {
if($data){
	foreach ($data as $key => $value) {
    // $msg .= 'name: ' . $value['name'] . ' -> description: ' . $value['description'] . PHP_EOL;
	// print_r($value);
	if ($value['FunctionType']=='Product_insert'){
		// echo 'TEST';
		echo json_encode(product_insert($value));
	}
	if ($value['FunctionType']=='Product_Update'){
		// echo 'TEST';
		echo json_encode(product_update($value));
	}
	
	}
}
		
function product_insert ($value){ 
	$msg='';
	$res_det = array();
	// print_r($value);
	// $message = 'Hello '.($user->is_logged_in() ? $user->get('first_name') : 'Guest');
	$code=addslashes($value['code']);
	$name=addslashes($value['name']);
	$product_type=addslashes($value['product_type']);
	$category=addslashes($value['category']);
	$alt_category =addslashes($value['alt_category']);
	$description=addslashes($value['description']);
	$original_price=is_numeric(addslashes($value['original_price']))?addslashes($value['original_price']):0;
	// $discount_amt=addslashes($value['discount_amt']);
	$discount_amt=is_numeric(addslashes($value['discount_amt']))?addslashes($value['discount_amt']):0;
	// $discount_per=addslashes($value['discount_per']);
	$discount_per=is_numeric(addslashes($value['discount_per']))?addslashes($value['discount_per']):0;
	// $net_price=addslashes($value['net_price']);
	$net_price=is_numeric(addslashes($value['net_price']))?addslashes($value['net_price']):0;
	$status=addslashes($value['status']);
	$created_by=addslashes($value['created_by']);
	$last_updated_by=addslashes($value['last_updated_by']);
	$is_display=addslashes($value['is_display']);
	$display_order=addslashes($value['display_order']);

	$imgdata=$value['image'];
	$imgdata1=$value['image1'];
	$imgdata2=$value['image2'];
	$imgdata3=$value['image3'];
	$imgdata4=$value['image4'];

	$creation_date = date('Y-m-d H:i:s'); 
	$last_update_date = date('Y-m-d H:i:s'); 
	//call connection
	$database = new Database();
	$db = $database->getConnection();
	$loginid = addslashes($value['loginId']);
	$auth_token = addslashes($value['auth_token']);
    //credential check start
    $sql = "SELECT * FROM 0_users WHERE id = '" . $loginid . "' and auth_token = '" . $auth_token . "'";
    $result = $db->query($sql);
    if ($result->num_rows > 0)
    { //if have proper token
		$sql = "INSERT INTO 0_products (`code`,`name`,`product_type`,`category`, `description`, `original_price`, `discount_amt`,`discount_per`,`net_price`, `status`, `created_by`, `creation_date`, `last_updated_by`,`last_update_date`,`alt_category`,`is_display`,`display_order`)
			VALUES('".$code."', '".$name."', '".$product_type."', '".$category."', '".$description."', '".$original_price."', '".$discount_amt."', '".$discount_per."', '".$net_price."', '".$status."','".$created_by."', '".$creation_date."', '".$last_updated_by."', '".$last_update_date."', '".$alt_category."', '".$is_display."', '".$display_order."')";
			// $msg .='insert: '.$sql;

			// exit($msg);
		if ($db->query($sql) === TRUE) {
				//save and upload image
				if ($imgdata){
					list($type, $imgdata) = explode(';', $imgdata);
					// print_r('type: '.substr($type,strripos($type,'/')-strlen($type)+1));
					$type=substr($type,strripos($type,'/')-strlen($type)+1);
					list(, $imgdata)      = explode(',', $imgdata);
					$imgdata = base64_decode($imgdata);
					// print_r('processed data:'.$imgdata);
					file_put_contents('../images/uploads/allproducts/'.$db->insert_id.'_0.'.$type, $imgdata);
					$image=$db->insert_id.'_0.'.$type;
				}
				else{
					$image='noimg.png';
				}

				if ($imgdata1){
					list($type, $imgdata1) = explode(';', $imgdata1);
					// print_r('type: '.substr($type,strripos($type,'/')-strlen($type)+1));
					$type=substr($type,strripos($type,'/')-strlen($type)+1);
					list(, $imgdata1)      = explode(',', $imgdata1);
					$imgdata1 = base64_decode($imgdata1);
					// print_r('processed data:'.$imgdata);
					file_put_contents('../images/uploads/allproducts/'.$db->insert_id.'_1.'.$type, $imgdata1);
					$image1=$db->insert_id.'_1.'.$type;
				}
				else{
					$image1='noimg.png';
				}

				if ($imgdata2){
					list($type, $imgdata2) = explode(';', $imgdata2);
					// print_r('type: '.substr($type,strripos($type,'/')-strlen($type)+1));
					$type=substr($type,strripos($type,'/')-strlen($type)+1);
					list(, $imgdata2)      = explode(',', $imgdata2);
					$imgdata2 = base64_decode($imgdata2);
					// print_r('processed data:'.$imgdata);
					file_put_contents('../images/uploads/allproducts/'.$db->insert_id.'_2.'.$type, $imgdata2);
					$image2=$db->insert_id.'_2.'.$type;
				}
				else{
					$image2='noimg.png';
				}

				if ($imgdata3){
					list($type, $imgdata3) = explode(';', $imgdata3);
					// print_r('type: '.substr($type,strripos($type,'/')-strlen($type)+1));
					$type=substr($type,strripos($type,'/')-strlen($type)+1);
					list(, $imgdata3)      = explode(',', $imgdata3);
					$imgdata3 = base64_decode($imgdata3);
					// print_r('processed data:'.$imgdata);
					file_put_contents('../images/uploads/allproducts/'.$db->insert_id.'_3.'.$type, $imgdata3);
					$image3=$db->insert_id.'_3.'.$type;
				}
				else{
					$image3='noimg.png';
				}

				if ($imgdata4){
					list($type, $imgdata4) = explode(';', $imgdata4);
					// print_r('type: '.substr($type,strripos($type,'/')-strlen($type)+1));
					$type=substr($type,strripos($type,'/')-strlen($type)+1);
					list(, $imgdata4)      = explode(',', $imgdata4);
					$imgdata4 = base64_decode($imgdata4);
					// print_r('processed data:'.$imgdata);
					file_put_contents('../images/uploads/allproducts/'.$db->insert_id.'_4.'.$type, $imgdata4);
					$image4=$db->insert_id.'_4.'.$type;
				}
				else{
					$image4='noimg.png';
				}
				$updateid=$db->insert_id;
				$sql="update 0_products set image='".$image."', image1='".$image1."',image2='".$image2."', image3='".$image3."', image4='".$image4."', alt_category='".$alt_category."' where id =".$db->insert_id;
				if ($db->query($sql) === TRUE) {
						// $msg .=" and successfully images uploaded";
						array_push($res_det, array('error'=>false,'msg'=>'Successfully inserted id ' . $updateid));
				} 

			} // if data insert not successful
		else{
				// $msg .= "Not Successful for ".$sql;
				array_push($res_det, array('error'=>true,'msg'=>'Not Successfull ' . $sql));
			}
	}
		else // if donot have proper authentication
		{
			array_push($res_det, array('error'=>true,'You do not have proper credential'));
		}
		$db -> close();
		return $res_det;
}
		
function product_update ($value){ 
	$msg="";
	$res = array(
        'error' => false
    );
	$res_det = array();
	// $response["Msg"] = array();
	// $msg .= 'name: ' . $value['name'] . ' -> description: ' . $value['description'] . PHP_EOL;
	// print_r($value);
	// $id=addslashes($value['id']);
	$id=addslashes($value['prdid']);
	$code=addslashes($value['code']);
	$name=addslashes($value['name']);
	$product_type=addslashes($value['product_type']);
	$category=addslashes($value['category']);
	$alt_category =addslashes($value['alt_category']);
	$description=addslashes($value['description']);
	$original_price=addslashes($value['original_price']);
	$discount_amt=addslashes($value['discount_amt']);
	$discount_per=addslashes($value['discount_per']);
	$net_price=addslashes($value['net_price']);
	$status=addslashes($value['status']);
	// $created_by=addslashes($value['created_by']);
	$last_updated_by=addslashes($value['last_updated_by']);
	$is_display=addslashes($value['is_display']);
	$display_order=addslashes($value['display_order']);

	$imgdata=$value['image'];
	$imgdata1=$value['image1'];
	$imgdata2=$value['image2'];
	$imgdata3=$value['image3'];
	$imgdata4=$value['image4'];
	// $creation_date = date('Y-m-d H:i:s'); 
	$last_update_date = date('Y-m-d H:i:s'); 
	$loginid = addslashes($value['loginId']);
	$auth_token = addslashes($value['auth_token']);
	//call connection
	$database = new Database();
	$db = $database->getConnection();
	$sql = "SELECT * FROM 0_users WHERE id = '" . $loginid . "' and auth_token = '" . $auth_token . "'";
    $result = $db->query($sql);
    if ($result->num_rows > 0)
    { //if have proper token 
			$sql="update 0_products set `code`='".$code."', `name`='".$name."', `product_type`='".$product_type."', `category`='".$category."',`description`='".$description."', `original_price`='".$original_price."', `discount_amt`='".$discount_amt."', `discount_per`='".$discount_per."', `net_price`='".$net_price."',  `status`='".$status."', `last_updated_by`='".$last_updated_by."', `last_update_date`='".$last_update_date."',`alt_category`='".$alt_category."', `is_display`='".$is_display."', `display_order`='".$display_order."'" ;
			// exit($msg);
			//Working for 5 image data
			if ($imgdata){
				list($type, $imgdata) = explode(';', $imgdata);
				// print_r('type: '.substr($type,strripos($type,'/')-strlen($type)+1));
				$type=substr($type,strripos($type,'/')-strlen($type)+1);
				list(, $imgdata)      = explode(',', $imgdata);
				$imgdata = base64_decode($imgdata);
				// print_r('processed data:'.$imgdata);
				file_put_contents('../images/uploads/allproducts/'.$id.'_0.'.$type, $imgdata);
				$image=$id.'_0.'.$type;
				$sql .= ", image='".$image."'";
			}
			if ($imgdata1){
				list($type, $imgdata1) = explode(';', $imgdata1);
				// print_r('type: '.substr($type,strripos($type,'/')-strlen($type)+1));
				$type=substr($type,strripos($type,'/')-strlen($type)+1);
				list(, $imgdata1)      = explode(',', $imgdata1);
				$imgdata1 = base64_decode($imgdata1);
				// print_r('processed data:'.$imgdata);
				file_put_contents('../images/uploads/allproducts/'.$id.'_1.'.$type, $imgdata1);
				$image1=$id.'_1.'.$type;
				$sql .= ", image1='".$image1."'";
			}
			if ($imgdata2){
				list($type, $imgdata2) = explode(';', $imgdata2);
				// print_r('type: '.substr($type,strripos($type,'/')-strlen($type)+1));
				$type=substr($type,strripos($type,'/')-strlen($type)+1);
				list(, $imgdata2)      = explode(',', $imgdata2);
				$imgdata2 = base64_decode($imgdata2);
				// print_r('processed data:'.$imgdata);
				file_put_contents('../images/uploads/allproducts/'.$id.'_2.'.$type, $imgdata2);
				$image2=$id.'_2.'.$type;
				$sql .= ", image2='".$image2."'";
			}
			if ($imgdata3){
				list($type, $imgdata3) = explode(';', $imgdata3);
				// print_r('type: '.substr($type,strripos($type,'/')-strlen($type)+1));
				$type=substr($type,strripos($type,'/')-strlen($type)+1);
				list(, $imgdata3)      = explode(',', $imgdata3);
				$imgdata3 = base64_decode($imgdata3);
				// print_r('processed data:'.$imgdata);
				file_put_contents('../images/uploads/allproducts/'.$id.'_3.'.$type, $imgdata3);
				$image3=$id.'_3.'.$type;
				$sql .= ", image3='".$image3."'";
			}
					
			if ($imgdata4){
				list($type, $imgdata4) = explode(';', $imgdata4);
				// print_r('type: '.substr($type,strripos($type,'/')-strlen($type)+1));
				$type=substr($type,strripos($type,'/')-strlen($type)+1);
				list(, $imgdata4)      = explode(',', $imgdata4);
				$imgdata4 = base64_decode($imgdata4);
				// print_r('processed data:'.$imgdata);
				file_put_contents('../images/uploads/allproducts/'.$id.'_4.'.$type, $imgdata4);
				$image4=$id.'_4.'.$type;
				$sql .= ", image4='".$image4."'";
			}
			$sql .= " WHERE id='".$id."'";				 
			if ($db->query($sql) === TRUE) {
				//  $msg =" Product successfully updated";
				// array_push($res_det, array('msg'=>'Successfully updated for ' . $id));
				array_push($res_det, array('error'=>false,'msg'=>'Successfully updated for ' . $id));
			} 
			else{
				// $msg = "Not Successful for ".$sql;
				// array_push($res_det, array('msg'=>'Error found and not successful for ' . $id));
				array_push($res_det, array('error'=>true,'msg'=>'Error found and not successful for ' . $id));
			}
	} //Authentication ends here
	else // if donot have proper authentication
	{
		array_push($res_det, array('error'=>false,'msg'=>'you do not have  proper credential'));
	}
	//return $msg;
	$db -> close();
	// $res['msg']=$res_det;
	return $res_det;
}

function product_read($id, $category,$alt_category,$price,$product_type,$limit)
{
    $database = new Database();
	$conn = $database->getConnection();
	// echo 'cat'.$category.'price'.$price;
	$sql= "SELECT * FROM `0_products`";
	$sql_limit = "";
	$where = array();
	/**
	 *  If $_POST items are present, sanitize and create SQL
	 */
	if ( $id <> 0 ) {
		$where[] = " id = '".$id."'";
	}
	if ( $category <>0 ) {
		$where[] = "category = '".$category."'";
	}
	if ( $alt_category<>0 ) {
		$where[] = "alt_category = '".$alt_category."'";
	}
	
	if ( $price<>0 ) {
		$p=array(0,10000,20000,50000,100000,500000,10000000);
		$max=$p[$price];
		$min=$p[$price-1];	
		$where[] = "net_price  > ".$min." AND net_price  <=".$max;
	}
	if ( $product_type<>0 ) {
		$where[] = "product_type = '".$product_type."'";
	}
	$where[] = "is_display = '1'"; //show only display is set to true
	/**
	 *  One or more $_POST items were found, so add them to the query
	 */
	if ( sizeof($where) > 0 ) {
		$sql .= ' WHERE '.implode(' AND ', $where).' order by display_order';  
		$sql_limit .= 'SELECT COUNT(*) FROM 0_products tbl1 WHERE '.implode(' AND ', $where).' order by display_order';   
	}
	if($limit<>0)
	{
		$sql_limit = 'SELECT * FROM `0_products` tbl WHERE ('.$sql_limit.')<='.$limit;
		// $sql=$sql_limit;
	}
	// echo 'sql '.$sql.PHP_EOL;
    $result = $conn->query($sql);
    $products = array();
    $res = array(
        'error' => false
    );
    while ($row = $result->fetch_assoc())
    {
        array_push($products, $row);
        // echo 'row:'.$row;    
    }
    $res['products'] = $products;
    $conn->close();
    // header("Content-type: application/json");
    echo json_encode($res);
}
function product_delete($id,$auth_token,$loginID){
    $res_det = array();
    $database = new Database();
    $db = $database->getConnection();

    $sql = "SELECT * FROM 0_users WHERE id = '" . $loginID . "' and auth_token = '" . $auth_token . "'";
    $result = $db->query($sql);
    if ($result->num_rows > 0)
    { //if have proper token
        $sql="DELETE FROM 0_products WHERE id='".$id."'";
        if ($db->query($sql) === true)
        {
            // array_push($err,false);
            // array_push($msg,"Successful for inserting id:".$db->insert_id.' sql:'.$sql.PHP_EOL);
            array_push($res_det, 'Successfully deleted id ' .$id);
        }
        else
        {
            // array_push($err,true);
            // array_push($msg,"Not Successful for ".$sql.PHP_EOL);
            array_push($res_det, 'Not Successfull ' . $sql);
        }

    }
    else // if donot have proper authentication
    {
        array_push($res_det, 'You do not have proper credential');
	}
	$db -> close();
    echo json_encode($res_det);
    return json_encode($res_det);
}

?>