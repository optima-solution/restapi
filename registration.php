<?php 
include_once 'header.php';
//regiestration start
$id                = '';
$name              = '';
$email             = '';
$role              = '';
$website           = '';
$facebook          = '';
$image             = '';
$phone             = '';
$address           = '';
$location          = '';
$email_verified_at = '';
$password          = '';
$remember_token    = '';
$created_at        = '';
$updated_at        = '';
$birth_day         = '';
$marriage_day      = '';
$auth_token        = '';
//regisrtation end

if (get_ses('name')){
   // $name=get_ses('name');
   $loginID=get_ses('loginID');
   $phone=$loginID;
   $data = '[{
         "FunctionType" : "read",
         "id" : "'.$loginID.'"
      }]';
   // echo $data;
   $url=pathUrl('crud/users_c.php');
   // echo $url;
   $make_call = callAPI('POST', $url, $data);
   $response = json_decode($make_call, true);
//    echo 'make_call:'.$make_call.users.'<br/> response'.$response["users"][0]["name"];
   $id                = $response["users"][0]["id"];
   $name              = $response["users"][0]["name"];
   $email             = $response["users"][0]["email"];
   $role              = $response["users"][0]["role"];
   $website           = $response["users"][0]["website"];
   $facebook          = $response["users"][0]["facebook"];
   $image             = $response["users"][0]["image"];
   $phone             = $response["users"][0]["phone"];
   $address           = $response["users"][0]["address"];
   $location          = $response["users"][0]["location"];
   $email_verified_at = $response["users"][0]["email_verified_at"];
   $password          = ltrim(rtrim($response["users"][0]["password"]));
   $remember_token    = $response["users"][0]["remember_token"];
   $created_at        = $response["users"][0]["created_at"];
   $updated_at        = $response["users"][0]["updated_at"];
   $birth_day         = $response["users"][0]["birth_day"];
   $marriage_day      = $response["users"][0]["marriage_day"];
   $auth_token        = $response["users"][0]["auth_token"]; 
}
//Edit Profile php code start
if (isset($_POST['EditUser'])) {
    // echo 'int';
   $err=chkError();
   if(strlen($err)>0){
       echo '<br/>errors:'.$err.'<br/>';
   }
   else{
       EditUser();
   }
}
function EditUser(){
    // echo 'edit initialized';
    // echo  $_FILES['image']['size'];
    $image='';
    if ($_FILES['image']['size'] <> 0 ){
        // echo 'sent<br/>';
        $image=converttob64(); 
        // echo $image;
    }
    $data = '[{
        "FunctionType" : "user_update",
        "loginID" : "'.get_ses('loginID').'",
        "id" : "'.$_POST['email'].'",
        "name" : "'.$_POST['name'].'",
        "email" : "'.$_POST['email'].'",
        "role" : "'.get_ses('role').'",
        "website" : "'.$_POST['website'].'",
        "facebook" : "'.$_POST['facebook'].'",
        "image" : "'.$image.'",
        "phone" : "'.$_POST['phone'].'",
        "address" : "'.$_POST['address'].'",
        "location" : "'.$_POST['location'].'",
        "email_verified_at" : "'.$_POST['email'].'",
        "password" : "'.md5(trim($_POST['password'])).'",
        "remember_token" : "'.$_POST['email'].'",
        "created_at" : "'.$_POST['email'].'",
        "updated_at" : "'.$_POST['email'].'",
        "birth_day" : "'.$_POST['birth_day'].'",
        "marriage_day" : "'.$_POST['marriage_day'].'",
        "auth_token" : "'.get_ses('auth_token').'"
    }]';
    // echo $data;
    $url=pathUrl('crud/users_c.php');
    // echo $url;
    $make_call = callAPI('POST', $url, $data);
    $response = json_decode($make_call, true);
    // echo 'make_call:'.$make_call.'<br/> response'.$response;
    $errors   =$response['response'][0]['errors'];
    $msg   ='msg:'.$response['response'][0]['msg'];
    if ($errors) {
        echo '<br/>errors:'.$errors.'<br/>'.$msg;
    }
    else{
        echo "<br/>successfully updated";
    }
}  

//Edit profile php code end
//registration php code start
if (isset($_POST['register'])) {
   $err=chkError();
   if(strlen($err)>0){
        // $id                = $_POST['email'];
        $name              = $_POST['name'];
        $email             = $_POST['email'];
        // $role              = $_POST['email'];
        $website           = $_POST['website'];
        $facebook          = $_POST['facebook'];
        // $image             = $_POST['image'];
        $phone             = $_POST['phone'];
        $address           = $_POST['address'];
        $location          = $_POST['location'];
        // $email_verified_at = $_POST['email_verified_at'];
        $password          = rtrim(ltrim($_POST['password']));
        // $remember_token    = $_POST['remember_token'];
        // $created_at        = $_POST['created_at'];
        // $updated_at        = $_POST['updated_at'];
        $birth_day         = $_POST['birth_day'];
        $marriage_day      = $_POST['marriage_day'];
        // $auth_token        = $_POST['email'];
       echo '<br/>errors:'.$err.'<br/>';
   }
   else{
    //    print_r($_POST);
		$url = "https://www.google.com/recaptcha/api/siteverify";
		$data = [
			'secret' => "6Lew1r4ZAAAAABllnoVhgDwW85ZCCmtUGqhkSeuS",
			'response' => $_POST['token'],
			// 'remoteip' => $_SERVER['REMOTE_ADDR']
		];

		$options = array(
		    'http' => array(
		      'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
		      'method'  => 'POST',
		      'content' => http_build_query($data)
		    )
		  );

		$context  = stream_context_create($options);
  		$response = file_get_contents($url, false, $context);

        $res = json_decode($response, true);
        // print_r($res);
		if($res['success'] == true) {
            RegisterUser();
        }
   }
   
}
function RegisterUser(){
    $image='';
   // echo  $_FILES['image']['size'];
   if ($_FILES['image']['size'] <> 0 ){
       // echo 'sent<br/>';
       $image=converttob64(); 
       // echo $image;
   }
   $data = '[{
       "FunctionType" : "user_insert",
       "id" : "'.rtrim(ltrim($_POST['email'])).'",
       "name" : "'.$_POST['name'].'",
       "email" : "'.rtrim(ltrim($_POST['email'])).'",
       "role" : "user",
       "website" : "'.$_POST['website'].'",
       "facebook" : "'.$_POST['facebook'].'",
       "image" : "'.$image.'",
       "phone" : "'.rtrim(ltrim($_POST['phone'])).'",
       "address" : "'.$_POST['address'].'",
       "location" : "'.$_POST['location'].'",
       "email_verified_at" : "'.$_POST['email'].'",
       "password" : "'.md5(rtrim(ltrim($_POST['password']))).'",
       "remember_token" : "'.$_POST['email'].'",
       "created_at" : "'.$_POST['email'].'",
       "updated_at" : "'.$_POST['email'].'",
       "birth_day" : "'.$_POST['birth_day'].'",
       "marriage_day" : "'.$_POST['marriage_day'].'",
       "auth_token" : "'.$_POST['email'].'"
   }]';
//    echo $data;
   $url=pathUrl('crud/users_c.php');
   // echo $url;
   $make_call = callAPI('POST', $url, $data);
   $response = json_decode($make_call, true);
   // echo 'make_call:'.$make_call.'<br/> response'.$response;
   $errors   =$response['response'][0]['errors'];
   $msg   ='msg:'.$response['response'][0]['msg'];
   if ($errors) {
       echo '<span style="color: red;"><b>Sorry, we could not register you. Please check your information or contact us.</b></span>'.$msg;
   }
   else{
       echo '<span style="color: green;"><b>You have Successfully registered. Thank you. &nbsp;<a href="login.php" style="color:blue">Log in </a></b></span>'.$msg;
   }
}
function chkError(){
   $err="";
   if ($_POST['password']!=$_POST['repeat_password']){
       $err="Password did not match";
   }
   if (strpos($_POST['email'], '@') == false ){
       $err=$err."<br/>not a valild email".strpos($_POST['email'], '@').$_POST['email'];
   }
   if (strlen($_POST['phone']) <10 ){
       $err=$err.PHP_EOL."<br/>not a valild phone number";
   }
   return $err;
}
function callAPI($method, $url, $data){
   // echo $method, $url,$data;
   $curl = curl_init();
   switch ($method){
      case "POST":
         curl_setopt($curl, CURLOPT_POST, 1);
         if ($data)
            curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
         break;
      case "PUT":
         curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
         if ($data)
            curl_setopt($curl, CURLOPT_POSTFIELDS, $data);			 					
         break;
      default:
         if ($data)
            $url = sprintf("%s?%s", $url, http_build_query($data));
   }
   // OPTIONS:
   curl_setopt($curl, CURLOPT_URL, $url);
   curl_setopt($curl, CURLOPT_HTTPHEADER, array(
      'APIKEY: 111111111111111111111',
      'Content-Type: application/json',
   ));
   curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
   curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
   // EXECUTE:
   $result = curl_exec($curl);
   if(!$result){die("Connection Failure");}
   curl_close($curl);
   return $result;
}

function converttob64()
{
   // echo 'initialized<br/>';
   $errors=array();
   $allowed_ext= array('jpg','jpeg','png','gif');
   $file_name =$_FILES['image']['name'];
   // echo 'fn:'.$file_name.'<br/>';
//   $file_name =$_FILES['image']['tmp_name'];
   // $file_ext = strtolower( end(explode('.',$file_name)));
   $tmp_ext=explode('.',$file_name);
   $file_ext = strtolower(end($tmp_ext));
   // echo $file_ext.'<br/>';
   $file_size=$_FILES['image']['size'];
   // echo $file_size.'<br/>';
   $file_tmp= $_FILES['image']['tmp_name'];
   // echo $file_tmp.'<br>';

   $type = pathinfo($file_tmp, PATHINFO_EXTENSION);
   // echo 'type'.$type.'<br/>';
   $data = file_get_contents($file_tmp);
   // echo 'data:'.$data.'<br/>';
   $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
   // echo "Base64 is ".$base64;

   if(in_array($file_ext,$allowed_ext) === false)
   {
       $errors[]='Extension not allowed';
   }

   if($file_size > 12097152)
   {
       $errors[]= 'File size must be under 10 mb';

   }
   if(empty($errors))
   {
   // if( move_uploaded_file($file_tmp, 'images/'.$file_name));
   // {
   //  echo 'File uploaded';
   // }
   return $base64 = 'data:image/' . $file_ext . ';base64,' . base64_encode($data);
   }
   else
   {
       foreach($errors as $error)
       {
           echo $error , '<br/>'; 
       }
   }
//  print_r($errors);

}

function pathUrl($dir = __DIR__){
   $root = "";
   $dir = str_replace('\\', '/', realpath($dir));
   //HTTPS or HTTP
   $root .= !empty($_SERVER['HTTPS']) ? 'https' : 'http';
   //HOST
   $root .= '://' . $_SERVER['HTTP_HOST'];
   //ALIAS
   if(!empty($_SERVER['CONTEXT_PREFIX'])) {
       $root .= $_SERVER['CONTEXT_PREFIX'];
       $root .= substr($dir, strlen($_SERVER[ 'CONTEXT_DOCUMENT_ROOT' ]));
   } else {
       $root .= substr($dir, strlen($_SERVER[ 'DOCUMENT_ROOT' ]));
   }
   // $root .= '/';
   return $root;
}
//registration php code end
 ?>

<div class="row">
   <div id="registration" class="row center-align" style="width: 100%;"><!-- style="display: none;" -->
        <h4 style="color: royalblue;">Membership Form</h4>
        <h6 style="color: royalblue;">Please submit your information to get our latest offers and eclusive design</h6>
        <form id="userform" style="width: 100%; margin-left: 5%;" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>" enctype="multipart/form-data">
            <div class="row">
                <div class="input-field  col l6 m6 s12">
                    <input name="name" type="text" class="validate" value=" <?php echo $name; ?>"> </input>
                    <label for="name">name<span style="color: red;">&nbsp;*<span></label>
                </div>
                <div class="input-field  col l6 m6 s12">
                    <input name="email" type="email" class="validate" value=" <?php echo $email; ?>"> </input>
                    <label for="email">email<span style="color: red;">&nbsp;*<span></label>
                    <span class="helper-text" data-error="not a valid email" data-success="ok">email</span>
                </div>
                <div class="input-field  col l12 m12 s12 left-align">
                    <label for="image">Image</label><br><br>
                    <img id="img_thumb" style="width: 100px; height: 100px; object-fit:fill;" src="images/uploads/users/<?php echo $image; ?>"/>
                    <input name="image" id="image" type="file" class="validate  black-text" onchange="pre_img();"> </input>
                </div>
                <div class="input-field  col l6 m6 s12">
                    <input name="phone" type="text" class="validate" value=" <?php echo $phone; ?>"> </input>
                    <label for="phone">phone<span style="color: red;">&nbsp;*<span></label>
                </div>
                <div class="input-field  col l6 m6 s12">
                    <input name="address" type="text" class="validate" value=" <?php echo $address; ?>"> </input>
                    <label for="address">address</label>
                </div>
                <div class="input-field  col l6 m6 s12">
                    <input id="password" onkeyup="chkvalue()" name="password" type="password" class="validate"> </input>
                    <label for="password">password<span style="color: red;">&nbsp;*<span></label>
                </div>
                <div class="input-field  col l6 m6 s12">
                    <input id="repeat_password" onkeyup="chkvalue()" name="repeat_password" type="password" class="validate"> </input>
                    <label for="repeat_password"  id="chkpass">Repeat password<span style="color: red;">&nbsp;*<span></label>
                </div>
                <div class="input-field  col l6 m6 s12">
                    <input name="location" type="text" class="validate" value=" <?php echo $location; ?>"> </input>
                    <label for="location">City</label>
                </div>
                <div class="input-field  col l6 m6 s12">
                    <input name="facebook" type="text" class="validate" value=" <?php echo $facebook; ?>"> </input>
                    <label for="facebook">facebook</label>
                </div>
                <div class="input-field  col l6 m6 s12">
                    <input name="website" type="text" class="validate" value=" <?php echo $website; ?>"> </input>
                    <label for="website">website</label>
                </div>
                <div class="input-field  col l6 m6 s12">
                    <input name="birth_day" type="text" class="datepicker validate" value=" <?php echo $birth_day; ?>"> </input>
                    <label for="birth_day">birth_day</label>
                </div>
                <div class="input-field  col l6 m6 s12">
                    <input name="marriage_day" type="text" class="datepicker validate" value=" <?php echo $marriage_day; ?>"> </input>
                    <label for="marriage_day">marriage_day</label>
                </div>
            </div>
            <!-- <div class="g-recaptcha" data-sitekey="6Lew1r4ZAAAAALMwvgYqX2PqZXBDzH1Sh7s7W180"></div>
            <button class="g-recaptcha" 
                data-sitekey="6Lew1r4ZAAAAALMwvgYqX2PqZXBDzH1Sh7s7W180" 
                data-callback='onSubmit' 
                data-action='submit'>Submit</button> -->
                <input type="hidden" id="token" name="token">
            <div class="input-field col s12">
                <?php
                if (get_ses('name')){
                    echo '<button id="EditUser" name="EditUser" class="waves-effect waves-light btn-small" 
                     ><i class="material-icons right">check_box</i>Edit
                    </button>';
                }
                else
                {
                    echo '<button id="register" name="register" class="waves-effect waves-light btn-small"><i class="material-icons right">check_box</i>Register
                    </button>';
                }                   
                ?>   
            </div>
        </form>
    </div>
  </div>

<?php include_once 'footer.php'; ?>
<script src="https://www.google.com/recaptcha/api.js?render=6Lew1r4ZAAAAALMwvgYqX2PqZXBDzH1Sh7s7W180"></script>

<script type="text/javascript">
    //google recaptcha starts
    //  function onSubmit(token) {
    //      console.log('submit');
    //     document.getElementById("userform").submit();
    // }
        grecaptcha.ready(function() {
          grecaptcha.execute('6Lew1r4ZAAAAALMwvgYqX2PqZXBDzH1Sh7s7W180', {action: 'submit'}).then(function(token) {
              console.log(token);
              document.getElementById("token").value = token;
              // Add your logic to submit to your backend server here.
          });
        });
    //google recaptcha ends

    $( ".datepicker" ).datepicker({
        format: 'yyyy/mm/dd', // Default format
        yearRange: [1950,2050]
        }); 
    //previe image as thumbnail
    function pre_img() {
        readURL(document.getElementById("image"), "img_thumb");
        // $("#image").change(function() {readURL(this,"img_thumb");});
            function readURL(input, img_thumb) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        $('#' + img_thumb).attr('src', e.target.result);
                    }
                    reader.readAsDataURL(input.files[0]); // convert to base64 string
                }
            }
        }
    function chkvalue(){
        if (document.getElementById("password").value != document.getElementById("repeat_password").value)
            {
                document.getElementById("chkpass").innerHTML='Not Same';
                document.getElementById("register").disabled = true;
            }
            else
            {document.getElementById("chkpass").innerHTML='Same';}
        document.getElementById("register").disabled = false;
        
    }
</script>
<?php include 'endfooter.php'; ?>  