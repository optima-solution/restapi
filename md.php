<?php include 'header.php' ?>
<style>
#bgdiv{
  /* The image used */
  position: absolute;
  background-image: url("images/uploads/carousel/Jamuna Future Park.jpeg");
  /* Add the blur effect */
  opacity:0.5;
  filter:blur(2px);
  /* Center and scale the image nicely */
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
  z-index: -1;
  width:100%;
}
#imgdiv {
    position:relative;
    width:100%;
    margin:0 auto;
    z-index: 5;
  }
  #imgdiv img {
    box-shadow: 10px 10px 5px grey;
    position:absolute;
    top: 40px;
    -webkit-transition: all 1s ease-in-out;
    -moz-transition: all 1s ease-in-out;
    -o-transition: all 1s ease-in-out;
    transition: all 1s ease-in-out;
  }

.toprighthide {
    opacity:0;
    -webkit-transform:scale(0,0);
    -webkit-transform-origin: top right;
    -moz-transform:scale(0,0);
    -moz-transform-origin: top right;
    -o-transform:scale(0,0);
    -o-transform-origin: top right;
    transform:scale(0,0);
    transform-origin: top right;
  }

  .toprightshow {
    opacity:1;
    -webkit-transform:scale(1,1);
    -webkit-transform-origin: top right;
    -moz-transform:scale(1,1);
    -moz-transform-origin: top right;
    -o-transform:scale(1,1);
    -o-transform-origin: top right;
    transform:scale(1,1);
    transform-origin: top right;
  }

 .bottomlefthide {
    -webkit-transform:scale(0,0);
    -webkit-transform-origin: bottom left;
    -moz-transform:scale(0,0);
    -moz-transform-origin: bottom left;
    -o-transform:scale(0,0);
    -o-transform-origin: bottom left;
    transform:scale(0,0);
    transform-origin: bottom left;
  }
  .bottomlefshow {
    -webkit-transform:scale(1,1);
    -webkit-transform-origin: bottom left;
    -moz-transform:scale(1,1);
    -moz-transform-origin: bottom left;
    -o-transform:scale(1,1);
    -o-transform-origin: bottom left;
    transform:scale(1,1);
    transform-origin: bottom left;
  }
</style>
<div id="root" class="black">
<h5 style="background-color: black; color: white; width:100%; font-size: 30px; text-align:center; position:absolute; top:40px; z-index:100;"><b>PROFILE OF THE  MANAGING DIRECTOR</b></h5>
<div id="imgdiv">
    <div id="bgdiv"></div>
    <img id="img1" width="400" height="600" src="images/uploads/md/md0.jpg">
    <img id="img2" width="400" height="600" src="images/uploads/md/md1.jpg">
  </div>
</div>
<div class="row center-align white-text" style="background-color:#e68a00;">
	<div class="col s12 m12 l12">
		<div style="padding-left: 5%; padding-right: 5%; text-align: justify !important; font-size: 18px; color: #000033;">
			<p>&nbsp;&nbsp;&nbsp;&nbsp; Mrs. Farida Hossain is the Managing Director of Golden World Ltd.  She is a very successful lady enterpreneur, a philantrhopist, social and education worker and a role model in business for women entrepreneurs.<br/>  
            <br/>&nbsp;&nbsp;&nbsp;&nbsp; Mrs. Farida Hossain started her industrial journey in the year 1984 with Rishal Group of Industries. Her personal efficiency as vice chairman of the group raise the company at today's height.<br/> 
            <br/> &nbsp;&nbsp;&nbsp;&nbsp;In the year 2003, she started SHAROJ INTERNATIONAL SCHOOL AND COLLEGE as President of the governing body. Through her excellent dinamism and effective educational proficiency result of all public exams students of Sharoj International Scool and College score 100% pass and a major portion of GPA-5.<br/> 
            <br/>&nbsp;&nbsp;&nbsp;&nbsp;In the year 2010 she started her journey in the fileld of ornament Gold and diamond. Very big advantage of Golden World Jewellers is Mrs. Farida Hossain visited all over the world specially manufacturing hub and moderate and craft. The designs of ornaments updated. With her graduated knowldedge of atristic ornament fashions golden world jewellers achieved today's height.<br/> 
            <br/>&nbsp;&nbsp;&nbsp;&nbsp; She has been elected as Executive Commitee Member of prestigious Bangladesh Jewellery Somity (Bajus). Under her supervision Golden World Ltd. has achieved the gold import license from Bangladesh Bank. Currently only a few companies have got this license.<br/>
            <br/>&nbsp;&nbsp;&nbsp;&nbsp; Her spouse Mr. Sakhawat Hossain is a pioneer industrialist and became the CIP 2 decades ago. He has investment in RMG, Real Estate etc.<br/> 
            <br/>&nbsp;&nbsp;&nbsp;&nbsp;Mrs. Hossain is also a proud mother of a doctor and an engineer. Both of them are working at USA. She has also another daughter and son who are also studying at USA.
			</p>
		</div>
	</div>
</div>
<?php include 'footer.php' ?>

<script src="scripts/vue.js"></script>
        <script src="scripts/axios.min.js"></script>

<script type="text/javascript">
  $(document).ready(function() {
    var counter=1;
    setInterval(function(){ 
    var imnum=counter % 9; 
    var img1src="images/uploads/md/md".concat(imnum).concat(".jpg"); 
    var img2src="images/uploads/md/md".concat(imnum+1).concat(".jpg"); 
    console.log("img1src",img1src);
    $("#img1").attr("src",img1src);
    $("#img2").attr("src",img2src);
    $("#img1").addClass("toprighthide");   
      if ((counter % 4)==1)
        {
          console.log('c',counter);
          $("#img1").removeClass("toprighthide");
          $("#img1").addClass("toprightshow");
          $("#img2").addClass("bottomlefthide");
          $("#img2").removeClass("bottomlefshow");
        }
      else
        {
          console.log('c',counter);
          $("#img1").removeClass("toprightshow");
          $("#img1").addClass("toprighthide");
          $("#img2").removeClass("bottomlefthide");
          $("#img2").addClass("bottomlefshow");
        }
      counter=counter+1;
      
    }, 5000);   
  });
    
    // Register service worker.
    if ('serviceWorker' in navigator) {
        window.addEventListener('load', () => {
            navigator.serviceWorker.register('service-worker.js')
                .then((reg) => {
                    console.log('Service worker registered.', reg);
                });
        });
    }
</script>
<?php include 'endfooter.php';?>