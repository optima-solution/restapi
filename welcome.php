<?php 

include_once 'header.php';
include_once 'config/Database.php';

?>

    <div class="row center-align" style="width: 100%;">
        <h1>Welcome &nbsp;<?php echo ucwords(get_ses('name'));  ?></h1>
        <h5><a href="registration.php">
        <?php  if (get_ses('name'))
            {
                echo 'Edit profile';
            }
        ?>
        </a></h5>
        <a href="index.php"><h3>Go to Home Page</h3></a>
    </div>

    <?php include 'footer.php'; ?>