<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<style>
#bgdiv{
  /* The image used */
  position: absolute;
  background-image: url("images/uploads/carousel/Jamuna Future Park.jpeg");
  /* Add the blur effect */
  opacity:0.1;
  /* Center and scale the image nicely */
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
  z-index: -1;
  height:650px;
  width:100%;
}
#imgdiv {
    position:relative;
    height:650px;
    width:100%;
    margin:0 auto;
    z-index: 5;
  }
  #imgdiv img {
    box-shadow: 10px 10px 5px grey;
    position:absolute;
    left:25%;
    top: 20px;
    -webkit-transition: all 1s ease-in-out;
    -moz-transition: all 1s ease-in-out;
    -o-transition: all 1s ease-in-out;
    transition: all 1s ease-in-out;
  }

.toprighthide {
    opacity:0;
    -webkit-transform:scale(0,0);
    -webkit-transform-origin: top right;
    -moz-transform:scale(0,0);
    -moz-transform-origin: top right;
    -o-transform:scale(0,0);
    -o-transform-origin: top right;
    transform:scale(0,0);
    transform-origin: top right;
  }

  .toprightshow {
    opacity:1;
    -webkit-transform:scale(1,1);
    -webkit-transform-origin: top right;
    -moz-transform:scale(1,1);
    -moz-transform-origin: top right;
    -o-transform:scale(1,1);
    -o-transform-origin: top right;
    transform:scale(1,1);
    transform-origin: top right;
  }

 .bottomlefthide {
    -webkit-transform:scale(0,0);
    -webkit-transform-origin: bottom left;
    -moz-transform:scale(0,0);
    -moz-transform-origin: bottom left;
    -o-transform:scale(0,0);
    -o-transform-origin: bottom left;
    transform:scale(0,0);
    transform-origin: bottom left;
  }
  .bottomlefshow {
    -webkit-transform:scale(1,1);
    -webkit-transform-origin: bottom left;
    -moz-transform:scale(1,1);
    -moz-transform-origin: bottom left;
    -o-transform:scale(1,1);
    -o-transform-origin: bottom left;
    transform:scale(1,1);
    transform-origin: bottom left;
  }
</style>

</head>
<body>

  <div id="imgdiv">
    <div id="bgdiv"></div>
    <img id="img1" width="400" height="600" src="images/uploads/md/md0.jpg">
    <img id="img2" width="400" height="600" src="images/uploads/md/md1.jpg">
  </div>

</body>
<script src="scripts/jquery.min.js"></script>
<script>
  $(document).ready(function() {
    var counter=1;
    setInterval(function(){ 
    var imnum=counter % 9; 
    var img1src="images/uploads/md/md".concat(imnum).concat(".jpg"); 
    var img2src="images/uploads/md/md".concat(imnum+1).concat(".jpg"); 
    console.log("img1src",img1src);
    $("#img1").attr("src",img1src);
    $("#img2").attr("src",img2src);
    $("#img1").addClass("toprighthide");   
      if ((counter % 4)==1)
        {
          console.log('c',counter);
          $("#img1").removeClass("toprighthide");
          $("#img1").addClass("toprightshow");
          $("#img2").addClass("bottomlefthide");
          $("#img2").removeClass("bottomlefshow");
        }
      else
        {
          console.log('c',counter);
          $("#img1").removeClass("toprightshow");
          $("#img1").addClass("toprighthide");
          $("#img2").removeClass("bottomlefthide");
          $("#img2").addClass("bottomlefshow");
        }
      counter=counter+1;
      
    }, 6000);   
  });

</script>
</html>
