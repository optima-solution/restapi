<?php include 'header.php';
$data = '[{
    "FunctionType" : "read"
 }]';
// echo $data;
$url=pathUrl('crud/gold_price_c.php');
// echo $url;
$make_call = callAPI('POST', $url, $data);
$response = json_decode($make_call, true);
//    echo 'make_call:'.$make_call.'<br/> response'.$response["gold_price"][0]["karat"];
$karat1                = $response["gold_price"][0]["karat"];
$price1              = number_format($response["gold_price"][0]["price"]);
$karat2                = $response["gold_price"][1]["karat"];
$price2              = number_format($response["gold_price"][1]["price"]);
$karat3                = $response["gold_price"][2]["karat"];
$price3              = number_format($response["gold_price"][2]["price"]);
$karat4                = $response["gold_price"][3]["karat"];
$price4              = number_format($response["gold_price"][3]["price"]);

function pathUrl($dir = __DIR__){
    $root = "";
    $dir = str_replace('\\', '/', realpath($dir));
    //HTTPS or HTTP
    $root .= !empty($_SERVER['HTTPS']) ? 'https' : 'http';
    //HOST
    $root .= '://' . $_SERVER['HTTP_HOST'];
    //ALIAS
    if(!empty($_SERVER['CONTEXT_PREFIX'])) {
        $root .= $_SERVER['CONTEXT_PREFIX'];
        $root .= substr($dir, strlen($_SERVER[ 'CONTEXT_DOCUMENT_ROOT' ]));
    } else {
        $root .= substr($dir, strlen($_SERVER[ 'DOCUMENT_ROOT' ]));
    }
    // $root .= '/';
    return $root;
 }
 function callAPI($method, $url, $data){
    // echo $method, $url,$data;
    $curl = curl_init();
    switch ($method){
       case "POST":
          curl_setopt($curl, CURLOPT_POST, 1);
          if ($data)
             curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
          break;
       case "PUT":
          curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
          if ($data)
             curl_setopt($curl, CURLOPT_POSTFIELDS, $data);			 					
          break;
       default:
          if ($data)
             $url = sprintf("%s?%s", $url, http_build_query($data));
    }
    // OPTIONS:
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_HTTPHEADER, array(
       'APIKEY: 111111111111111111111',
       'Content-Type: application/json',
    ));
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    // EXECUTE:
    $result = curl_exec($curl);
    if(!$result){die("Connection Failure");}
    curl_close($curl);
    return $result;
 }
?>
<div id="root">
    <div id="car_main" class="carousel carousel-slider center-align car_div_slider black">
        <div class="carousel-item black white-text center-align car_div" v-for="elem in carsels" :key="elem.id">
            <h6>{{elem.heading}}</h6>
            <img id="car_img" class="activator car_img" style="width:100%; object-fit: fill;" v-bind:src="'images/uploads/carousel/'+elem.image">
        </div>
    </div>
    <div id="nav">
        <div class="nav-btn">
            <i id="price_icon" class="material-icons"> chevron_left </i>
            <h6 class="rotate">TODAY GOLD PRICE</h6>
        </div>
        <ul>
            <li>
                <h6>TODAY'S GOLD PRICE</h6>
                    <p>(PER GRAM)</p>
                    <p><?php echo $karat1; ?> -৳ <?php echo $price1; ?></p>
                    <p><?php echo $karat2; ?> -৳ <?php echo $price2; ?></p>
                    <p><?php echo $karat3; ?> -৳ <?php echo $price3; ?></p>
                    <p><?php echo $karat4; ?> -৳ <?php echo $price4; ?></p>
            </li>
        </ul>
    </div>
    
    <div class="row black center-align white-text">
        <div class="col l12 m112 s12">
            <div class="row">
            <h4>Type</h4>
            <div id='prd_type' class="col l2 m4 s6  black-text" v-for="category in prd_type" :key="category.id">
                <a v-bind:href="'product.php?product_type='+ category.id">
                    <div class="card">
                        <div class="card-image waves-effect waves-block waves-light">
                            <img class="activator" v-bind:src="'images/uploads/'+ category.image" style="width:100%; height:150px; object-fit:fill;" />
                            <p>{{category.name}}</p>
                        </div>
                    </div>
                </a>
            </div>
            </div>
        </div>
    </div>

    <div id="car_prd" class="carousel black" style="height: 210px;">
            <!-- <a href="productdetails.php?id={{product.id}}">
            <img class="activator"  v-bind:src="'images/uploads/allproducts/'+product.image"> 
            </a> -->
    </div>
    <div class="black" style="z-index: 3000; position:fixed; top:55px; left:0px;">
      <ul>
        <li>
          <div class="tooltip">
            <a  href="#topring"><i class="fa fa-eercast" style="font-size:12px;color:white"></i></a>
            <a class="tooltiptext" href="#topring">Ear Ring</a>
          </div>
        </li>
        <li>
          <div class="tooltip">
          <a  href="#EarRing"><i class="fa fa-eercast" style="font-size:12px;color:white"></i></a>
            <a class="tooltiptext" href="#EarRing">Ring</a>
          </div>
        </li>
        <li>
          <div class="tooltip">
          <a  href="#Ring"><i class="fa fa-eercast" style="font-size:12px;color:white"></i></a>
            <a class="tooltiptext" href="#Ring">Bala</a>
          </div>
        </li>
        <li>
          <div class="tooltip">
          <a  href="#Bala"><i class="fa fa-eercast" style="font-size:12px;color:white"></i></a>
            <a class="tooltiptext" href="#Bala">Locket</a>
          </div>
        </li>
        <li>
          <div class="tooltip">
          <a  href="#Locket"><i class="fa fa-eercast" style="font-size:12px;color:white"></i></a>
            <a class="tooltiptext" href="#Locket">Necklace</a>
          </div>
        </li>
        <li>
          <div class="tooltip">
          <a  href="#Necklace"><i class="fa fa-eercast" style="font-size:12px;color:white"></i></a>
            <a class="tooltiptext" href="#Necklace">Nose Pin</a>
          </div>
        </li>
        <li>
          <div class="tooltip">
          <a  href="#NosePin"><i class="fa fa-eercast" style="font-size:12px;color:white"></i></a>
            <a class="tooltiptext" href="#NosePin">Taj</a>
          </div>
        </li>
      </ul>
    </div>

        <!-- <div class="row black center-align white-text">
            <div class="col l2 m2 s6">
                <h4>Install</h4>
                <a href="https://youtu.be/Sy-5nRylmAE"><img src="images/install.png" style="width:100%; max-width:150px; height:200px; object-fit:fill;"></a>
            </div>
            <div class="col l12 m112 s12">
                <div class="row">
                <h4>Category</h4>
                <div id='prd_cat_1' class="col l2 m4 s6  black-text" v-for="category in prd_cat" :key="category.id">
                    <a v-bind:href="'product.php?cat='+ category.id">
                        <div class="card">
                            <div class="card-image waves-effect waves-block waves-light">
                                <img class="activator" v-bind:src="'images/uploads/'+ category.image" style="width:100%; height:150px; object-fit:fill;" />
                                <p>{{category.name}}</p>
                            </div>
                        </div>
                    </a>
                </div>
                </div>
            </div>
        </div> -->
        <div class="row center-align black white-text"><h4>BRIEF OF GOLDEN WORLD</h4></div>
        <div class="row center-align black white-text">
            <div class="paralax-about black center-align">
            <img src="images/uploads/branches.jpg" class="materialbox aboutimg">
            <p class="aboutp" style="left:100px; right:10px;"> We are proud to introduce Golden World Ltd. one of the leading Jewellery house, where quality, selection, value, trust, innovative design and servce has been trademark of excellence. Golden World Ltd. started its journey in the year 2010 under the supervision of its founder and Managing Director, <b>Mrs. Farida Hossain</b>. All this have been earned by providing approach and exliminary portfolio of unque and exclusive Diamond, Gold, Knudan and Platinum Jewellery have made us the most sought brand image among the fashionable socialities corporate high fliers, working women, celebrities and media luminaries. We are one of the first company who came up with the concept of online Jewellery selling in the country with the focus to tap the younger generation and also from their near and dear ones. Golden World offers the largest collection of Gold and Diamond Jewellery in the countryr and the customers has the option to scroll the entire range as per their requirement. Our artisans are the best in the world and value of customer attention and satisfaction and we benefit taking in to consideration all aspect of healthy and honest trade practices. The Management of Golden World dreams of making online Jewellery business a success model and make our country and every Bangladeshi proud. As an extenstion of sucessfule relationship of soul mate Golden world is being nurtured as a brand that enriches a woman's life and make us believe that you will celebrate your desire with gold, diamonds and all other jewellery adding beauty to everyone's life
        </p>
        <a href="about.php">read more</a>
        </div>
        <div class="row center-align black white-text"><h4 id="topring">PRODUCT CATEGORY</h4></div>
        <!-- scroll starts -->
        <div class="contentp black white-text">
            <hr/><h4 id="EarRing" class="center-align">Ear Ring</h4><hr/>
            <div class="paralax-image-r black">
                <img src="images/uploads/Ear Ring_cat.jpeg" class="materialbox rm">
                <img src="images/uploads/allproducts/8_0.jpeg" class="materialbox r1 c1">
                <img src="images/uploads/allproducts/9_0.jpeg" class="materialbox r1 c2">
                <img src="images/uploads/allproducts/10_0.jpeg" class="materialbox r2 c1">
                <img src="images/uploads/allproducts/11_0.jpeg" class="materialbox r2 c2">
            </div>
            <hr/><h4 id="Ring" class="center-align">Finger Ring</h4><hr/>
            <div class="paralax-image-r black">
                <img src="images/uploads/Ring_cat.jpeg" class="materialbox rm">
                <img src="images/uploads/allproducts/74_0.jpeg"class="materialbox r1 c1">
                <img src="images/uploads/allproducts/75_0.jpeg" class="materialbox r1 c2">
                <img src="images/uploads/allproducts/76_0.jpeg" class="materialbox r2 c1">
                <img src="images/uploads/allproducts/77_0.jpeg" class="materialbox r2 c2">
            </div>
            <hr/><h4 id="Bala" class="center-align">Bala</h4><hr/>
            <div class="paralax-image-r black">
                <img src="images/uploads/Bange_cat.jpeg" class="materialbox rm">
                <img src="images/uploads/allproducts/11_0.jpeg" class="materialbox r1 c1">
                <img src="images/uploads/allproducts/12_0.jpeg" class="materialbox r1 c2">
                <img src="images/uploads/allproducts/12_0.jpeg" class="materialbox r2 c1">
                <img src="images/uploads/allproducts/14_0.jpeg"class="materialbox r2 c2">
            </div>
            <hr/><h4 id="Locket" class="center-align">Locket</h4><hr/>
            <div class="paralax-image-r black">
                <img src="images/uploads/Locket_cat.jpeg" class="materialbox rm">
                <img src="images/uploads/allproducts/78_0.jpeg" class="materialbox r1 c1">
                <img src="images/uploads/allproducts/79_0.jpeg" class="materialbox r1 c2">
                <img src="images/uploads/allproducts/80_0.jpeg" class="materialbox r2 c1">
                <img src="images/uploads/allproducts/81_0.jpeg" class="materialbox r2 c2">
            </div>
            <hr/><h4 id="Necklace" class="center-align">Necklace</h4><hr/>
            <div class="paralax-image-r black">
                <img src="images/uploads/Necklace_cat.jpeg" class="materialbox rm">
                <img src="images/uploads/allproducts/1_0.jpeg" class="materialbox r1 c1">
                <img src="images/uploads/allproducts/2_0.jpeg" class="materialbox r1 c2">
                <img src="images/uploads/allproducts/3_0.jpeg" class="materialbox r2 c1">
                <img src="images/uploads/allproducts/4_0.jpeg" class="materialbox r2 c2">
            </div>
            <hr/><h4 id="NosePin" class="center-align">Nose Pin</h4><hr/>
            <div class="paralax-image-r black">
                <img src="images/uploads/Nose Pin_cat.jpeg" class="materialbox rm">
                <img src="images/uploads/allproducts/8_0.jpeg" class="materialbox r1 c1">
                <img src="images/uploads/allproducts/9_0.jpeg" class="materialbox r1 c2">
                <img src="images/uploads/allproducts/10_0.jpeg" class="materialbox r2 c1">
                <img src="images/uploads/allproducts/11_0.jpeg" class="materialbox r2 c2">
            </div>
            <hr/><h4 class="center-align">Solid Churi</h4><hr/>
            <div class="paralax-image-r black">
                <img src="images/uploads/Solid Churi_cat.jpeg" class="materialbox rm">
                <img src="images/uploads/allproducts/17_0.jpeg" class="materialbox r1 c1">
                <img src="images/uploads/allproducts/18_0.jpeg" class="materialbox r1 c2">
                <img src="images/uploads/allproducts/19_0.jpeg" class="materialbox r2 c1">
                <img src="images/uploads/allproducts/20_0.jpeg" class="materialbox r2 c2">
            </div>
            <hr/><h4 id="Taj" class="center-align">Taj</h4><hr/>
            <div class="paralax-image-r black">
                <img src="images/uploads/Taj_cat.jpeg" class="materialbox rm">
                <img src="images/uploads/allproducts/86_0.jpeg" class="materialbox r1 c1">
                <img src="images/uploads/allproducts/86_0.jpeg" class="materialbox r1 c2">
                <img src="images/uploads/allproducts/87_0.jpeg" class="materialbox r2 c1">
                <img src="images/uploads/allproducts/88_0.jpeg" class="materialbox r2 c2">
            </div>
        </div>
        <!-- scroll ends-->
        <hr>
        <!---- divvvvvvv -->
        <!-- <div id='catwiseprd' class="row center-align"></div> -->
        <hr>
        <div class="row black center-align white-text">
            <h4>Lifestyles</h4>
            <div id='cat_1' class="col l2 m4 s6 black-text center-align" v-for="category in posts" :key="category.id">
                <a v-bind:href="'product.php?alt_category='+ category.id">
                    <div class="card">
                        <div class="card-image waves-effect waves-block waves-light">
                            <img class="activator car_img Lifestyles_img" v-bind:src="'images/uploads/'+ category.image" style="width:100%; object-fit:fill;" />
                            <p>{{category.name}}</p>
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </div>

    <div class="row black white-text center-align">
        <h3>Our Policy</h3>
        <!-- <div class="row"> -->
        <div class="col s3">
            <div class="center promo promo-example">
                <i class="material-icons large">repeat</i>
                <p class="promo-caption">15 days' return</p>
                <!-- <p class="light center">We did most of the heavy lifting for you to provide a default stylings that incorporate our custom components.</p> -->
            </div>
        </div>
        <div class="col s3">
            <div class="center promo promo-example">
                <i class="material-icons large">stars</i>
                <p class="promo-caption">1 Year Warranty</p>
                <!-- <p class="light center">By utilizing elements and principles of Material Design, we were able to create a framework that focuses on User Experience.</p> -->
            </div>
        </div>
        <div class="col s3">
            <div class="center promo promo-example">
                <i class="material-icons large">brightness_7</i>
                <p class="promo-caption">Life time maintenance</p>
                <!-- <p class="light center">We have provided detailed documentation as well as specific code examples to help new users get started.</p> -->
            </div>
        </div>
        <div class="col s3">
            <div class="center promo promo-example">
                <i class="material-icons large">airport_shuttle</i>
                <p class="promo-caption">Free Transport</p>
                <!-- <p class="light center">We have provided detailed documentation as well as specific code examples to help new users get started.</p> -->
            </div>
        </div>
        <!-- </div> -->
    </div>

    <div class="row black white-text center-align">
        <h5>Customer speaks for us</h5>
        <div id="car_test" class="carousel carousel-slider center" style="min-height: 200px;">
            <div class="carousel-item green black-text center-align" href="#one!">
                <div class="testimonial black-text col s6 m6 l6">
                    <img src="images/male_ex.png" alt="Avatar" style="width:90px">
                    <p><span>Rashedul Islam</span> CEO at Optima Solution.</p>
                    <p>Execellent Design.</p>
                </div>

                <div class="testimonial black-text col s6 m6 l6">
                    <img src="images/fem_ex.png" alt="Avatar" style="width:90px">
                    <p><span>Fahmida Sharmin</span> DMD at Mashreq Bank.</p>
                    <p>Execellent Design.</p>
                </div>
            </div>
            <div class="carousel-item green black-text center-align" href="#one!">
                <div class="testimonial black-text col s6 m6 l6">
                    <img src="images/male_ex.png" alt="Avatar" style="width:90px">
                    <p><span>Lutfur Rahman</span> SEVP, Bay Leasing & Investment Ltd.</p>
                    <p>Modern, Classic, Vintage all available.</p>
                </div>
                <div class="testimonial black-text col s6 m6 l6">
                    <img src="images/fem_ex.png" alt="Avatar" style="width:90px">
                    <p><span>Nargis Sultana</span> Teacher, VNS.</p>
                    <p>Trendy and beautifule.</p>
                </div>
            </div>
        </div>
    </div>
</div>

<?php include 'footer.php';?>
        
<script src="scripts/vue.js"></script>
<script src="scripts/axios.min.js"></script>
<script>
var app = new Vue({
        el: '#root',
        data () {
        return {
            posts: null,
            loading: true,
            errored: false,
            prd_cat: null,
            carsels: null,
            products:null,
            prd_type:[
                {"id":"1","name":"Gold","image":"gold_cat.jpg" },
                {"id":"2","name":"Diamond","image":"diamond_cat.jpg" },
                {"id":"3","name":"Patinum","image":"platinum_cat.jpg" },
                {"id":"4","name":"Pearl","image":"pearl_cat.jpg" }
            ]
        }
        },  
        mounted: 
        function ()  {
            this.getAllCategories();
        },
        methods: {
        getAllCategories: function(){
            axios.get("crud/carousel_c.php?FunctionType=read&&status=show&&pagename=home")
            .then(response => {
                console.log('response1:',response.data.carousels);
                this.carsels = response.data.carousels;     

            }).catch(err => {
                console.log(err)
            });

            axios.get("crud/category_c.php?FunctionType=read&&main_catagory_id=-1")
            .then(response => {
                console.log('posts:',response.data.catagories);
                this.posts = response.data.catagories
            }).catch(err => {
                console.log(err)
            });
            axios.get("crud/category_c.php?FunctionType=read&&main_catagory_id=1")
            .then(response => {
                console.log('response:',response.data.catagories)
                this.prd_cat = response.data.catagories
            }).catch(err => {
                console.log(err)
            });
            axios.get("crud/product_c.php?FunctionType=read")
            .then(response => {
                console.log('products:',response.data.products);
                this.products = response.data.products;
                let mrq_img = '';
                let data=this.products;
                let catwiseprd='';
                let categories=this.prd_cat;
                var cat_counter=[];
                for (let i = 0; i < categories.length; ++i){
                    var a='divprddedt'+categories[i].id;
                    this[a]='';
                    cat_counter[i]=0;
                    console.log(i,'=',cat_counter[i]);
                }
                // console.log('length',data.length);
                for (let i = 0; i < data.length; ++i){
                    // mrq_img += `<img src="images/uploads/allproducts/${data[i].image}" style="width:200px; height: 200px; object-fit:fill;">`;
                    mrq_img += `<a href="productdetails.php?id=${data[i].id}" class="carousel-item"><img class="activator" src="images/uploads/allproducts/${data[i].image}" style="width:200px; height: 200px; object-fit:fill; float:left;"></a>`;  
                    cat_counter[data[i].category]=parseInt(cat_counter[data[i].category])+1;
                    // console.log('catcounter',data[i].category,'=',cat_counter[data[i].category]);
                    // if(cat_counter[data[i].category]<=4)
                        // {
                            var a='divprddedt'+data[i].category;
                            //card image gallery
                            // this[a]+=`
                            //     <div class="col l3 m4 s6 divborder">
                            //         <img class="prd_img" src="images/uploads/allproducts/${data[i].image}" style="object-fit:fill;" />
                            //         Description:${data[i].description}
                            //     </div>`;
                            //card image gallery end
                            //carousel start
                            this[a]+= `<a href="productdetails.php?id=${data[i].id}" class="carousel-item"><img class="prd_img" src="images/uploads/allproducts/${data[i].image}" style="width:200px; height: 200px; object-fit:fill; float:left;"></a>`;  
                            //carousel end
                        // }
                }
                // console.log('mrq:',mrq_img);
                for (let i = 0; i < categories.length; ++i){
                        var a='divprddedt'+categories[i].id;
                        // console.log(this[a]);
                            // catwiseprd +=`<a href="'product.php?cat='+ ${categories[i].id}">
                            //     <div class="card">
                            //         <div class="card-image waves-effect waves-block waves-light">
                            //             <div class="col l3 m3 s3 divborder">
                            //             <h4>${categories[i].name}</h4><br/><hr>
                            //                 <img class="activator" src="images/uploads/${categories[i].image}" style="width:150px; height:150px; object-fit:fill;" />
                            //             </div>
                            //             <div class="col l9 m9 s9">
                            //                 <div class="row">
                            //                 <div id="car_prd1" class="carousel black" style="height: 210px;">
                            //                 `;
                            // catwiseprd +=`<div class="card">
                            //                 <div class="card-image waves-effect waves-block waves-light">
                            //                     <div class="col l3 m3 s3 divborder">
                            //                     <h4>${categories[i].name}</h4><br/><hr>
                            //                         <img class="activator" src="images/uploads/${categories[i].image}" style="width:150px; height:150px; object-fit:fill;" />
                            //                     </div>
                            //                     <div class="col l9 m9 s9">
                            //                         <div class="row">`;

                            catwiseprd +=`<div class="card black white-text">
                                            <div class="card-image waves-effect waves-block waves-light">
                                                <div class="col l3 m3 s3 divborder">
                                                <h4>${categories[i].name}</h4><hr>
                                                    <img class="activator" src="images/uploads/${categories[i].image}" style="width:150px; height:150px; object-fit:fill;" />
                                                </div>
                                                <div class="col l9 m9 s9 black">
                                                    <div class="row black">`;
                            catwiseprd +=`<div id="div`+categories[i].id+ `" class="car_prd1 carousel black" style="height: 210px;"><br/>`;
                            catwiseprd +=  this[a];   
                            // catwiseprd +=  mrq_img;              
                            catwiseprd +=`</div></div></div></div></div>`;
                    }
                $('#car_prd').html(mrq_img);
                $('#catwiseprd').html(catwiseprd);
                // console.log('catwiseprd:',catwiseprd);
                 //start of jquery
                 (function($){
                    $(function(){
                    //MAIN CAROUSEL
                    $('#car_main').carousel({
                        fullWidth: true, 
                        indicators: true,
                        duration: 500//animation duration
                    });
                    setInterval(function() {
                        $('#car_main').carousel('next');
                        }, 4000); // change image every 4 seconds

                    //TESTIMONIAL CAROUSEL
                    $('#car_test').carousel({
                        fullWidth: true, 
                        indicators: true,
                        duration: 500 //animation duration
                    });
                    setInterval(function() {
                        $('#car_test').carousel('next');
                        }, 4000); // change image every 4 seconds

                    //PRODUCT CAROUSEL
                    // $('#car_prd').carousel();
                    $('#car_prd').carousel({
                        shift:50,
                        padding:50,
                    });
                    setInterval(function() {
                        $('#car_prd').carousel('next');
                        }, 4000); // change image every 4 seconds
                    //category wise carousle draft
                    $('.car_prd1').carousel({
                        shift:50,
                        padding:50,
                    });                            
                    setInterval(function() {
                        $('.car_prd1').carousel('next');
                        }, 4000); // change image every 4 seconds 
                    //category wise carousle draft end

                    }); // end of document ready              

                    
                })(jQuery); // end of jQuery name space  

            }).catch(err => {
                console.log(err)
            });
            
            },
    }
    });
</script>

<script type="text/javascript">
    //start of jquery
    (function($){
        $(function(){
            // console.log('ttt');
            $('.materialbox').materialbox();
            //scroll speed
            var onScroll = function() {
            var scrollTop = $(this).scrollTop();
            // console.log(scrollTop);
            // var $el = $(".aboutp");
            // $el.css('top', scrollTop -  $el.offset().top+250);
            // console.log('el ll:'+ (scrollTop -$el.offset().top));
            // var x = document.getElementsByClassName("paralax-image-r")[0].childElementCount;
            // console.log('count:'+x);
            // var arr = document.getElementsByClassName("paralax-image-r");
            // console.log('count main:'+arr.length);
            $('.paralax-image-r').each(function(index, elem) {
            var $elem = $(elem);
            $elem.find('.rm').css('top', scrollTop - $elem.offset().top+250);
            $elem.find('.r1').css('top', scrollTop - $elem.offset().top+250);
            $elem.find('.r2').css('top', scrollTop - $elem.offset().top+400);
            // var c=$elem.find('.r1');
            // console.log('img='+c+' child='+$elem.childNodes);
            });
            
            //scroll speed end
        };
        onScroll.apply(window);
        $(window).on('scroll', onScroll);
            //toggle gold price function
            // $('.fixed-action-btn').floatingActionButton();//gold price button
            $('#nav').stop().animate({
                'margin-right': '-200px'
            }, 1000);

            function toggleDivs() {
                var $inner = $("#nav");
                if ($inner.css("margin-right") == "-200px") {
                    $inner.animate({
                        'margin-right': '0'
                    });
                    $("#price_icon").html('clear')
                } else {
                    $inner.animate({
                        'margin-right': "-200px"
                    });
                    $("#price_icon").html('chevron_left')
                }
            }
            $(".nav-btn").bind("click", function() {
                toggleDivs();
            });

        }); // end of document ready
    })(jQuery); // end of jQuery name space
    

    // Register service worker.
    if ('serviceWorker' in navigator) {
        window.addEventListener('load', () => {
            navigator.serviceWorker.register('service-worker.js')
                .then((reg) => {
                    console.log('Service worker registered.', reg);
                });
        });
    }
</script>
<?php include 'endfooter.php';?>