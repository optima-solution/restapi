<?php
include_once 'header.php';
include_once 'config/Database.php';
// echo 'session data: '.get_ses('role').get_ses('login').get_ses('name');
$phone             = '';
function login($phone,$password) {
    //initialzie session login functions can be found on session.php  
    // echo 'login initiazlized';
    set_ses('login', false);
    set_ses('loginID', null);
    set_ses('name', null);
    set_ses('role', null);
    set_ses('auth_token', null);
    set_ses('phone', null);
      
    $database = new Database();
    $conn     = $database->getConnection();
    $sql      = "SELECT * FROM 0_users WHERE trim(phone) = trim('" . $phone . "') or trim(email) = trim('" . $phone . "')";
    // echo $sql;
    $result   = $conn->query($sql);
    if ($result->num_rows > 0) {
        while ($userinfo = $result->fetch_assoc()) {
            // echo $userinfo["password"].' given '.md5($password);
            if ($userinfo["password"] == md5($password)) {
                // echo $userinfo["password"].'given'.$password;
                set_ses('login', true);
                set_ses('loginID', $userinfo["id"]);
                set_ses('name', $userinfo["name"]);
                set_ses('role', $userinfo["role"]);
                set_ses('phone', $userinfo["phone"]);
                // echo "<script type='text/javascript'>alert('login successful');</script>";
                // Generating 12 digits random number for authorization toke to be used in CRUD API
                $auth_token = generateRandomString(12);
                set_ses('auth_token', $auth_token);
                //update database with authorization toke
                $res_des_auth = $conn->query("Update 0_users set auth_token='" . $auth_token . "' WHERE id = '" . $userinfo["id"] . "'");
                del_ses('danger');
            } else {
                
                // echo $userinfo["password"].'not given'.$password;
                set_ses('danger', 'Please Insert Correct Password.');
                echo "<script type='text/javascript'>alert('Please Insert Correct Password');</script>";
            }
        }
    } else {
        set_ses('danger', 'Please Insert Correct email/phone number.');
        echo "<script type='text/javascript'>alert('Please Insert Correct email/phone number');</script>";
    }
    
    if (get_ses('login') != null && get_ses('login') == true) {
        // header('location: welcome.php');
        echo "<script>window.location.href = 'welcome.php';</script>";
    }
}

if (isset($_POST['login'])) {
      //phone or required for login id
    $phone    = isset($_POST['phone']) ? trim($_POST['phone']) : set_ses('danger', 'Please Insert Correct email/phone number.');
    $password = isset($_POST['password']) ? trim($_POST['password']) : set_ses('danger', 'Please Insert Correct Password.');
    login($phone,$password);
}
//Generate random for authorization token
function generateRandomString($length = 10)
{
    $characters       = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString     = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}
?>

    <div class="row center-align" style="width: 100%;">
        <h5>Member Login</h5>
        <div id="login" class="row center-align">
            <form style="width: 80%; margin-left: 7%;" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
                <div class="row">
                    <div class="input-field col s12 m12 l12">
                        <i class="material-icons prefix">contact_phone</i>
                        <input name="phone" type="text" class="validate" value=" <?php echo $phone; ?> "> </input>
                        <label for="phone">Email/Phone Number</label>
                    </div>
                    <div class="input-field col s12  s12 m12 l12">
                        <i class="material-icons prefix">visibility</i>
                        <input name="password" type="password" class="validate"></input>
                        <label for="password">password</label>
                    </div>

                </div>
                <div class="row">
                    <div class="input-field col s12">
                        <input type="checkbox" />
                        <span style="padding-right:20px;">Remember me</span>  
                        <button name='login' class="waves-effect waves-light btn-small">
                            <i class="material-icons right">check_box</i>login
                        </button>
                    </div>
                </div>
                <div class="input-field col s12">
                    <a id="showreg"  href="registration.php"> Not a Member? <br/>Please Register to get our exclusive offers</a>
                </div>
            </form>
        </div>
        <!-- `id`, `name`, `email`,  `facebook`, `image`, `phone`, `address`, `location`,  `password`, `website`, `birth_day`, `marriage_day` -->
    </div>

    <?php include 'footer.php'; ?>
    <?php include 'endfooter.php' ?>