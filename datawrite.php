<?php include 'header.php';?>
 	 	<div>
			<label for="name">Name</label><br>
			<input type="text" id="name" name="name" value="" /><br>
			<label for="name">description</label><br>
			<input type="text" id="description" name="description" value="" /><br>
			<label for="name">price</label><br>
			<input type="text" id="price" name="price" value="" /><br>
			<label for="category_id">category_id</label><br>
			<input type="text" id="category_id" name="category_id" value="" /><br><br>
			<input type="file" id="img_file" accept="image/x-png,image/jpeg,image/gif" /><br/>
			<img id="img_thumb" style="width: 100px; height: 100px; object-fit:fill;"/>
			<br/>
			<button id="btnsave" style="width: 100px;">Save</button><br/>
			<!-- <button id="btntest" style="width: 100px;">Test</button><br/> -->
			<div id="panelBody"></div>
		</div>
	</main>
		
  <script src="scripts/jquery.min.js"></script>
  <script src="scripts/materialize.min.js"></script>
  <script src="scripts/app.js"></script>
  <script src="scripts/install.js"></script>
	<script>
		// document.getElementById('btntest').addEventListener('click', SavePhoto);
		// document.getElementById('img_file').addEventListener('change', pre_img);
	//preview image function
		$("#img_file").change(function() {readURL(this);});
		function readURL(input) {
		  if (input.files && input.files[0]) {
		    var reader = new FileReader();
		    
		    reader.onload = function(e) {
		      $('#img_thumb').attr('src', e.target.result);
		    }
		    
		    reader.readAsDataURL(input.files[0]); // convert to base64 string
		  }
		}
		
		async function SavePhoto() 
		{
		    let photo = document.getElementById("img_file").files[0];

		    let formData = new FormData();
	   	         
		    formData.append("upfile", photo);
		    // console.log(formData);
		      // const form = new FormData(document.querySelector('#profileData'));

			  const url = 'items/imgupload.php'
			   fetch(url, {
				    method: 'POST',
				    body: formData,
				  }).then(response => {
				    console.log(response)
				  })
		}

		function init() {
		    initializeServiceWorker();
		    initializeDB();
		    checkIndexedDB();
		}

		init();

		function initializeServiceWorker() {
		    if(navigator.serviceWorker) {
		        navigator.serviceWorker.register('service-worker.js')
		        .then(function() {
		        	console.log('service worker ready');
		            return navigator.serviceWorker.ready
		        })
		        .then(function(registration) {
		            document.getElementById('btnsave').addEventListener('click', (event) => {
		                event.preventDefault();
		                saveData().then(function() {
		                	// console.log('saving data');
		                    if(registration.sync) {
		                    	//console.log('example-synced');
		                        registration.sync.register('item-sync')
		                        //clearing data	
		                        .catch(function(err) {
		                        	console.log('sync err: ',err);
		                            return err;
		                        })
		                    } else {
		                        // sync isn't there so fallback
		                        //console.log('checking internet');
		                        checkInternet();
		                    }
		                });
		            })
		        })
		    } else {
		        document.getElementById('btnsave').addEventListener('click', (event) => {
		            event.preventDefault();
		            saveData().then(function() {
		                checkInternet();
		            });
		        })
		    }
		}

		function initializeDB() {
		    var newsletterDB = window.indexedDB.open('itemDB');

		    newsletterDB.onupgradeneeded = function(event) {
		        var db = event.target.result;

		        var itemTbl = db.createObjectStore("itemTbl", { autoIncrement: true });
		        itemTbl.createIndex("id", "id", { unique: true });
		        itemTbl.createIndex("name", "name", { unique: false });
		        itemTbl.createIndex("description", "description", { unique: false });
		        itemTbl.createIndex("price", "price", { unique: false });
		        itemTbl.createIndex("category_id", "category_id", { unique: false });
		        itemTbl.createIndex("img_file", "img_file", { unique: false });
		    }
		}

		function checkIndexedDB() {
		    if(navigator.onLine) {
		        console.log('online');
		        var newsletterDB = window.indexedDB.open('itemDB');
		        // console.log('db reading');
		        newsletterDB.onsuccess = function(event) {
		        	// console.log('still reading');
		            this.result.transaction("itemTbl").objectStore("itemTbl").getAll().onsuccess =async function(event) { //success start
						// console.log(JSON.stringify(event.target.result));
		                 window.fetch('items/create.php', {
		                        method: 'POST',
		                        headers: {
							      "Content-type": "application/json; charset=UTF-8"
							    },
							    body: JSON.stringify(event.target.result)
		                    }).then(function(rez) {
		                    	// console.log(rez);
		                        return rez.text();
		                    }).then(function(response) {
		                    	//console.log(response);
		                        newsletterDB.result.transaction("itemTbl", "readwrite")
		                        .objectStore("itemTbl")
		                        .clear();
		                    }).catch(function(err) {
		                        console.log('err ', err);
		                    })
		            }; //success end
		        };
		    }
		}
		
		function getfiledata(){
			var input = document.getElementById('img_file');
			var blob="";
			if (input.value){
				console.log('img file checking');
				file = input.files[0],	
				reader = new FileReader();
				reader.onloadend = function () {
					blob = reader.result;
					// console.log('int blob:',blob);
				}
				reader.readAsDataURL(file); 
			}
			else {
					blob="cc";
			}
		}

		function saveData() {
	    return new Promise(function(resolve, reject) {

	    	var input = document.getElementById('img_file');
			var file = input.files[0],	
			reader = new FileReader();
			var blob="";
			var msg='';
			if (input.value){
				// console.log('img file checking');
				reader.onloadend = function () {
					//blob = reader.result;
					var tmpObj = {
		            FunctionType: 'item_insert',
		            name: document.getElementById('name').value,
		            description: document.getElementById('description').value,
		            price: document.getElementById('price').value,
		            category_id: document.getElementById('category_id').value,
		            img_file: reader.result
		        	};
		        	// console.log('file:',tmpObj);
					var myDB = window.indexedDB.open('itemDB');

					myDB.onsuccess = function(event) {
						var objStore = this.result.transaction('itemTbl', 'readwrite').objectStore('itemTbl');
						objStore.add(tmpObj);
						msg='successfully';
						resolve();
					}

					myDB.onerror = function(err) {
						msg='error';
						reject(err);
					}
				}
				reader.readAsDataURL(file); 
				alert('Product Saved',msg);
			}
			else {
				var tmpObj = {
		            name: document.getElementById('name').value,
		            description: document.getElementById('description').value,
		            price: document.getElementById('price').value,
		            category_id: document.getElementById('category_id').value,
		            img_file: ""
		        	};
		        	console.log(tmpObj);
					var myDB = window.indexedDB.open('itemDB');

					myDB.onsuccess = function(event) {
						var objStore = this.result.transaction('itemTbl', 'readwrite').objectStore('itemTbl');
						objStore.add(tmpObj);
						resolve();
					}
					myDB.onerror = function(err) {
						reject(err);
					}
			}      
	    

	   	 })
		}

		function checkInternet() {
		    event.preventDefault();
		    if(navigator.onLine) {
		          console.log('online');
		    } else {
		        alert("You are offline! When your internet returns, we'll finish up your request.");
		    }
		}

		window.addEventListener('offline', function() {
		    alert('You have lost internet access!');
		});

	</script>

</body>

</html>
