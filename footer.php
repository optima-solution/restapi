<!-- <div class="container black white-text center-align"> -->

  <footer class="black page-footer center-align">
    <div class="row black center-align white-text">
        <h4>Locate us</h4>
        <div class="col m6 s12">
          <h5>Gulshan Branch</h5><br/>
          <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3650.7367521562605!2d90.4136431144566!3d23.792386793089047!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3755c71297aaeb7d%3A0x7cce4c02d87c303e!2sPink%20City!5e0!3m2!1sen!2sbd!4v1586317010224!5m2!1sen!2sbd" width="300" height="250" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0">
          </iframe>
          <h6>Pink City (2nd Floor), <br/>Gulshan, Dhaka</h6><br/>
        </div>
        <div class="col m6 s12">
          <h5>Jamuna Branch</h5><br/>
          <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3650.1651959785727!2d90.42080137716151!3d23.812724043386332!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3755c62fb95f16c1%3A0xb333248370356dee!2sJamuna%20Future%20Park!5e0!3m2!1sen!2sbd!4v1586317187198!5m2!1sen!2sbd" width="300" height="250" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0">
          </iframe>
          <h6>Jamuna Future Park (2nd Floor), <br> Bashundhara R/A, Dhaka</h6><br/>
        </div>
    </div>
      <div class="row black center-align white-text">
        <div class="col s6 m3 l3 center-align">
          <h5 class="white-text">About Us</h5>
          <ul>
            <li><a class="grey-text text-lighten-3" href="about.php">About us</a></li>
            <li><a class="grey-text text-lighten-3" href="md.php">Managing Director's Profile</a></li>
            <li><a class="grey-text text-lighten-3" href="message.php">Managing Director's Message</a></li>
            <li><a class="grey-text text-lighten-3" href="testimoninals.php">Our Certifications</a></li>
            <li><a class="grey-text text-lighten-3" href="contactus">Stores</a></li>

          </ul>
        </div>
        <div class="col s6 m3 l3 center-align">
          <h5 class="white-text">CUSTOMER DELIGHTS</h5>
          <ul>
            <li><a class="grey-text text-lighten-3" href="registration.php">Registration</a></li>  
            <li><a class="grey-text text-lighten-3" href="product.php">Products</a></li>
            <li><a class="grey-text text-lighten-3" href="gallery.php">Gallery</a></li>
            <li><a class="grey-text text-lighten-3" href="career.php">Careers</a></li>
            <li><a class="grey-text text-lighten-3" href="faq.php">FAQ</a></li> 
          </ul>
        </div>
        <div class="col s6 m3 l3 center-align">
          <h5 class="white-text">POLICIES</h5>
          <ul>
            <li><a class="grey-text text-lighten-3" href="#!">15 Days Returns</a></li>
            <li><a class="grey-text text-lighten-3" href="#!">Free Shipping</a></li>
            <li><a class="grey-text text-lighten-3" href="#!">Lifetime Exchange</a></li>
            <li><a class="grey-text text-lighten-3" href="#!">Franchisee</a></li>
            <li><a class="grey-text text-lighten-3" href="#!">Certified Jewellery</a></li>
          </ul>
        </div>
        <div class="col s6 m3 l3 center-align">
          <h5 class="white-text">OTHERS</h5>
          <ul>
            <li><a class="grey-text text-lighten-3" href="#!">Privilege Cards</a></li>
            <li><a class="grey-text text-lighten-3" href="#!">Ring Size</a></li>
            <li><a class="grey-text text-lighten-3" href="#!">Gift Card</a></li>
            <li><a class="grey-text text-lighten-3" href="#!">Jewellery Care</a></li>
          </ul>
        </div>
      </div>
    <div>We accept credit cards <br/>
      <i class="fa fa-cc-amex" style="font-size:48px;"></i>&nbsp;&nbsp;
      <i class="fa fa-cc-mastercard" style="font-size:48px;"></i>&nbsp;&nbsp;
      <i class="fa fa-cc-visa" style="font-size:48px;"></i> 
    </div>        
    <div class="footer-copyright">
      <div class="container"><span style="padding-right: 20px;">©2020 Copyright</span>
       <a class="grey-text text-lighten-4 right" href="http://optima-solution.com"> Optima Solution</a>
      </div>
    </div class="red-text"><b>DISCLAIMER: THIS WEB SITE IN UNDER CONSTRUCTION...</b></div>
    </div>
  </footer>
<!-- </div> -->

    
    <!-- <div class="fixed-action-btn" style="z-index: 2000;">
      <button class="btn-floating btn-large red" onclick="showhidecart()">
        <i class="large material-icons">add_shopping_cart</i>
      </button>
    </div> -->
    <div class="fixed-action-btn" style="z-index: 2000;">
      <button class="btn-floating btn-large red" onclick="showhidecart()">
        <i class="large material-icons">add_shopping_cart</i>
      </button>
      <a class="btn-floating btn-large blue modal-trigger"
      <?php if (get_ses('login') != null && get_ses('login') == true) {
      echo ' href="#chatmodal">';}
      else {
      echo ' href="#loginmodal">';
          } ?>
        <i class="large material-icons">chat</i>
      </a>
    </div>
    <div class="modal wrapper" style="width:30%; height: 70%;" id="loginmodal">
      <div class="modal-content">
        <div class="row">
          <div class="col l6 left-align chattitle">
            <h6>Please login</h6>
            <!-- <h6 id="Msg"></h6> -->
          </div>
          <div class="col l6 right-align">
            <button class="modal-close waves-green black right-align btn-small">X</button>
          </div>
        </div>
      </div>
    </div>
    <div class="modal wrapper" id="chatmodal">
      <div class="modal-content">
          <div class="row chattitle">
            <div class="col l6 left-align">
              <h6>Q & A (under construction..)</h6>
              <!-- <h6 id="Msg"></h6> -->
            </div>
            <div class="col l6 right-align">
              <button class="modal-close waves-green blue right-align btn-small">X</button>
            </div>
          </div>
          <div class="form">
            <div class="bot-inbox inbox row">
                  <div class="col l10 m10 s10 msg-headersystem">
                      <p>GW: Hello there, how can I help you?</p>
                  </div>
              </div>
          </div><!--row--> 
      </div><!--modal-content row--> 
      <div class="typing-field">
        <div class="input-data">
            <input id="data" type="text" placeholder="Type something here.." required>
            <button id="send-btn">Send</button>
        </div>
      </div>
  </div><!--addmodal-->
    <div id="cart"  class="cart grey white-text">
    </div>
    <!--ribbon-->
    <div class="row ribbonfooter black white-text center-align hide-on-med-and-up">
      <div class="col s3 m3 l3">
        <a href="product.php"><i class="material-icons">search</i></a>
      </div>
      <div class="col s3 m3 l3">
        <a href="registration.php"><i class="material-icons">person</i></a>
      </div>
      <div class="col s3 m3 l3">
        <i class="material-icons">add_shopping_cart</i>
      </div>
      <div class="col s3 m3 l3">
        <i class="material-icons">favorite_border</i>
      </div>  
    </div>

    <script src="scripts/app.js"></script>
    <script src="scripts/install.js"></script>
    </main>
