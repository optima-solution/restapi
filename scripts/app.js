//RASHED'S CODE FOR PWA AND OFFLINE DATA SYNCHRONIZATION
// 'use strict';

(function($){
  $(function(){
    $('.modal').modal();
    $('.sidenav').sidenav();//SIDE NAV IS THE STACKED MENU IN  THE LEFT
    $('.fixed-action-btn').floatingActionButton(); //THE CART BUTTON IN THE RIGHT BOTTOM
  //chat code
  $("#send-btn").on("click", function(){
    $value = $("#data").val();
    $msg = '<div class="user-inbox inbox"><div class="msg-header"><p>Me: '+ $value +'</p></div></div>';
    $(".form").append($msg);
    $("#data").val('');
    
    // start ajax code
    $.ajax({
        url: 'crud/chatmsg.php',
        type: 'POST',
        data: 'functiontype=read&&text='+$value,
        success: function(result){
          if(result.includes('Please provide your name and cell no')){
            $reply = '<div class="msg-header"><p>'+ result +'</p></div>';
            $(".form").append($reply);
            $("#data").append($reply);
            // when chat goes down the scroll bar automatically comes to the bottom
            $(".form").scrollTop($(".form")[0].scrollHeight);
          }
          else{
            $reply = '<div class="msg-headersystem"><p>'+ result +'</p></div>';
            $(".form").append($reply);
            // when chat goes down the scroll bar automatically comes to the bottom
            $(".form").scrollTop($(".form")[0].scrollHeight);
          }
            
        }
    });
});
  //chat code ends
  
  }); // end of document ready
})(jQuery); // end of jQuery name space

init();

function init() {
  //displayNotification();
}

Notification.requestPermission(function(status) {
    console.log('Notification permission status:', status);
});

function displayNotification() {
  if (Notification.permission == 'granted') {
    navigator.serviceWorker.getRegistration().then(function(reg) {
      reg.showNotification('Hello world!');
    });
  }
}

const channel = new BroadcastChannel('sw-messages');
channel.addEventListener('message', event => {
  console.log('Received', (event.data),event.data);
  // alert('response from server:', event.data);
  document.getElementById('Msg').innerHTML=JSON.stringify(event.data);
});

function currentdatetime(){
  var today = new Date();
  var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
  var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
  var dateTime = date+' '+time;
  return dateTime;
}

//THE FUNCTION SHALL WRITE CART HTML CODE
function writehtmlforcart(){
  // alert('show cart');
  var ordertxt='';
  var html='';
  var total_price=0;
  var counter=0;
  for (i=0;i<localStorage.length;i++){
    
    // console.log('obj.name',obj.name);
    // console.log('key',localStorage.key(i),'prd',localStorage.key(i).substring(0,7));
    if (localStorage.key(i).substring(0,7)=='product')
      {
      var obj = JSON.parse(localStorage.getItem(localStorage.key(i)));
      console.log(obj.name,'original price',obj.original_price)
      total_price= total_price + parseInt(obj.original_price);
      console.log('total price',total_price)
      html  = html + '<tr><td><img style="width:30px; height:30px; object-fit:fill;" src="images/uploads/allproducts/'+obj.image+'"><br/>'+obj.name+'</td><td>'+numberWithCommas(obj.original_price)+'</td><td><button onclick="deletecart(\''+localStorage.key(i)+'\')">X</button></td></tr>';
      ordertxt=obj.name+','+ordertxt;
      counter=counter+1;
      // console.log('localstorage',i,localStorage.getItem(localStorage.key(i)));

      }
    }
  html= html+'</table><br/><br/><br/>';
  html=html+'<form  method="post" action="product.php" enctype="multipart/form-data">';
  html= html+'<button id="place_order" name="place_order"  class="waves-effect waves-light btn-small">Place Order</button><span><b>&nbsp;Tk.'+numberWithCommas(total_price)+'</b></span>';
  html=html+'<input type="hidden" id="orders" name="orders" value="'+ordertxt+'">';
  html=html+'</form>';
  html='<h6>CART &nbsp;&nbsp;'+counter+'&nbsp;items</h6><br/><table class="striped">'+html;
  console.log('html',html);
  $("#cart").html(html);
}

function showcart(){
  writehtmlforcart();
  $("#cart").css("display", "block");
}

function hidecart(){
  var $display =  $("#cart").css("display");
  $("#cart").css("display", "block");
}

function showhidecart(){
  writehtmlforcart();
  var $display =  $("#cart").css("display");
  if($display=="none") {$("#cart").css("display", "block");}
  else {$("#cart").css("display", "none");}
}

function numberWithCommas(x) {
  return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function deletecart(key){
  console.log('deletecart:',key);
  localStorage.removeItem(key);
  writehtmlforcart();
}

//SEND MESSAGE TO VIBER NUMBER FOR SUBMITTING CART ITEM FOR PURCHASE
function sendViberMsg() {
  console.log('sending Message');

    var myHeaders = new Headers();
      myHeaders.append("X-Viber-Auth-Token", "4bf78675af27dd13-4dba5372cf39ba2b-c67d668b850afd65");
      myHeaders.append("Content-Type", "text/plain");
      myHeaders.append("Access-Control-Allow-Origin", "*");

      var raw = '{"receiver":"mF3DiyunQnRnEN6wnWBDLw==","min_api_version":1,"sender":{"name":"rashed"},      "tracking_data":"tracking data","type":"text","text":"Hello world!"}';

      var requestOptions = {
        method: 'POST',
        headers: myHeaders,
        body: raw,
        redirect: 'follow'
      };

      fetch("https://chatapi.viber.com/pa/send_message", requestOptions)
        .then(response => response.text())
        .then(result => console.log(result))
        .catch(error => console.log('error', error));
}

// RESIZE IMAGE FUNCTION RESIZE IMAGE CANVAST TO A PROVIDED HEIGHT AND WIDTH
function ResizeImage(base64image,width,height) {
  return new Promise(function(resolve,reject){
    var base64ResizedImage = null;
    let img = new Image();
    img.src = base64image;

    img.onload = () => {
        // height=400;
        // width=400;
        // Check if the image require resize at all
        if(img.height <= height && img.width <= width) {
            // base64ResizedImage = img;
            // console.log('r1',base64ResizedImage);
            let resizingCanvas = document.createElement('canvas');
            let resizingCanvasContext = resizingCanvas.getContext("2d");

            resizingCanvas.width = img.width;
            resizingCanvas.height = img.height;

            resizingCanvasContext.drawImage(img, 0, 0, resizingCanvas.width, resizingCanvas.height);

            // output the canvas pixels as an image. params: format, quality
            base64ResizedImage = resizingCanvas.toDataURL('image/jpeg', 0.85);

            // TODO: Call method to do something with the resize image
        }
        else {
            // Make sure the width and height preserve the original aspect ratio and adjust if needed
            if(img.height > img.width) {
                width = Math.floor(height * (img.width / img.height));
            }
            else {
                height = Math.floor(width * (img.height / img.width));
            }

            let resizingCanvas = document.createElement('canvas');
            let resizingCanvasContext = resizingCanvas.getContext("2d");

            // Start with original image size
            resizingCanvas.width = img.width;
            resizingCanvas.height = img.height;


            // Draw the original image on the (temp) resizing canvas
            resizingCanvasContext.drawImage(img, 0, 0, resizingCanvas.width, resizingCanvas.height);

            let curImageDimensions = {
                width: Math.floor(img.width),
                height: Math.floor(img.height)
            };

            let halfImageDimensions = {
                width: null,
                height: null
            };

            // Quickly reduce the dize by 50% each time in few iterations until the size is less then
            // 2x time the target size - the motivation for it, is to reduce the aliasing that would have been
            // created with direct reduction of very big image to small image
            while (curImageDimensions.width * 0.5 > width) {
                // Reduce the resizing canvas by half and refresh the image
                halfImageDimensions.width = Math.floor(curImageDimensions.width * 0.5);
                halfImageDimensions.height = Math.floor(curImageDimensions.height * 0.5);

                resizingCanvasContext.drawImage(resizingCanvas, 0, 0, curImageDimensions.width, curImageDimensions.height,
                    0, 0, halfImageDimensions.width, halfImageDimensions.height);

                curImageDimensions.width = halfImageDimensions.width;
                curImageDimensions.height = halfImageDimensions.height;
            }

            // Now do final resize for the resizingCanvas to meet the dimension requirments
            // directly to the output canvas, that will output the final image
            let outputCanvas = document.createElement('canvas');
            let outputCanvasContext = outputCanvas.getContext("2d");

            outputCanvas.width = width;
            outputCanvas.height = height;

            outputCanvasContext.drawImage(resizingCanvas, 0, 0, curImageDimensions.width, curImageDimensions.height,
                0, 0, width, height);

            // output the canvas pixels as an image. params: format, quality
            base64ResizedImage = outputCanvas.toDataURL('image/jpeg', 0.85);
            // console.log(base64ResizedImage);

            // TODO: Call method to do something with the resize image
        }
        // console.log(base64ResizedImage);
        resolve (base64ResizedImage);
    };
  })
}

//CAPTCHA FUNCTION STARTS
function Captcha(){
  var alpha = new Array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z');
  var i;
  for (i=0;i<6;i++){
    var a = alpha[Math.floor(Math.random() * alpha.length)];
    var b = alpha[Math.floor(Math.random() * alpha.length)];
    var c = alpha[Math.floor(Math.random() * alpha.length)];
    var d = alpha[Math.floor(Math.random() * alpha.length)];
    var e = alpha[Math.floor(Math.random() * alpha.length)];
    var f = alpha[Math.floor(Math.random() * alpha.length)];
    var g = alpha[Math.floor(Math.random() * alpha.length)];
  }
  var code = a + ' ' + b + ' ' + ' ' + c + ' ' + d + ' ' + e + ' '+ f + ' ' + g;
  document.getElementById("mainCaptcha").value = code
}
function ValidCaptcha(){
  var string1 = removeSpaces(document.getElementById('mainCaptcha').value);
  var string2 = removeSpaces(document.getElementById('txtInput').value);
  if (string1 == string2){
    return true;
  }
  else{        
    return false;
  }
}
function removeSpaces(string){
  return string.split(' ').join('');
}
//CAPTCHA FUNCTION ENDS