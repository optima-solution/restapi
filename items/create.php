<?php
header("Access-Control-Allow-Origin: *");
// header("Content-Type: application/json; charset=UTF-8");
header("Content-Type: application/json;");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
 
include_once '../config/Database.php';

// include_once '../class/Items.php';
// $items = new Items($db);
// include_once '../class/Products.php';
// $products = new Products($db);


$json = file_get_contents('php://input');
$data = json_decode($json,true); // convert to php array
  
// foreach ($_POST['items'] as  $value) {
foreach ($data as $key => $value) {
    // $msg .= 'name: ' . $value['name'] . ' -> description: ' . $value['description'] . PHP_EOL;
	// print_r($value);
	if ($value['FunctionType']=='product_insert'){
		echo json_encode(product_insert($value));
	}
	else if ($value['FunctionType']=='item_insert'){
		echo json_encode(product_insert($value));
	}
	else if ($value['FunctionType']=='category_insert'){
		echo json_encode(category_insert($value));
	}
}
		
function category_insert($value){

	// include_once '../class/Category.php';
	$database = new Database();
	$db = $database->getConnection();

	// $category = new category($db);

	// $category->name=addslashes($value['name']);
	// $category->description=addslashes($value['description']);
	// $category->main_catagory_id=addslashes($value['main_catagory_id']);
	// $category->created_at=date('Y-m-d H:i:s');
	// $category->updated_at=date('Y-m-d H:i:s');

	$name=addslashes($value['name']);
	$description=addslashes($value['description']);
	$main_catagory_id=addslashes($value['main_catagory_id']);
	$alt_category=addslashes($value['alt_category']);
	$created_at=date('Y-m-d H:i:s');
	$updated_at=date('Y-m-d H:i:s');


	$imgdata=$value['image'];
	if ($imgdata){
				list($type, $imgdata) = explode(';', $imgdata);
				print_r('type: '.substr($type,strripos($type,'/')-strlen($type)+1));
				$type=substr($type,strripos($type,'/')-strlen($type)+1);
				list(, $imgdata)      = explode(',', $imgdata);
				$imgdata = base64_decode($imgdata);
				// print_r('processed data:'.$imgdata);
				file_put_contents('../images/uploads/'.addslashes($value['name']).'_cat.'.$type, $imgdata);
				$image=addslashes($value['name']).'_cat.'.$type;
			}
			else{
				$image='noimg.png';
			}
	// $category->image=$image;
	// if(!empty($category->name) && !empty($category->image))
	// {
	//     if($category->create()){         
	//         http_response_code(201);         
	//         echo json_encode(array("message" => "category was created."));
	//     } else{         
	//         http_response_code(503);        
	//         echo json_encode(array("message" => "Unable to create category."));
	//     }
	// }else{    
	//     http_response_code(400);    
	//     echo json_encode(array("message" =>"Unable to create category. Data is incomplete."));
	//     // echo "Unable to create item. Data is incomplete.";
	// }

	
	$msg ='';
	 $sql = "INSERT INTO catagories (`main_catagory_id`,`name`, `image`, `description`, `created_at`, `updated_at`,`alt_category`)
	    VALUES('".$main_catagory_id."', '".$name."', '".$description."', '".$image."', '".$created_at."', '".$updated_at."', '".$alt_category."')";
	    $msg .='insert: '.$sql;

	    // exit($msg);
    if ($db->query($sql) === TRUE) {
    		$msg .= "Successful for ".$db->insert_id.$sql.PHP_EOL;
    	}
    else
    {
    	$msg .= "Not Successful for ".$sql.PHP_EOL;
    }
    return $msg;
} 

function item_insert ($value){ 
	$msg="";

	//*************** begin php post method insert*******************
	// $items->name=$_GET['name'];
	// $items->description=$_GET['description'];
	// $items->price=$_GET['price'];
	// $items->category_id=$_GET['category_id'];
	// $items->created=date('Y-m-d H:i:s');

	// if(!empty($items->name) && !empty($items->description) &&
	//     !empty($items->price) && !empty($items->category_id))
	// {
	//     if($items->create()){         
	//         http_response_code(201);         
	//         echo json_encode(array("message" => "Item was created."));
	//     } else{         
	//         http_response_code(503);        
	//         echo json_encode(array("message" => "Unable to create item."));
	//     }
	// }else{    
	//     http_response_code(400);    
	//     echo json_encode(array("message" =>"Unable to create item. Data is incomplete."));
	//     // echo "Unable to create item. Data is incomplete.";
	// }

	//*************** end php post method insert*******************

    // $msg .= 'name: ' . $value['name'] . ' -> description: ' . $value['description'] . PHP_EOL;
	// print_r($value);
	$name=addslashes($value['name']);
	$description=addslashes($value['description']);
	$price=addslashes($value['price']);
	$category_id=addslashes($value['category_id']);
	$alt_category=addslashes($value['alt_category']);
	$data=$value['img_file'];

	$created = date('Y-m-d H:i:s'); 
	// call connection 
	$database = new Database();
	$db = $database->getConnection();

	 $sql="INSERT INTO items (`name`, `description`, `price`, `category_id`, `created`, `alt_category`)
        VALUES('".$name."','".$description."','".$price."','".$category_id."','".$created."', '".$alt_category."')";
    if ($db->query($sql) === TRUE) {
    		$msg .= "Successful for ".$db->insert_id.$sql.PHP_EOL;
    	}
    else{
    		$msg .= "Not Successful for ".$sql;
    	}

	if ($data){
		list($type, $data) = explode(';', $data);
		// print_r('type: '.substr($type,strripos($type,'/')-strlen($type)+1));
		$type=substr($type,strripos($type,'/')-strlen($type)+1);
		list(, $data)      = explode(',', $data);
		$data = base64_decode($data);
		// print_r('processed data:'.$data);
		file_put_contents('../images/uploads/'.$db->insert_id.'.'.$type, $data);
	}
	return $msg;
} // end item_insert function



function product_insert ($value){ 
	$msg="";
	// $msg .= 'name: ' . $value['name'] . ' -> description: ' . $value['description'] . PHP_EOL;
	// print_r($value);
	// $id=addslashes($value['id']);
	$code=addslashes($value['code']);
	$name=addslashes($value['name']);
	$category=addslashes($value['category']);
	$alt_category =addslashes($value['alt_category']);
	$description=addslashes($value['description']);
	$original_price=addslashes($value['original_price']);
	$discount_amt=addslashes($value['discount_amt']);
	$discount_per=addslashes($value['discount_per']);
	$net_price=addslashes($value['net_price']);
	$status=addslashes($value['status']);

	$created_by=addslashes($value['created_by']);
	$last_updated_by=addslashes($value['last_updated_by']);

	$imgdata=$value['image'];
	$imgdata1=$value['image1'];
	$imgdata2=$value['image2'];
	$imgdata3=$value['image3'];
	$imgdata4=$value['image4'];

	$creation_date = date('Y-m-d H:i:s'); 
	$last_update_date = date('Y-m-d H:i:s'); 
	//call connection
	$database = new Database();
	$db = $database->getConnection();

	 $sql = "INSERT INTO 0_products (`code`,`name`,`category`, `description`, `original_price`, `discount_amt`,`discount_per`,`net_price`, `status`, `created_by`, `creation_date`, `last_updated_by`,`last_update_date`,`alt_category`)
	    VALUES('".$code."', '".$name."', '".$category."', '".$description."', '".$original_price."', '".$discount_amt."', '".$discount_per."', '".$net_price."', '".$status."','".$created_by."', '".$creation_date."', '".$last_updated_by."', '".$last_update_date."', '".$alt_category."')";
	    $msg .='insert: '.$sql;

	    // exit($msg);
    if ($db->query($sql) === TRUE) {
    		$msg .= "Successful for ".$db->insert_id.$sql.PHP_EOL;
    		//save and upload image
    		if ($imgdata){
				list($type, $imgdata) = explode(';', $imgdata);
				print_r('type: '.substr($type,strripos($type,'/')-strlen($type)+1));
				$type=substr($type,strripos($type,'/')-strlen($type)+1);
				list(, $imgdata)      = explode(',', $imgdata);
				$imgdata = base64_decode($imgdata);
				// print_r('processed data:'.$imgdata);
				file_put_contents('../images/uploads/'.$db->insert_id.'_0.'.$type, $imgdata);
				$image=$db->insert_id.'_0.'.$type;
			}
			else{
				$image='noimg.png';
			}

			if ($imgdata1){
				list($type, $imgdata1) = explode(';', $imgdata1);
				// print_r('type: '.substr($type,strripos($type,'/')-strlen($type)+1));
				$type=substr($type,strripos($type,'/')-strlen($type)+1);
				list(, $imgdata1)      = explode(',', $imgdata1);
				$imgdata1 = base64_decode($imgdata1);
				// print_r('processed data:'.$imgdata);
				file_put_contents('../images/uploads/'.$db->insert_id.'_1.'.$type, $imgdata1);
				$image1=$db->insert_id.'_1.'.$type;
			}
			else{
				$image1='noimg.png';
			}

			if ($imgdata2){
				list($type, $imgdata2) = explode(';', $imgdata2);
				// print_r('type: '.substr($type,strripos($type,'/')-strlen($type)+1));
				$type=substr($type,strripos($type,'/')-strlen($type)+1);
				list(, $imgdata2)      = explode(',', $imgdata2);
				$imgdata2 = base64_decode($imgdata2);
				// print_r('processed data:'.$imgdata);
				file_put_contents('../images/uploads/'.$db->insert_id.'_2.'.$type, $imgdata2);
				$image2=$db->insert_id.'_2.'.$type;
			}
			else{
				$image2='noimg.png';
			}

			if ($imgdata3){
				list($type, $imgdata3) = explode(';', $imgdata3);
				// print_r('type: '.substr($type,strripos($type,'/')-strlen($type)+1));
				$type=substr($type,strripos($type,'/')-strlen($type)+1);
				list(, $imgdata3)      = explode(',', $imgdata3);
				$imgdata3 = base64_decode($imgdata3);
				// print_r('processed data:'.$imgdata);
				file_put_contents('../images/uploads/'.$db->insert_id.'_3.'.$type, $imgdata3);
				$image3=$db->insert_id.'_3.'.$type;
			}
			else{
				$image3='noimg.png';
			}

			if ($imgdata4){
				list($type, $imgdata4) = explode(';', $imgdata4);
				// print_r('type: '.substr($type,strripos($type,'/')-strlen($type)+1));
				$type=substr($type,strripos($type,'/')-strlen($type)+1);
				list(, $imgdata4)      = explode(',', $imgdata4);
				$imgdata4 = base64_decode($imgdata4);
				// print_r('processed data:'.$imgdata);
				file_put_contents('../images/uploads/'.$db->insert_id.'_4.'.$type, $imgdata4);
				$image4=$db->insert_id.'_4.'.$type;
			}
			else{
				$image4='noimg.png';
			}

			$sql="update 0_products set image='".$image."', image1='".$image1."',image2='".$image2."', image3='".$image3."', image4='".$image4."', alt_category='".$alt_category."' where id =".$db->insert_id;
			$msg .=' update: '.$sql;
			 if ($db->query($sql) === TRUE) {
			 		$msg .=" and successfully images uploaded";
			 } 

    	} // if data insert not successful
    else{
    		$msg .= "Not Successful for ".$sql;
    	}
   return $msg;
}
		
	
?>