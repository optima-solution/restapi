<!-- THIS FILE WAS PRIMARILY USED FOR TESTING CRUD READ OPTIONS-->
<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");

include_once '../config/Database.php';


if($_GET['FunctionType'] != ""){
    call_user_func($_GET['FunctionType']); 
    //print_r ($_GET['FunctionType']);
}
 
function items_read(){ 
    $database = new Database();
    $db = $database->getConnection();

    include_once '../class/Items.php';
    $items = new Items($db);

    $items->id = (isset($_GET['id']) && $_GET['id']) ? $_GET['id'] : '0';

    $items->category_id = (isset($_GET['category']) && $_GET['category']) ? $_GET['category'] : '0';
    // echo ('id:'.$items->id.'cate:'.$items->category_id.PHP_EOL);
    $result = $items->read();

    if($result->num_rows > 0){    
        $itemRecords=array();
        $itemRecords["items"]=array(); 
        while ($item = $result->fetch_assoc()) {    
            extract($item); 
            $itemDetails=array(
                "id" => $id,
                "name" => $name,
                "description" => $description,
                "price" => $price,
                "category_id" => $category_id,            
                "created" => $created,
                "modified" => $modified         
            ); 
           array_push($itemRecords["items"], $itemDetails);
           //array_push($itemDetails);
        }    
        http_response_code(200);     
        echo json_encode($itemRecords);
        //echo json_encode($itemDetails);
    }else{     
        http_response_code(404);     
        echo json_encode(
            array("message" => "No item found.")
        );
    } 
}

function products_read (){ 
    $database = new Database();
    $db = $database->getConnection();
    include_once '../class/Products.php';

    $products = new Products($db);

    $products->id = (isset($_GET['id']) && $_GET['id']) ? $_GET['id'] : '0';

    $products->category = (isset($_GET['category']) && $_GET['category']) ? $_GET['category'] : '0';
    // print_r ('id:'.$products->id.'cate:'.$products->category.PHP_EOL);
    $result = $products->read();

    if($result->num_rows > 0){    
        $productRecords=array();
        $productRecords["products"]=array(); 
        while ($item = $result->fetch_assoc()) {    
            extract($item); 
            $product_Details=array(
                "id" => $id,
                "code" => $code,
                "name" => $name,
                "category" => $category,
                "image" => $image,
                "description" => $description,
                "original_price" => $original_price,
                "discount_amt" => $discount_amt,
                "discount_per" => $discount_per,
                "net_price" => $net_price,
                "status" => $status,
                "image1" => $image1,
                "image2" => $image2,
                "image3" => $image3,
                "image4" => $image4,
                "created_by" => $created_by,
                "creation_date" => $creation_date,
                "last_updated_by" => $last_updated_by,
                "last_update_date" => $last_update_date
            ); 
           array_push($productRecords["products"], $product_Details);
           // array_push($product_Details);
        }    
        http_response_code(200);     
        echo json_encode($productRecords);
        //echo json_encode($product_Details);
    }else{     
        http_response_code(404);     
        echo json_encode(
            array("message" => "No product found.")
        );
    } 
}
?>