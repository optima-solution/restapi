<?php include 'header.php' ?>
<div id="root">
  <div class="row">
    <h5 class="left">Product Upload</h5>&nbsp;&nbsp;&nbsp;
    <a class="waves-effect waves-light btn modal-trigger" href="#addmodal" style="margin-top: 10px;" @click="showingBtnSave = true; showingBtnUpdate = false; 
    selectElement('prdtypediv','product_type','', prdtypes, 'Select Product Type');
    selectElement('catagorydiv','category', '', catagories, 'Select Category');
    selectElement('alt_categorydiv','alt_category', '', alt_catagories,'Select Alternative Category');">Add New Product</a>
    <table class="striped responsive-table">
      <tr>
        <th>ID</th>
        <th>Product Code</th>
        <th>Product Name</th>
        <th>Category</th>
        <th>Description</th>
        <th>Net Price</th>
        <th>Edit</th>
        <th>Delete</th>
      </tr>
      <tr v-for="product in products">
        <td>{{product.id}}</td>
        <td>{{product.code}}</td>
        <td>{{product.name}}</td>
        <td>{{product.category}}</td>
        <td>{{product.description}}</td>
        <td>{{product.net_price}}</td>
        <td><a class="waves-effect waves-light btn" @click="showingBtnSave = false;showingBtnUpdate = true; selectProduct(product)">Edit</a></td>
        <td><button class="waves-effect waves-light btn" @click="deleteProduct(product)" >Delete</button></td>
      </tr>
    </table>
  </div><!--row-->
    <div class="modal" style="width:100%; height: 800px;" id="addmodal">
      <div class="modal-content">
        <div class="row">
          <div class="col l6 m6 s6 left-align">
            <h6>Add/Edit Product</h6>
            <!-- <h6 id="Msg"></h6> -->
          </div>
          <div class="col  l6 m6 s6 right-align">
            <button class="modal-close waves-green right-align btn-small">Close</button>
          </div>
        </div>
        <div class="row">
          <div class="input-field col s6 m4 l4">
            <label for="prdid">Product ID</label></br/>
            <input id="prdid" type="text" class="validate  black-text" v-model="clickedProduct.id"  disabled="true">
          </div>
          <div class="input-field col s6 m4 l4">
            <label for="code">Product code</label></br/>
            <input id="code" type="text" class="validate  black-text" v-model="clickedProduct.code">
          </div>
          <div class="input-field col s6 m4 l4">
            <label for="name">Product name</label><br/>
            <input id="name" type="text" class="validate  black-text" v-model="clickedProduct.name">
          </div>
          <div id='prdtypediv' class="input-field col s6 m4 l4">
            <label id="lblstatus">Select Product Type</label><br/><br/>
            <!-- <select id="product_type" v-model="selprdtype">
                  <option v-for="prdtype in prdtypes" v-bind:value="prdtype.id">
                  {{ prdtype.name }}
                  </option>
            </select>  -->
          </div>
          <div id="catagorydiv" class="input-field col s6 m4 l4">
              <label id="lblcategory">Select Category</label><br/><br/>
              <!-- <select id="category" v-model="selcat">
                  <option v-for="category in catagories" v-bind:value="category.id">
                  {{ category.name }}
                  </option>
              </select>        -->
          </div>
          <div class="input-field col s6 m4 l4">
            <label for="original_price">Original Price</label><br/>
            <input id="original_price" type="text" class="validate  black-text
            " v-model="clickedProduct.original_price">
          </div>
          <div class="input-field col s6 m4 l4">
            <label for="discount_amt">Discount Amount</label><br/>
            <input id="discount_amt" type="text" class="validate  black-text" v-model="clickedProduct.discount_amt">
          </div>
          <div class="input-field col s6 m4 l4">
            <label for="discount_per">Discount Percentage</label><br/>
            <input id="discount_per" type="text" class="validate  black-text" v-model="clickedProduct.discount_per">
          </div>
          <div class="input-field col s6 m4 l4">
            <label for="net_price">Net Price</label><br/>
            <input id="net_price" type="text" class="validate  black-text" v-model="clickedProduct.net_price">
          </div>
          <div class="input-field col s6 m4 l4">
            <label for="description">Description</label><br/>
            <textarea id="description" class="materialize-textarea  black-text" v-model="clickedProduct.description"></textarea> 
          </div>
          <div class="input-field col s6 m4 l4">
            <label for="image">Product Image(400X400)</label><br><br>
            <img id="img_thumb" style="width: 100px; height: 100px; object-fit:fill;"/>
            <input id="image" type="file" class="validate  black-text">         
          </div>
          <div class="input-field col s6 m4 l4">
            <label for="image1">Additional Image1</label><br><br>
            <img id="img_thumb1" style="width: 100px; height: 100px; object-fit:fill;"/>
            <input id="image1" type="file" class="validate  black-text">
          </div>
          <div class="input-field col s6 m4 l4">
            <label for="image2">Additional Image2</label><br><br>
            <img id="img_thumb2" style="width: 100px; height: 100px; object-fit:fill;"/>
            <input id="image2" type="file" class="validate  black-text">
          </div>
          <div class="input-field col s6 m4 l4">
            <label for="image3">Additional Image3</label><br><br>
            <img id="img_thumb3" style="width: 100px; height: 100px; object-fit:fill;"/>
            <input id="image3" type="file" class="validate  black-text">
          </div>
          <div class="input-field col s6 m4 l4">
            <label for="image4">Additional Image4</label><br><br>
            <img id="img_thumb4" style="width: 100px; height: 100px; object-fit:fill;"/>
            <input id="image4" type="file" class="validate  black-text">
          </div>
          <div id='statusdiv' class="input-field col s6 m4 l4">
            <label id="lblstatus">Select Product Status</label><br/><br/>
            <select id="status" v-model="selstatus">
                  <option v-for="statcat in status" v-bind:value="statcat.id">
                  {{ statcat.name }}
                  </option>
            </select> 
          </div>
          <div id="alt_categorydiv" class=" col s6 m4 l4">
            <label id="lblalt_category">Select Alternative Category</label><br/><br/>
              <!-- <select id="alt_category" v-model="selaltcat">
                  <option v-for="category in alt_catagories" v-bind:value="category.id">
                  {{ category.name }}
                  </option>
              </select>                      -->
          </div>
          <div class="input-field col s6 m4 l4">
            <label for="is_display">Show? (Show-1, hide-0)</label><br/>
            <textarea id="is_display" class="materialize-textarea  black-text" v-model="clickedProduct.is_display"></textarea> 
          </div>
          <div class="input-field col s6 m4 l4">
            <label for="display_order">Display Order(Priority 0 to 10)</label><br/>
            <textarea id="display_order" class="materialize-textarea  black-text" v-model="clickedProduct.display_order"></textarea> 
          </div>
      </div><!--row-->
    </div> <!--Modal Content-->
    <div class="row left-align">
          <button id="btnsave" class="waves-effect waves-light btn left-align" v-if="showingBtnSave">Save Category</button>
           <button id="btnupdate" class="waves-effect waves-light btn left-align" v-if="showingBtnUpdate">update Category</button><br/>
           <h6 id="Msg"></h6>

    </div>
  </div><!--Add Modal-->
</div> <!--root-->

<?php include 'footer.php' ?>

<script src="scripts/vue.js"></script>
<script src="scripts/axios.min.js"></script>
<script type="text/javascript">
  var app = new Vue({

  el: "#root",
  data: {
    showingdeleteModal: false,
    showingBtnSave: true,
    showingBtnUpdate: true,
    products: [],
    clickedProduct: {},
    selcat:null,
    catagories:[],
    selaltcat:null,
    alt_catagories:[],
    selstatus:null,
    selprdtype:null,
    status: [
      { name: 'Hot product', id: '1' },
      { name: 'Basic product', id: '2' },
      { name: 'New Arrival', id: '3' },
      { name: 'Stock Out', id: '4' },
      { name: 'Archive', id: '5' },
    ],
    prdtypes: [
      { name: 'Gold', id: '1' },
      { name: 'Diamond', id: '2' },
      { name: 'Platinum', id: '3' },
      { name: 'Pearl', id: '4' },
    ],
  },
  mounted: 
  function () {
    this.getAllProducts();
    this.getAllCategories();
    this.getAllAltCategories();
  },
  methods: {
    getAllCategories: function(){
      axios.get("crud/category_c.php?FunctionType=read&&main_catagory_id=1")
      .then(function(response){
        console.log("responsecat:",response.data.catagories);
        if (response.data.error) {
          app.errorMessage = response.data.message;
        }else{
          app.catagories = response.data.catagories;
        }
      });
    },
    getAllAltCategories: function(){
      axios.get("crud/category_c.php?FunctionType=read&&main_catagory_id=-1")
      .then(function(response){
        console.log("responsealtcat:",response.data.catagories);
        if (response.data.error) {
          app.errorMessage = response.data.message;
        }else{
          app.alt_catagories = response.data.catagories;
        }
      });
    },
    getAllProducts: function(){
        axios.get("crud/product_c.php?FunctionType=read")
        .then(response => {
            console.log('response:',response.data);
            this.products = response.data.products;
        }).catch(err => {
          console.log(err)
        });
      },
      //Fetching selected value to select optin
      //since selected value is not renderd hence in the label it is shown
      selectElement: function(divid, selectid, valueToSelect, data, label) {  
        console.log('data..for',selectid,':',data);
        let select_view = '';
        let sel = '';
        for (let i = 0; i < data.length; ++i) {
            if(data[i].id == valueToSelect){
                sel = 'selected';
            }else{
                sel = '';
            }
            select_view += `<option value="${data[i].id}" ${sel}>${data[i].name}</option>`;
        }
        select_view = `<label>${label}</label><br><select id="${selectid}">${select_view}</select>`;
        $('#'+ divid).html(select_view);
        console.log('#'+ divid, select_view);
        $('#'+ selectid).formSelect();
      },
      selectProduct(products){
         app.clickedProduct = products;
         // console.log('src', 'images/uploads/' + products.image);
         document.getElementById('img_thumb').src='images/uploads/allproducts/' + products.image;
         //Fetching selected value to select element
        //  console.log('product', products);
        //  console.log('catagories', this.catagories);
        //  console.log('altcatagories',this.alt_catagories);
        this.selectElement('prdtypediv','product_type', products.product_type, this.prdtypes, 'Select Product Type'); 
        this.selectElement('catagorydiv','category', products.category, this.catagories, 'Select Category');
         this.selectElement('statusdiv', 'status', products.status, this.status, 'Select Product Status');
         this.selectElement('alt_categorydiv','alt_category', products.alt_category, this.alt_catagories,'Select Alternative Category');
          $('#addmodal').modal('open');
        },
      deleteProduct:function(products){
        // var formData = app.toFormData(app.clickedUser);
        app.clickedProduct = products;
        if(confirm("Do you really want to delete?")){
          var  loginId= <?php echo "'".get_ses('loginID')."'"; ?>;
          var auth_token=<?php echo "'".get_ses('auth_token')."'"; ?>;
          var url="crud/product_c.php?FunctionType=delete&&id="+products.id+"&&loginID="+loginId+"&&auth_token="+auth_token;
          console.log(url);
          axios.get(url)
            .then(function(response){
              console.log(response);
              app.clickedUser = {};
              if (response.data.error) {
                app.errorMessage = response.data.message;
              }else{
                app.successMessage = response.data.message;
                alert(response.data);
                app.getAllCategories();
              }
            });          
        }
        },
        toFormData: function(obj){
          var form_data = new FormData();
              for ( var key in obj ) {
                  form_data.append(key, obj[key]);
              } 
              return form_data;
        }    
    }
});
</script>

<script type="text/javascript">
  $(document).ready(function () {
      $('.modal').modal();
      $('select').formSelect();
    });
  
  pre_img(); // preview image 


    function init() {
        initializeServiceWorker();
        initializeDB();
        // checkIndexedDB();
    }

    init();

    // STEP OF PROCESS: REGISTER SERVICE WORKER->SAVE TO INDEXEDDB->SEND FOR SYNC
    //STEP IN SW: ->SYNC WHEN ONLINE->DELETE FROM INDEXEDDB
    function initializeServiceWorker() {
        if(navigator.serviceWorker) {
            navigator.serviceWorker.register('service-worker.js')
            .then(function() {
              console.log('service worker ready');
                return navigator.serviceWorker.ready
            })
            .then(function(registration) {
                console.log('registered..');
                document.getElementById('btnsave').addEventListener('click', (event) => {
                    // event.preventDefault();
                    saveData('Product_insert').then(function() {
                      document.getElementById('Msg').innerHTML='Please Wait..';
                      console.log('inserting data');
                        if(registration.sync) {
                          //console.log('example-synced');
                            registration.sync.register('product-sync')
                            //clearing data
                            .then(function(){
                              if(navigator.onLine){
                                // alert('Product Upload Successful');
                              }
                            }) 
                            .catch(function(err) {
                              console.log('sync err: ',err);
                                return err;
                            })
                        } else {
                            // sync isn't there so fallback
                            //console.log('checking internet');
                            checkInternet();
                        }
                    });
                })//BUTTON SAVE ENDS HERE
                document.getElementById('btnupdate').addEventListener('click', (event) => {
                    // event.preventDefault();
                    saveData('Product_Update').then(function() {
                      document.getElementById('Msg').innerHTML='Please Wait..';
                      console.log('saving data');
                        if(registration.sync) {
                          //console.log('example-synced');
                            registration.sync.register('product-sync')
                            //clearing data
                            .then(function(){
                              if(navigator.onLine){
                                // alert('Product Upload Successful');
                              }
                            }) 
                            .catch(function(err) {
                              console.log('sync err: ',err);
                                return err;
                            })
                        } else {
                            // sync isn't there so fallback
                            //console.log('checking internet');
                            checkInternet();
                        }
                    });
                })//button update ends here
            })
        } 
        // else {
        //     document.getElementById('btnsave').addEventListener('click', (event) => {
        //         event.preventDefault();
        //         saveData().then(function() {
        //             checkInternet();
        //         });
        //     })
        // }
    }

    function initializeDB() {
        var newsletterDB = window.indexedDB.open('productDB');

        newsletterDB.onupgradeneeded = function(event) {
            var db = event.target.result;

            var itemTbl = db.createObjectStore("productTbl", { autoIncrement: true });
            itemTbl.createIndex("FunctionType", "FunctionType", { unique: true });
            itemTbl.createIndex("id", "id", { unique: true });
            itemTbl.createIndex("prdid", "prdid", { unique: true });
            itemTbl.createIndex("code", "code", { unique: false });
            itemTbl.createIndex("name", "name", { unique: false });      
            itemTbl.createIndex("product_type", "product_type", { unique: false }); 
            itemTbl.createIndex("category", "category", { unique: false });
            itemTbl.createIndex("description", "description", { unique: false });
            itemTbl.createIndex("original_price", "original_price", { unique: false });
            itemTbl.createIndex("discount_amt", "discount_amt", { unique: false });
            itemTbl.createIndex("discount_per", "discount_per", { unique: false });
            itemTbl.createIndex("net_price", "net_price", { unique: false });
            itemTbl.createIndex("status", "status", { unique: false });
            itemTbl.createIndex("created_by", "created_by", { unique: false });
            itemTbl.createIndex("creation_date", "creation_date", { unique: false });
            itemTbl.createIndex("last_updated_by", "last_updated_by", { unique: false });
            itemTbl.createIndex("last_update_date", "last_update_date", { unique: false });
            itemTbl.createIndex("image", "image", { unique: false });
            itemTbl.createIndex("image1", "image1", { unique: false });
            itemTbl.createIndex("image2", "image2", { unique: false });
            itemTbl.createIndex("image3", "image3", { unique: false });
            itemTbl.createIndex("image4", "image4", { unique: false });
            itemTbl.createIndex("alt_category", "alt_category", { unique: false });
            itemTbl.createIndex("loginId", "loginId", { unique: false });
            itemTbl.createIndex("auth_token", "auth_token", { unique: false });
            itemTbl.createIndex("is_display", "is_display", { unique: false });
            itemTbl.createIndex("display_order", "display_order", { unique: false });
        }
    }

    
 

   function set_img_res(inputfile) {
    return new Promise(function(resolve,reject){
      if (inputfile.value){
          var file = inputfile.files[0];  
          reader = new FileReader();
          reader.onloadend=function(){ 
            // ResizeImage(e.target.result).then(function(res){alert(res);});
            // resolve(reader.result);
            ResizeImage(reader.result,400,400).then(function(res){resolve(res);});
          }
          
          reader.readAsDataURL(file); 
       }
      else{
        resolve("");
      }
    })
  }

   async function saveData (FunctionType){
      return new Promise(function(resolve, reject) {
        // TAKING IMAGE DATA TO BLOB
     let image_res="";
     let image_res1="";
     let image_res2="";
     let image_res3="";
     let image_res4="";

     var inputfile = document.getElementById('image');
      
      set_img_res(inputfile).then(function(result){
        image_res=result;//main image
        // console.log('img',image_res);
      }).then(function(){
        inputfile = document.getElementById('image1');
        set_img_res(inputfile).then(function(result){
          image_res1=result;
          // console.log('img1',image_res1);
        }).then(function(){
          inputfile = document.getElementById('image2');
          set_img_res(inputfile).then(function(result){
            image_res2=result;
            // console.log('img2',image_res2);
          }).then(function(){
            inputfile= document.getElementById('image3');
            set_img_res(inputfile).then(function(result){
              image_res3=result;
              // console.log('img3',image_res3);
          }).then(function(){
            inputfile=document.getElementById('image4');
            set_img_res(inputfile).then(function(result){
              image_res4=result;
              // console.log('img4',image_res4);
            }).then(function(){
                var msg='';
                // console.log('img file checking');
                  var tmpObj = {
                        FunctionType: FunctionType,
                        prdid: document.getElementById('prdid').value,
                        code: document.getElementById('code').value,
                        name: document.getElementById('name').value,
                        product_type: document.getElementById('product_type').value,
                        category: document.getElementById('category').value,
                        description: document.getElementById('description').value,
                        original_price: document.getElementById('original_price').value,
                        discount_amt: document.getElementById('discount_amt').value,
                        discount_per: document.getElementById('discount_per').value,
                        net_price: document.getElementById('net_price').value,
                        status: document.getElementById('status').value,
                        created_by: "Admin",
                        creation_date:currentdatetime(),
                        last_updated_by: "Admin",
                        last_update_date:currentdatetime() ,
                        image: image_res,
                        image1: image_res1,
                        image2: image_res2,
                        image3: image_res3,
                        image4: image_res4,
                        alt_category: document.getElementById('alt_category').value,
                        loginId: <?php echo "'".get_ses('loginID')."'"; ?>,
                        auth_token: <?php echo "'".get_ses('auth_token')."'"; ?>,
                        is_display: document.getElementById('is_display').value,
                        display_order: document.getElementById('display_order').value,
                      };
                       console.log('file:',tmpObj);
                    var myDB = window.indexedDB.open('productDB');

                    myDB.onsuccess = function(event) {
                      var objStore = this.result.transaction('productTbl', 'readwrite').objectStore('productTbl');
                      objStore.add(tmpObj);
                      msg='successfully';
                      resolve();
                    }

                    myDB.onerror = function(err) {
                      msg='error';
                      reject(err);
                    }

            })
          })
        })
       })
      })
    }) //promise
}//function savedata()
  

    function checkInternet() {
        // event.preventDefault();
        if(navigator.onLine) {
              console.log('online');
        } else {
            alert("You are offline! When your internet returns, we'll finish up your request.");
        }
    }

    window.addEventListener('offline', function() {
        alert('You have lost internet access!');
    });


function pre_img(){
    //preview image function
    $("#image").change(function() {readURL(this,"img_thumb");});
    $("#image1").change(function() {readURL(this,"img_thumb1");});
    $("#image2").change(function() {readURL(this,"img_thumb2");});
    $("#image3").change(function() {readURL(this,"img_thumb3");});
    $("#image4").change(function() {readURL(this,"img_thumb4");});

    function readURL(input,img_thumb) {
      if (input.files && input.files[0]) {
        var reader = new FileReader();       
        reader.onload = function(e) {
          console.log(img_thumb)
          $('#'+img_thumb).attr('src', e.target.result);
          // ResizeImage(e.target.result).then(function(res){alert(res);});
        }
        reader.readAsDataURL(input.files[0]); // convert to base64 string
      }
    }
}
</script>
<?php include 'endfooter.php';?>