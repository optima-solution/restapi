<?php 
include 'header.php';
// header("Access-Control-Allow-Origin: *");


if (isset($_POST['place_order'])){
	if (!get_ses('login')){
		echo "<script>alert('Please login first');</script>";
	}
	else{
	
		// var myHeaders = new Headers();
		// myHeaders.append("X-Viber-Auth-Token", "4bf78675af27dd13-4dba5372cf39ba2b-c67d668b850afd65");
		// myHeaders.append("Content-Type", "text/plain");
		// myHeaders.append("Access-Control-Allow-Origin", "*");
  
		// var raw = '{"receiver":"mF3DiyunQnRnEN6wnWBDLw==","min_api_version":1,"sender":{"name":"rashed"},      "tracking_data":"tracking data","type":"text","text":"Hello world!"}';
  
		// var requestOptions = {
		//   method: 'POST',
		//   headers: myHeaders,
		//   body: raw,
		//   redirect: 'follow'
		// };
  
		// fetch("https://chatapi.viber.com/pa/send_message", requestOptions)
		//   .then(response => response.text())
		//   .then(result => console.log(result))
		//   .catch(error => console.log('error', error));
		  

		$curl = curl_init();
		$phone=get_ses('phone');
		$orders=$_POST["orders"];
        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://chatapi.viber.com/pa/send_message",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS =>"{\r\n   \"receiver\":\"mF3DiyunQnRnEN6wnWBDLw==\",\r\n   \"min_api_version\":1,\r\n   \"sender\":{\r\n      \"name\":\"rashed\"\r\n   },\r\n   \"tracking_data\":\"tracking data\",\r\n   \"type\":\"text\",\r\n   \"text\":\"You have got a new order from $phone of $orders\"\r\n}",
          CURLOPT_HTTPHEADER => array(
            "X-Viber-Auth-Token: 4bf78675af27dd13-4dba5372cf39ba2b-c67d668b850afd65",
            "Content-Type: text/plain"
          ),
        ));
        
        $response = curl_exec($curl);
        
        curl_close($curl);
        // echo $response;
		echo "<script>alert('Thank you for placing your order. Our Team shall contact you soon');</script>";
	}
}

?>
<style type="text/css">
	  div#filter {position: fixed; top: 50px; left: 0px; 
	  margin: 0px; padding: 20px 0px 0px; background:white; 
	  color: black; list-style: none; width: 18s0px; height: 100%;
	  border-radius:0px 10px 10px 0px; z-index:1000;}
	  div#filter::-webkit-scrollbar {
			display: none;
		}
		/* Hide scrollbar for IE, Edge and Firefox */
		div#filter {
		-ms-overflow-style: none;  /* IE and Edge */
		scrollbar-width: none;  /* Firefox */
		}
	  div#filter .nav-btn{background:#26a69a; color:white; border-radius:0px 10px 10px 0px; position:absolute;top:50px; left:160px; width:20px; height:120px; display:block; cursor:pointer; z-index:1000;}
	  /*div#filter .nav-btn{background:#ff3385; color:white; border-radius:10px 0px 0px 10px; position:absolute;top:0px; left:25px; width:25px; height:250px; display:block; cursor:pointer;}*/
	  /*div#filter .nav-btn img{margin:20px 0px 0px 13px; }*/
	  /*div#filter li {width: 170px; height: 200px; margin-bottom:20px; text-align:center; text-decoration:none; color:#FFF;}*/
	  /*div#filter li a{color:#FFF; text-decoration: none;}*/
</style>

<div id="app">
	<div><!--  variable categories -->		
			<div class="row black">
				<div class="col s3 m2 l2">
					<div class="white-text black" id="filter" style="overflow: auto;">
						<div class="nav-btn">
							<i id="price_icon" class="material-icons"> chevron_right </i>
							<h6 id="btnshow" class="rotate">Show</h6>
						</div>
						<form name="form1" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>" >
							<button name='filter' class="waves-effect waves-light btn-small">
							<i class="material-icons right">check_box</i>Filter
							</button>   
							<h6 style='color: blue'>Filter by price </h6> 
							<p><label><input type="radio" name="price" id="price" value="0"<?php if ((isset($_POST['price']))&& ($_POST['price']=='0')) {echo 'checked';} ?>/><span>All</span></label></p> 
							<p><label><input type="radio" name="price" id="price" value="1" 
							<?php if ((isset($_POST['price']))&& ($_POST['price']=='1')) {echo 'checked';} ?>/><span>up to Tk. 10,000</span></label></p> 
							<p><label><input type="radio" name="price"  id="price" value="2" <?php if ((isset($_POST['price']))&& ($_POST['price']=='2')) {echo 'checked';} ?>/><span>Tk. 10,001-20,000</span></label></p>
							<p><label><input type="radio" name="price"  id="price" value="3" <?php if ((isset($_POST['price']))&& ($_POST['price']=='3')) {echo 'checked';} ?>/><span>Tk. 20,001-50,000</span></label></p>
							<p><label><input type="radio" name="price"  id="price" value="4" <?php if ((isset($_POST['price']))&& ($_POST['price']=='4')) {echo 'checked';} ?>/><span>Tk. 50,001-1,00,000</span></label></p>
							<p><label><input type="radio" name="price" id="price" value="5"  <?php if ((isset($_POST['price']))&& ($_POST['price']=='5')) {echo 'checked';} ?>/><span>Tk. 1,00,001-5,00,000</span></label></p>
							<p><label><input type="radio" name="price"  id="price" value="6"  <?php if ((isset($_POST['price']))&& ($_POST['price']=='6')) {echo 'checked';} ?>/><span>Above Tk. 5,00,000</span></label></p>

							<h6 style='color: blue'>Filter by Category </h6> 
							<p><label><input  type="radio" name="category" id="category" value="0" /><span>All</span></label></p> 
							<div v-for="category in prd_cat" :key="category.id">
								<p><label><input  type="radio" name="category" id="category" v-bind:value="category.id" /><span>{{category.name}}</span></label></p> 
							</div>

							<h6 style='color: blue'>Filter by Collection </h6> 
							<p><label><input  type="radio" name="alt_category" id="alt_category" value="0" /><span>All</span></label></p> 
							<div v-for="category in alt_cat" :key="category.id">
								<p><label><input  type="radio" name="alt_category" id="alt_category" v-bind:value="category.id" /><span>{{category.name}}</span></label></p> 
							</div>
						</form>
					</div>
				</div>
				<div id="products" class="col s9 m10 l10" >
					<div class="row black">
						<div class="col s12 m6 l3" v-for="product in products" :key="product.id">
							<div class="card" v-if="product.net_price>0"> 
								<div class="card-image waves-effect waves-block waves-light">
									<a v-bind:href="'productdetails.php?id='+ product.id"> 
										<img class="activator" v-bind:src="'images/uploads/allproducts/'+ product.image" style="width:100%; height:250px; object-fit:fill;" />
									</a>
								</div>
								<div class="card-content">
									<a v-bind:href="'productdetails.php?id='+ product.id"> 
										<span class=" activator blue-text text-darken-4"><u>{{product.name}}</u></span>
									</a>
									<ul><li>Previous Price: <strike>{{product.original_price|formatPrice}}</strike></li>
									<li>Discount: {{product.discount_per}}%</li>
									<li>Price: {{product.net_price|formatPrice}}</li>
									<li>Description:{{product.description}}</li> </ul>
									<p><button @click="addToCart(product)" class="btn light-blue"><i class="material-icons">add_shopping_cart</i></button></p>
								</div>
							</div>
							<div class="card" v-else> 
								<div class="card-image waves-effect waves-block waves-light">
									<a v-bind:href="'productdetails.php?id='+ product.id"> 
										<img class="activator" v-bind:src="'images/uploads/allproducts/'+ product.image" style="width:100%; height:250px; object-fit:fill;" />
									</a>
								</div>
								<div class="card-content">
									<a v-bind:href="'productdetails.php?id='+ product.id"> 
										<span class=" activator blue-text text-darken-4"><u>{{product.name}}</u></span>
									</a>
									<p>Description:{{product.description}}</p>
									<p>Pleace Call for price</p>
									<p><button @click="addToCart(product)" class="btn light-blue"><i class="material-icons">add_shopping_cart</i></button></p>
								</div>
							</div>
						</div><!--product column-->
					</div><!--Row-->
				</div>
			</div>
	</div>
	<div class="fixed-action-btn">
      <button class="btn-floating btn-large red" onclick="showhidecart();" style="z-index: 2000;">
        <i class="large material-icons">add_shopping_cart</i>
      </button>
	</div>
		
	<div id="cart" class="cart grey white-text">

	</div>
</div>

<?php 
// include 'footer.php';
?>

<script src="scripts/jquery.min.js"></script>
<script src="scripts/materialize.min.js"></script>
<script src="scripts/app.js"></script>
<script src="scripts/install.js"></script>
<script type="text/javascript">
	// Register service worker.
	if ('serviceWorker' in navigator) {
	  window.addEventListener('load', () => {
	    navigator.serviceWorker.register('service-worker.js')
	        .then((reg) => {
	          console.log('Service worker registered.', reg);
	        });
	  });
	}
</script>
<script src="scripts/vue.js"></script>
<script src="scripts/axios.min.js"></script>
<script>
	var app = new Vue({
	  el: '#app',
	  data () {
	    return {
	      products: null,
		  prd_cat: null,
		  alt_cat:null,
	      loading: true,
	      errored: false,
	      available:false,
		  url:'',
	    }
	  },
		filters: {
			formatPrice(value) {
				    var formatter = new Intl.NumberFormat('en-US', {
			        style: 'currency',
			        currency: 'BDT',
			        minimumFractionDigits: 0
			    });
			    return formatter.format(value);
			}
		},
		methods:{
			addToCart: function (product) {
			// now we have access to the native event
			console.log('product',product);
			// const itmes = {
			// 	id: id,
			// 	location: "Lagos",
			// }
			product.quantity=1;
			window.localStorage.setItem('product'.concat(product.id), JSON.stringify(product));
			showcart();
			// for(var i in obj)
			// alert(i+"--"+obj[i]["firstName"]);
			  },
		},
	  mounted () {
	  		url="crud/product_c.php?FunctionType=read&&category=".concat(
			  <?php 
			  //when request come from home page
			  $cat = (isset($_GET['cat'])) ? $_GET['cat'] : '0';
			  $alt_cat = (isset($_GET['alt_category'])) ? $_GET['alt_category'] : '0';
			  $product_type = (isset($_GET['product_type'])) ? $_GET['product_type'] : '0';
			  //where request come from filter buttion
			  $cat1 = (isset($_POST['category'])) ? $_POST['category'] : '0';
			  $alt_cat1 = (isset($_POST['alt_category'])) ? $_POST['alt_category'] : '0';
			  $price = (isset($_POST['price'])) ? $_POST['price'] : '0';
			  $product_type1 = (isset($_POST['product_type'])) ? $_POST['product_type'] : '0';
			  $fcat=($cat1>0) ? $cat1 : $cat;
			  $falt_cat=($alt_cat1>0) ? $alt_cat1 : $alt_cat;
			  $st = $fcat."&&alt_category=".$falt_cat."&&price=". $price."&&product_type=". $product_type;
			  echo "'".$st."'";
			  ?>) ;
			  console.log(url);
	        // axios.get('items/read.php?FunctionType=products_read&&category_id=0')
			// axios.get('crud/product_c.php?FunctionType=read&&category_id=0')
			// axios.get('crud/product_c.php?FunctionType=read&&category=0&&price=0')
			axios.get(url)
	        .then(response => {
	            // console.log('response:',response.data.products);
	            this.products = response.data.products;
				console.log('products:',this.products);
	        }).catch(err => {
	          console.log(err)
	        });
			axios.get("crud/category_c.php?FunctionType=read&&main_catagory_id=-1")
				.then(response => {
					console.log('alt cat:',response.data.catagories)
					this.alt_cat = response.data.catagories
				}).catch(err => {
					console.log(err)
				});
			axios.get("crud/category_c.php?FunctionType=read&&main_catagory_id=1")
			.then(response => {
				console.log('main cat:',response.data.catagories)
				this.prd_cat = response.data.catagories
			}).catch(err => {
				console.log(err)
			});
	      }
	})
</script>
<script>
	$(function() {
		$('#filter').stop().animate({
			'margin-left': '0px'
		}, 1000);

		function toggleDivs() {
			var $prd_left="200px";
			var $inner = $("#filter");
			var $products = $("#products");
			if ($inner.css("margin-left") == "-160px") {
				$inner.animate({
					'margin-left': '0'
				});
				$products.animate({
					'margin-left': '0'
				});
				if ($('html').width() <= 400) {$prd_left="120px"; }
				else if ($('html').width() > 1250) {$prd_left="0px"; }
				else {$prd_left="100px"; }
				$("#products").css("padding-left", $prd_left);
				$("#products").removeClass("col s12 m12 l12");
				$("#products").addClass("col s9 m10 l10");
				$("#price_icon").html('clear');
				$("#btnshow").html('hide');
			} else {
				$inner.animate({
					'margin-left': "-160px"
				});
				$products.animate({
					'margin-left': '-200px'
				});
				if ($('html').width() <= 400 ) {$prd_left="120px"; }
				else if ($('html').width() > 1250) {$prd_left="200px"; }
				else {$prd_left="100px"; }
				$("#products").css("padding-left", $prd_left);
				$("#products").removeClass("col s9 m10 l10");
				$("#products").addClass("col s12 m12 l12");		
				$("#price_icon").html('chevron_right');
				$("#btnshow").html('show');
			}
			console.log('left:',$prd_left);
		}
		$(".nav-btn").bind("click", function() {
			toggleDivs();
		});
		toggleDivs();
	});
</script>

<?php include 'endfooter.php'; ?>  