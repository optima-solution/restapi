<?php 
    header('Access-Control-Allow-Origin: *');  
    include_once 'session.php'; 
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
  <meta http-equiv="Pragma" content="no-cache" />
  <meta http-equiv="Expires" content="0" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Golden World</title>
  <meta name="Ecommerce" content="Optima Ecommerce PWA">
  <link rel="stylesheet" type="text/css" href="styles/materialize.css"/>
  <link rel="stylesheet" type="text/css" href="styles/inline.css?v=02"/>
  <link rel="stylesheet" type="text/css" href="styles/media.css?v=01"/>
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-alpha.1/css/materialize.min.css"> -->

  <link rel="icon" href="images/favicon.ico" type="image/x-icon" />

  <!-- CODELAB: Add link rel manifest -->
  <link rel="manifest" href="manifest.json">
  <!-- CODELAB: Add iOS meta tags and icons -->
  <meta name="apple-mobile-web-app-capable" content="yes">
  <meta name="apple-mobile-web-app-status-bar-style" content="black">
  <meta name="apple-mobile-web-app-title" content="BLIL Rach Chart">
  <link rel="apple-touch-icon" href="images/icons/icon-152x152.png">
  <!-- social media-->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <!-- CODELAB: Add description here -->
  <meta name="description" content="Optima Ecommerce PWA">
  <!-- CODELAB: Add meta theme-color -->
  <meta name="theme-color" content="#2F3BA2" />
  
    <script src="scripts/jquery.min.js"></script>
    <script src="scripts/materialize.min.js"></script>
  <!-- Latest compiled and minified CSS -->
  <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css"> -->


</head>
<body>

  <header class="header row" >
    <div class="col l9 m9 s9"> 
    <a href="#" data-target="nav-mobile" class="sidenav-trigger">
      <i class="material-icons">menu</i></a>&nbsp;
    <a href="index.php"><img src="images/favicon.ico"/></a>&nbsp;
    <span id='user'> 
        <?php if(get_ses('login')) {echo strtok(ucwords(get_ses('name'))," ");} 
          else {echo "<a href='login.php'> login ".get_ses('name').get_ses('login').'</a>';} 
        ?> 
    </span>&nbsp;
    <a href="https://www.facebook.com/Golden-World-Jewellers-107445783929823" class="fa fa-facebook"></a>
    <a href="#" class="fa fa-twitter hide-on-small-only"></a>
    <!-- <a href="#" class="fa fa-google"></a> -->
    <!-- <a href="#" class="fa fa-linkedin"></a> -->
    <!-- <a href="#" class="fa fa-youtube"></a> -->
    <!-- <a href="#" class="fa fa-instagram"></a> -->
    <!-- <a href="#" class="fa fa-pinterest"></a> -->
    <button id="butInstall" aria-label="Install" hidden></button>
    <!-- <button id="butRefresh" aria-label="Refresh"></button> -->
    <!-- <button id="butLoan" title="Loan Interest Rate" aria-label="Loan"></button> -->
    <!-- <button id="butDep" title="Deposit Interest Rate" aria-label="Deposit"></button> -->
    </div>
    <div class="col l3 m3 s3 right-align hide-on-small-only" style="display:inline-block;">
      <a href="product.php"><i class="material-icons">search</i> </a>
      <input value="search" class="grey-text" style="width:200px;"></input>
    </div>
  </header>

  <main class="main">
  <ul id="nav-mobile" class="sidenav" style="z-index:2000;">
    <li><a href="index.php"><i class="material-icons">home</i>Home </a></li>
    <?php 
      if ((get_ses('role') === 'admin' || get_ses('role') === 'accounts')) 
          {echo 
            '<li><a href="product_write.php"><i class="material-icons">add</i>Prodcut Upload</a></li>
            <li><a href="category_write.php"><i class="material-icons">add</i>Category Upload </a></li>
            <li><a href="carousel_write.php"><i class="material-icons">add</i>Carousel Upload </a></li>'
            ;}
     ?>
    <!-- <li><a href="product_write.php"><i class="material-icons">add</i>Prodcut Upload </a></li> -->
    <!-- <li><a href="cart.php"><i class="material-icons">send</i>Cart List </a></li> -->
    <!-- <li><a href="registration.php"><i class="material-icons">person_add</i>Registration </a></li> -->
    <!-- <li><a href="login.php"><i class="material-icons">local_grocery_store</i>Login </a></li> -->
    <li><a href="about.php"><i class="material-icons">access_time</i>About us </a></li>
    <li><a href="md.php"><i class="material-icons">filter_vintage</i>Managing Director's Profile </a></li>
    <li><a href="message.php"><i class="material-icons">record_voice_over</i>Managing Director's Message</a></li>
    <li><a href="gallery.php"><i class="material-icons">emoji_events</i>Events & Gallery </a></li>
    <li><a href="product.php"><i class="material-icons">image</i>All products </a></li>
    <li><a href="contactus.php"><i class="material-icons">people</i>Contact Us</a></li>
    <li><a href="help.php"><i class="material-icons">help</i>Help</a></li>
    <li><a href="examples/webcam.html"><i class="material-icons">help</i>Augmented Reality</a></li>
    <?php if (get_ses('login') != null && get_ses('login') == true) {
      echo '<li><a href="logout.php">
              <i class="material-icons">power_settings_new</i>log out </a>
            </li>';}
      else {
      echo '<li><a href="login.php"><i class="material-icons">person</i>log in </a>
            </li>';
          } ?>
    </ul>