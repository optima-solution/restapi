<?php include 'header.php' ?>
<div id="root">
    <div id="car" class="carousel carousel-slider center-align">
        <div class="carousel-item black white-text center-align" v-for="elem in carsels" :key="elem.id">
            <h5>{{elem.heading}}</h5>
            <img id="car_img" class="activator" style="width: 100%; height: 100%; object-fit: contain;" v-bind:src="'images/uploads/carousel/'+elem.image">
            <p>{{elem.heading}}</p>

            <!-- <h6>Opening of Branch</h6> -->
            <!-- <img class="activator" style="width: 100%; height: 100%; object-fit: contain;" src="images/uploads/carousel/eidmubarak.png">
            <p>Eid Mubarak</p> -->
        </div>
	</div>
</div>
<div class="row center-align white-text" style="background-color:#e68a00;">
	<div class="col s12 m12 l12">
        <h5 style="color: white; font-size: 30px;"><b>ABOUT US</b></h5>
		<div style="padding-left: 5%; padding-right: 5%; text-align: justify !important; font-size: 18px; color: #000033;">
			<p>
                &nbsp;&nbsp;&nbsp;&nbsp;We are proud to introduce Golden World Ltd. one of the leading Jewellery house, where quality, selection, value, trust, innovative design and servce has been trademark of excellence. Golden World Ltd. started its journey in the year 2010 under the supervision of its founder and Managing Director, Mrs. Farida Hossain. All this have been earned by providing approach and exliminary portfolio of unque and exclusive Diamond, Gold, Knudan and Platinum Jewellery have made us the most sought brand image among the fashionable socialities corporate high fliers, working women, celebrities and media luminaries. We are one of the first company who came up with the concept of online Jewellery selling in the country with the focus to tap the younger generation and also from their near and dear ones. Golden World offers the largest collection of Gold and Diamond Jewellery in the countryr and the customers has the option to scroll the entire range as per their requirement. Our artisans are the best in the world and value of customer attention and satisfaction and we benefit taking in to consideration all aspect of healthy and honest trade practices. The Management of Golden World dreams of making online Jewellery business a success model and make our country and every Bangladeshi proud. As an extenstion of sucessfule relationship of soul mate Golden world is being nurtured as a brand that enriches a woman's life and make us believe that you will celebrate your desire with gold, diamonds and all other jewellery adding beauty to everyone's life.<br/> 
                <br/>&nbsp;&nbsp;&nbsp;&nbsp;Golden World Ltd. established its first show room at 2nd floor of Pink City, Gulshan, Dhaka. Within very short, its expanded its foot print to Jamuna Future Park (2nd Floor), Bashundhara R/A, Dhaka.<br/> 
                <br/>&nbsp;&nbsp;&nbsp;&nbsp;The big advantage of Golden World Ltd. is Mrs. Farida Hossain visited all over the world specially manufacturing hub and moderate and craft. The designs of ornaments are always being updated. With her graduated knowldedge of atristic ornament fashions golden world jewellers achieved today's height.<br/> 
                <br/>&nbsp;&nbsp;&nbsp;&nbsp;She has been elected as Executive Commitee Member of prestigious Bangladesh Jewellery Somity (Bajus). Under her supervision Golden World Ltd. has achieved the gold import license from Bangladesh Bank. Currently only a few companies have got this license.
                <!-- Golden World started its journey In the year 2010 under the supervision of it founder and Managing Director, Mrs. Farida Hossain. Golden World deals with the both Gold and Diamond ornaments which have very high value as fashion symbol and social status. -->
			</p>
		</div>
	</div>
</div>
<?php include 'footer.php' ?>

<script src="scripts/vue.js"></script>
        <script src="scripts/axios.min.js"></script>
        <script>
        var app = new Vue({
              el: '#root',
              data () {
                return {
                  posts: null,
                  loading: true,
                  errored: false,
                  prd_cat: null,
                  carsels: null
                }
              },
             
              mounted () {
                  
                    axios.get("crud/carousel_c.php?FunctionType=read&&status=show&&pagename=about")
                    .then(response => {
                        console.log('response1:',response.data.carousels)
                        this.carsels = response.data.carousels
                    }).catch(err => {
                        console.log(err)
                    });                   
                  }
            })
        </script>
        <script type="text/javascript">
            //start of jquery
            (function($){
                $(function(){
                    setTimeout(function(){ 
                        $('.carousel').carousel({
                    fullWidth: true,
                    indicators: true,
                    duration: 200
                });
            autoplay();      
                    }, 1200);
                              
            function autoplay() {
                    $('.carousel').carousel('next');
                    setTimeout(autoplay, 5000);
                }
            }); // end of document ready
            })(jQuery); // end of jQuery name space
            
            // Register service worker.
            if ('serviceWorker' in navigator) {
                window.addEventListener('load', () => {
                    navigator.serviceWorker.register('service-worker.js')
                        .then((reg) => {
                            console.log('Service worker registered.', reg);
                        });
                });
            }
        </script>
<?php include 'endfooter.php';?>