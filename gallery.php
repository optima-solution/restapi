<?php include 'header.php';?>
<style type="">
	div #myclass {
		border: solid 2px #ffcc00; 
		margin-left: 4%;
		margin-top: 2%; 
		margin-bottom: 1%;
	}
</style>
<div class="row black white-text center-align ">
	<h3 style="padding-top: 2%;" class=" blue-text">GALLERY AND EVENTS OF GOLDEN WORLD LTD</h3>
    <div class="col l5 m6 s12 black white-text center-align" id="myclass">
      <h5>Bangladesh Cable TV Program</h5>
      <div class="card center-align" >
        <div class="card-image waves-effect waves-block waves-light black  center-align">
          <img class="activator responsive-img" src='images/uploads/events/bdcabletv.jpg' style="object-fit:contain">
          </img>
          <p>Conference of Bangladesh Cable TV</p>
        </div>
      </div>
    </div>
    <div class="col l5 m6 s12 black white-text center-align" id="myclass">
      <h5>Bangladesh Cable TV Program</h5>
      <div class="card  center-align" >
        <div class="card-image waves-effect waves-block waves-light black  center-align">
          <img class="activator responsive-img" src='images/uploads/events/BdCableTv1.jpg' style="object-fit:contain">
          </img>
          <p>Conference of Bangladesh Cable TV</p>
        </div>
      </div>
    </div>
    <div class="col l5 m6 s12 black white-text center-align" id="myclass">
      <h5>Bangladesh Cable TV Program</h5>
      <div class="card" >
        <div class="card-image waves-effect waves-block waves-light black">
          <img class="activator responsive-img" src='images/uploads/events/bdcabletv2.jpg' style="object-fit:contain">
            <p>Conference of Bangladesh Cable TV</p>
          </img>
        </div>
      </div>
    </div>
    <div class="col l5 m6 12 black white-text center-align" id="myclass">
      <h5>Bangladesh Cable TV Program</h5>
      <div class="card" >
        <div class="card-image waves-effect waves-block waves-light  black">
          <img class="activator responsive-img" src='images/uploads/events/bdcabletv3.jpg' style="object-fit:contain">
            <p>Conference of Bangladesh Cable TV</p>
          </img>
        </div>
      </div>
    </div>
    <div class="col l5 m6 s12 black white-text center-align" id="myclass">
      <h5>Bangladesh Cable TV Program</h5>
      <div class="card" >
        <div class="card-image waves-effect waves-block waves-light black">
          <img class="activator responsive-img" src='images/uploads/events/conference.jpg' style="object-fit:contain">
            <p>conference</p>
          </img>
        </div>
      </div>
    </div>
    <div class="col l5 m6 s12 black white-text center-align" id="myclass">
      <h5>Jamuna Future Park Branch Opening</h5>
      <div class="card" >
        <div class="card-image waves-effect waves-block waves-light black">
          <img class="activator responsive-img" src='images/uploads/events/jamunaopening.jpg' style="object-fit:contain">
            <p>Jamuna Future Park Branch Opening</p>
          </img>
        </div>
      </div>
    </div>
    <div class="col l5 m6 s12 black white-text center-align" id="myclass">
      <h5>Jamuna Future Park Branch Opening</h5>
      <div class="card" >
        <div class="card-image waves-effect waves-block waves-light black">
          <img class="activator responsive-img" src='images/uploads/events/jamunaopening1.jpg' style="object-fit:contain">
            <p>Jamuna Future Park Branch Opening</p>
          </img>
        </div>
      </div>
    </div>
    <div class="col l5 m6 s12 black white-text center-align" id="myclass">
      <h5>Jamuna Future Park Branch Opening</h5>
      <div class="card" >
        <div class="card-image waves-effect waves-block waves-light black">
          <img class="activator responsive-img" src='images/uploads/events/jamunaopening2.jpg' style="object-fit:contain">
            <p>Jamuna Future Park Branch Opening</p>
          </img>
        </div>
      </div>
    </div>
</div>

<?php include 'footer.php';?>