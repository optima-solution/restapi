<?php include 'header.php' ?>
<div id="root">
  <div class="row">
    <h5 class="left">Carousel Upload</h5>&nbsp;&nbsp;&nbsp;
    <a class="waves-effect waves-light btn modal-trigger" href="#addmodal" style="margin-top: 10px;" @click="showingBtnSave = true; showingBtnUpdate = false; resetValue;">Add New Carousel</a>

    <table class="striped responsive-table">
      <tr>
        <th>ID</th>
        <th>Carousel Name</th>
        <th>Heading</th>
        <th>Description</th>
        <th>image</th>
        <th>Edit</th>
        <th>Delete</th>
      </tr>
      <tr v-for="carousel in carousels">
        <td>{{carousel.id}}</td>
        <td>{{carousel.name}}</td>
        <td>{{carousel.heading}}</td>
        <td>{{carousel.description}}</td>
        <td>{{carousel.image}}</td>
        <td><a class="waves-effect waves-light btn modal-trigger" href="#addmodal" @click="showingBtnSave = false;showingBtnUpdate = true; selectCarousel(carousel)">Edit</a></td>
        <td><button class="waves-effect waves-light btn" @click="deleteCarousel(carousel)" >Delete</button></td>
      </tr>
    </table>
  </div> <!--row-->
  

  <div class="modal" style="width:100%; height: 800px;" id="addmodal">
      <div class="modal-content">
          <div class="row">
            <div class="col l6 left-align">
              <h6>Add/Edit Carousel</h6>
              <!-- <h6 id="Msg"></h6> -->
            </div>
            <div class="col l6 right-align">
              <button class="modal-close waves-green right-align btn-small">Close</button>
            </div>
          </div>
          <div class="row">
            <div class="input-field col s6 m4 l4 left-align">
                <label>Carousel id</label><br/>
                <input id="id" type="text" class="validate  black-text left-align" v-model="clickedCarousel.id" disabled="true">     
            </div>
            <div class="input-field col s6 m4 l4 left-align">
                <label>Carousel name</label><br/>
                <input id="name" type="text" class="validate  black-text left-align" v-model="clickedCarousel.name">     
            </div>
            <div class="input-field col s6 m4 l4">
                <label>Heading</label><br/>
                <input id="heading" type="text" class="validate  black-text" v-model="clickedCarousel.heading">
            </div>
            <div class="input-field col s6 m4 l4">
                <label>Description</label><br/>
                <input id="description" type="text" class="validate  black-text" v-model="clickedCarousel.description">
            </div>
            <div class="input-field col s6 m4 l4">
                <label for="image">Carousel Image(1100X650)</label>
                <br>
                <br>
                <img id="img_thumb"  style="width: 100px; height: 100px; object-fit:fill;">
                <input id="image" type="file" class="validate  black-text" onchange="pre_img();">
            </div>
            <div class="input-field col s6 m4 l4">
                <label>Status(show/hide)</label><br/>
                <input id="status" type="text" class="validate  black-text" v-model="clickedCarousel.status">
            </div>
            <div class="input-field col s6 m4 l4">
                <label>Page Name(home/md/message/about)</label><br/>
                <input id="pagename" type="text" class="validate  black-text" v-model="clickedCarousel.pagename">
            </div>
          </div><!--row--> 
      </div><!--modal-content row--> 
      <div class="row left-align">
          <button id="btnsave" class="waves-effect waves-light btn left-align" v-if="showingBtnSave">Save Carousel</button>
           <button id="btnupdate" class="waves-effect waves-light btn left-align" v-if="showingBtnUpdate">update Carousel</button><br/>
           <h6 id="Msg"></h6>
      </div>
  </div><!--addmodal-->


</div><!--root-->

<?php include 'footer.php' ?>
  <script type = "text/javascript" >
    $(document).ready(function () {
      $('.modal').modal();
      $('select').formSelect();
    });

  function init() {
    initializeServiceWorker(); //geting service worker ready
    initializeDB(); // getting indexeddb ready
  }
  init();

  // STEP OF PROCESS: REGISTER SERVICE WORKER->SAVE TO INDEXEDDB->SEND FOR SYNC
  //STEP IN SW: ->SYNC WHEN ONLINE->DELETE FROM INDEXEDDB 
  function initializeServiceWorker() {
    if (navigator.serviceWorker) { //checking if browser supports service workers
      navigator.serviceWorker.register('service-worker.js') //registering SW
        .then(function () {
          // console.log('service worker ready');
          return navigator.serviceWorker.ready;
        })
        .then(function (registration) {
          //btnsave: for new carousel uploads
          document.getElementById('btnsave').addEventListener('click', (event) => {
            document.getElementById('Msg').innerHTML='Please Wait..';
            //restricting user for further save click
            // console.log('inserting data');
            event.preventDefault();
            saveData('carousel_insert').then(function () {     
              //syncing for offline storage to service worker
              if (registration.sync) {
                // console.log('carousel-synced');
                registration.sync.register('carousel-sync')
                  // .then(()=>{alert('Carousel submitted for Upload');})
                  .catch(function (err) {
                    // console.log('sync err: ',err);
                    return err;
                  })
              } else {
                // sync isn't there so fallback
                //console.log('checking internet');
                checkInternet();
              }
            });
          }) //btnsave ends here
          document.getElementById('btnupdate').addEventListener('click', (event) => {
            document.getElementById('Msg').innerHTML='Please Wait..';
            //restricting user for further update click
            event.preventDefault();
            saveData('carousel_update').then(function () {
              console.log('saving data');
              if (registration.sync) {
                // console.log('carousel-synced');
                registration.sync.register('carousel-sync')
                  // .then(()=>{alert('carousel Upload Successful');})
                  .catch(function (err) {
                    // console.log('sync err: ',err);
                    return err;
                  })
              } else {
                // sync isn't there so fallback
                //console.log('checking internet');
                checkInternet();
              }
            });
          })//button update ends here
        })
    } // if navigator.serviceWorker okay ends
    // else {
      // document.getElementById('btnsave').addEventListener('click', (event) => {
      //   event.preventDefault();
      //   saveData('carousel_insert').then(function () {
      //     checkInternet();
      //   });
      // })
    // }
  }

  function initializeDB() {
    var carouselDB = window.indexedDB.open('carouselDB');
    carouselDB.onupgradeneeded = function (event) {
      var db = event.target.result;
      var carouselTbl = db.createObjectStore("carouselTbl", {autoIncrement: true});
      carouselTbl.createIndex("FunctionType", "FunctionType", {unique: false});
      carouselTbl.createIndex("id", "id", {unique: true});
      carouselTbl.createIndex("name", "name", {unique: false});
      carouselTbl.createIndex("heading", "heading", {unique: false});
      carouselTbl.createIndex("description", "description", {unique: false});
      carouselTbl.createIndex("image", "image", {unique: false});
      carouselTbl.createIndex("status", "status", {unique: false});
      carouselTbl.createIndex("pagename", "pagename", {unique: false});
      //sending authorization credentials
      carouselTbl.createIndex("loginId", "loginId", {unique: false});
      carouselTbl.createIndex("auth_token", "auth_token", {unique: false});
    }
  }

  //Convert image to BLOB for IndexedDB
  function set_img_res(inputfile) {
    return new Promise(function (resolve, reject) {
      if (inputfile.value) {
        var file = inputfile.files[0];
        reader = new FileReader();
        reader.onloadend = function () {
          // resolve(reader.result);
          ResizeImage(reader.result,1100,650).then(function(res){resolve(res);});
        }
        reader.readAsDataURL(file);
      } else {
        resolve("");
      }
    })
  }

  async function saveData(FunctionType) {
    return new Promise(function (resolve, reject) {
      // TAKING IMAGE DATA TO BLOB
      let image_res = "";

      var inputfile = document.getElementById('image');

      set_img_res(inputfile).then(function (result) {
        image_res = result; //main image
        // console.log('img',image_res);
      }).then(function () {
        var msg = '';
        // console.log('img file checking');
        var tmpObj = {
          FunctionType: FunctionType,
          id: document.getElementById('id').value,
          name: document.getElementById('name').value,
          heading: document.getElementById('heading').value,
          description: document.getElementById('description').value,
          image: image_res,
          status: document.getElementById('status').value,
          pagename: document.getElementById('pagename').value,
          loginId: <?php echo "'".get_ses('loginID')."'"; ?>,
          auth_token: <?php echo "'".get_ses('auth_token')."'"; ?>
        };
        // console.log('file:', tmpObj);
        var myDB = window.indexedDB.open('carouselDB');
        myDB.onsuccess = function (event) {
          var objStore = this.result.transaction('carouselTbl', 'readwrite').objectStore('carouselTbl');
          objStore.add(tmpObj);
          msg = 'successfully';
          resolve();
        }

        myDB.onerror = function (err) {
          msg = 'error';
          reject(err);
        }
      })
    }) //promise
  } //function savedata()
  function checkInternet() {
    event.preventDefault();
    if (navigator.onLine) {
      console.log('online');
    } else {
      alert("You are offline! When your internet returns, we'll finish up your request.");
    }
  }
  window.addEventListener('offline', function () {
    alert('You have lost internet access!');
  });
  //previe image as thumbnail
  function pre_img() {
    readURL(document.getElementById("image"), "img_thumb");
    // $("#image").change(function() {readURL(this,"img_thumb");});
    function readURL(input, img_thumb) {
      if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
          $('#' + img_thumb).attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]); // convert to base64 string
      }
    }
  }
  </script>

<script src="scripts/vue.js"></script>
<script src="scripts/axios.min.js"></script>
<script type="text/javascript">
  var app = new Vue({

  el: "#root",
  data: {
    showingdeleteModal: false,
    showingBtnSave: true,
    showingBtnUpdate: true,
    carousels: [],
    clickedCarousel: {}
  },
  mounted: 
  function () {
    this.getAllCarousel();
  },
  
  methods: {
    resetValue(){
        document.getElementById('id').value = '';
    },
    getAllCarousel: function(){
      axios.get("crud/carousel_c.php?FunctionType=read")
      .then(function(response){
        console.log("response:",response.data.carousels);
        if (response.data.error) {
          app.errorMessage = response.data.message;
        }else{
          app.carousels = response.data.carousels;
        }
      });
    },
    selectCarousel(carousel){
        app.clickedCarousel = carousel;
        // console.log('src', 'images/uploads/carousel/' + carousel.image);
        document.getElementById('img_thumb').src='images/uploads/carousel/' + carousel.image;
      },
    deleteCarousel:function(carousel){
        // var formData = app.toFormData(app.clickedUser);
        app.clickedCarousel = carousel;
        if(confirm("Do you really want to delete?")){
          var  loginId= <?php echo "'".get_ses('loginID')."'"; ?>;
          var auth_token=<?php echo "'".get_ses('auth_token')."'"; ?>;
          var url="crud/carousel_c.php?FunctionType=delete&&id="+carousel.id+"&&loginID="+loginId+"&&auth_token="+auth_token;
          console.log(url);
          axios.get(url)
            .then(function(response){
              console.log(response);
              app.clickedUser = {};
              if (response.data.error) {
                app.errorMessage = response.data.message;
              }else{
                app.successMessage = response.data.message;
                alert(response.data);
                app.getAllCarousel();
              }
            });          
        }
      },
      toFormData: function(obj){
        var form_data = new FormData();
            for ( var key in obj ) {
                form_data.append(key, obj[key]);
            } 
            return form_data;
      }    
  }
});
</script>
<?php include 'endfooter.php' ?>