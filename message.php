<?php include 'header.php' ?>
<div id="root">
    <div id="car" class="carousel carousel-slider center-align car_div_slider black">
        <div class="carousel-item black white-text center-align car_div" v-for="elem in carsels" :key="elem.id">
            <h6>{{elem.heading}}</h6>
            <img id="car_img" class="activator car_img" style="object-fit: contain;" v-bind:src="'images/uploads/carousel/'+elem.image">

            <!-- <h6>Opening of Branch</h6> -->
            <!-- <img class="activator" style="width: 100%; height: 100%; object-fit: contain;" src="images/uploads/carousel/eidmubarak.png">
            <p>Eid Mubarak</p> -->
        </div>
	</div>
</div>
<div class="row center-align white-text"  style="background-color:#e68a00;">
	<div class="col s12 m12 l12">
        <h5 tyle="color: white; font-size: 30px;"><b> MESSAGE FROM MANAGING DIRECTOR</b></h5>
		<div style="padding-left: 5%; padding-right: 5%; text-align: justify !important; font-size: 18px; color: #000033;">
            <p>Assalamu Alaikum <br/>
            <br/>&nbsp;&nbsp;&nbsp;&nbsp;I welcome you all to Golden World. Besides my other business, I have started Golden World in 2010 because I found some interesting characteristics of gold which now I want to share with you. Gold is a unique metal which got the following unique features:
            <ul><li>1. &nbsp; Econmic value.</li><li>2. &nbsp;Scientific value.</li><li>3. &nbsp; Second to none aesthetic value.</li></ul>
            Gold became a part of every human culture from ancient period where they even equated gold with gods and rulers, and gold was sought in their name and dedicated to their glorification. Humans almost intuitively place a high value on gold, equating it with power, beauty, and the cultural elite. Even today, most of the central banks of different countries reserve gold as their back up currency including USA, Bangladesh etc. Gold is also scientifically of same importance.<br/>
            <br/>&nbsp;&nbsp;&nbsp;&nbsp;Hence, I concluded that Gold is a good saving instrument which can be simultaneously enjoyed and quickly liquidated. Then I establish Golden World Limited which preserves Golds of the highest standards around the globe and at the same time carries the subtle taste of personality, beauty and social status.<br/>
            <br/>Please enjoy your gold purchasing from us as symbol of fashion and beauty as well as mark it as a saving instrument for the future. <br/>
            <br/>Best wishes to all and Thank you for being with us.
            <br/><br/>Regards,
            <br/><h5>Farida Hossain</h5>Managing Director.<br/>Golden World Limited


			</p>
		</div>
	</div>
</div>
<?php include 'footer.php' ?>

<script src="scripts/vue.js"></script>
        <script src="scripts/axios.min.js"></script>
        <script>
        var app = new Vue({
              el: '#root',
              data () {
                return {
                  posts: null,
                  loading: true,
                  errored: false,
                  prd_cat: null,
                  carsels: null
                }
              },
             
              mounted () {
                  
                    axios.get("crud/carousel_c.php?FunctionType=read&&status=show&&pagename=message")
                    .then(response => {
                        console.log('response1:',response.data.carousels)
                        this.carsels = response.data.carousels
                    }).catch(err => {
                        console.log(err)
                    });                   
                  }
            })
        </script>
        <script type="text/javascript">
            //start of jquery
            (function($){
                $(function(){
                    setTimeout(function(){ 
                        $('.carousel').carousel({
                    fullWidth: true,
                    indicators: true,
                    duration: 200
                });
            // autoplay();      
                    }, 1200);
                              
            function autoplay() {
                    $('.carousel').carousel('next');
                    setTimeout(autoplay, 5000);
                }
            }); // end of document ready
            })(jQuery); // end of jQuery name space
            
            // Register service worker.
            if ('serviceWorker' in navigator) {
                window.addEventListener('load', () => {
                    navigator.serviceWorker.register('service-worker.js')
                        .then((reg) => {
                            console.log('Service worker registered.', reg);
                        });
                });
            }
        </script>
<?php include 'endfooter.php';?>