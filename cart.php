<?php include 'header.php' ?>
<div id="root">
  <div class="row">
    <h5 class="left">Category Upload</h5>&nbsp;&nbsp;&nbsp;
    <a class="waves-effect waves-light btn modal-trigger" href="#addmodal" style="margin-top: 10px;" @click="showingBtnSave = true; showingBtnUpdate = false; resetValue;">Add New Category</a>

    <table class="striped responsive-table">
      <tr>
        <th>ID</th>
        <th>Category Name</th>
        <th>description</th>
        <th>catagory type</th>
        <th>image</th>
        <th>Edit</th>
        <th>Delete</th>
      </tr>
      <tr v-for="category in catagories">
        <td>{{category.id}}</td>
        <td>{{category.name}}</td>
        <td>{{category.description}}</td>
        <td>{{category.main_catagory_id}}</td>
        <td>{{category.image}}</td>
        <td><a class="waves-effect waves-light btn modal-trigger" href="#addmodal" @click="showingBtnSave = false;showingBtnUpdate = true; selectCategory(category)">Edit</a></td>
        <td><button @click="selectCategory(category)" >Delete</button></td>
      </tr>
    </table>
  </div> <!--row-->
  

  <div class="modal" style="width:100%; height: 800px;" id="addmodal">
      <div class="modal-content">
          <div class="row">
            <div class="col l6 left-align">
              <h6>Add/Edit category</h6>
              <h6 id="Msg"></h6>
            </div>
            <div class="col l6 right-align">
              <button class="modal-close waves-green right-align btn-small">Close</button>
            </div>
          </div>
          <div class="row">
            <div class="input-field col s6 m4 l4 left-align">
                <label>Category id</label><br/>
                <input id="id" type="text" class="validate  black-text left-align" v-model="clickedCategory.id" disabled="true">     
            </div>
            <div class="input-field col s6 m4 l4 left-align">
                <label>Category name</label><br/>
                <input id="name" type="text" class="validate  black-text left-align" v-model="clickedCategory.name">     
            </div>
            <div class="input-field col s6 m4 l4">
                <label>Description</label><br/>
                <input id="description" type="text" class="validate  black-text" v-model="clickedCategory.description">
            </div>
            <div class="input-field col s6 m4 l4" id="select_box">
                <label>Catagory Type</label><br/>
                <!-- <input id="main_catagory_id" type="text" class="validate  black-text" v-model="clickedCategory.main_catagory_id"> -->
                
            </div>
            <div class="input-field col s6 m4 l4">
                <label for="image">Category Image(main:600X400 alternative: 600X900)</label>
                <br>
                <br>
                <img id="img_thumb"  style="width: 100px; height: 100px; object-fit:fill;">
                <input id="image" type="file" class="validate  black-text" onchange="pre_img();">
            </div>
          </div><!--row--> 
      </div><!--modal-content row--> 
      <div class="row left-align">
          <button id="btnsave" class="waves-effect waves-light btn left-align" v-if="showingBtnSave">Save Category</button>
           <button id="btnupdate" class="waves-effect waves-light btn left-align" v-if="showingBtnUpdate">update Category</button>
      </div>
  </div><!--addmodal-->


</div><!--root-->

<?php include 'footer.php' ?>
  <script type = "text/javascript" >
  
  var catagory;
  var sel_catagory_id;
    $(document).ready(function () {
      $('.modal').modal();
      $('select').formSelect();
      $('a.modal-trigger').click(function(){
        //   console.log('clicked', catagory);
        let select_view = '';
        let sel = '';
        for (let i = 0; i < catagory.length; ++i) {
            if(catagory[i].value == sel_catagory_id){
                sel = 'selected';
            }else{
                sel = '';
            }
            select_view += `<option value="${catagory[i].value}" ${sel}>${catagory[i].text}</option>`;
        }
        
        select_view = `<label>Catagory Type</label><br><select id="main_catagory_id">${select_view}</select>`;
        $('#select_box').html(select_view);
        $('#main_catagory_id').formSelect();
      });
    });

  function init() {
    initializeServiceWorker(); //geting service worker ready
    initializeDB(); // getting indexeddb ready
  }
  init();

  // STEP OF PROCESS: REGISTER SERVICE WORKER->SAVE TO INDEXEDDB->SEND FOR SYNC
  //STEP IN SW: ->SYNC WHEN ONLINE->DELETE FROM INDEXEDDB 
  function initializeServiceWorker() {
    if (navigator.serviceWorker) { //checking if browser supports service workers
      navigator.serviceWorker.register('service-worker.js') //registering SW
        .then(function () {
          // console.log('service worker ready');
          return navigator.serviceWorker.ready;
        })
        .then(function (registration) {
          //btnsave: for new category uploads
          document.getElementById('btnsave').addEventListener('click', (event) => {
            //restricting user for further save click
            // console.log('inserting data');
            event.preventDefault();
            saveData('category_insert').then(function () {     
              //syncing for offline storage to service worker
              if (registration.sync) {
                // console.log('category-synced');
                registration.sync.register('category-sync')
                  // .then(()=>{alert('Category submitted for Upload');})
                  .catch(function (err) {
                    // console.log('sync err: ',err);
                    return err;
                  })
              } else {
                // sync isn't there so fallback
                //console.log('checking internet');
                checkInternet();
              }
            });
          }) //btnsave ends here
          document.getElementById('btnupdate').addEventListener('click', (event) => {
            //restricting user for further update click
            event.preventDefault();
            saveData('category_update').then(function () {
              console.log('saving data');
              if (registration.sync) {
                // console.log('category-synced');
                registration.sync.register('category-sync')
                  // .then(()=>{alert('Category Upload Successful');})
                  .catch(function (err) {
                    // console.log('sync err: ',err);
                    return err;
                  })
              } else {
                // sync isn't there so fallback
                //console.log('checking internet');
                checkInternet();
              }
            });
          })//button update ends here
        })
    } // if navigator.serviceWorker okay ends
    // else {
      // document.getElementById('btnsave').addEventListener('click', (event) => {
      //   event.preventDefault();
      //   saveData('category_insert').then(function () {
      //     checkInternet();
      //   });
      // })
    // }
  }

  function initializeDB() {
    var categoryDB = window.indexedDB.open('categoryDB');
    categoryDB.onupgradeneeded = function (event) {
      var db = event.target.result;
      var categoryTbl = db.createObjectStore("categoryTbl", {autoIncrement: true});
      categoryTbl.createIndex("FunctionType", "FunctionType", {unique: false});
      categoryTbl.createIndex("id", "id", {unique: true});
      categoryTbl.createIndex("name", "name", {unique: false});
      categoryTbl.createIndex("description", "description", {unique: false});
      categoryTbl.createIndex("main_catagory_id", "main_catagory_id", {unique: false});
      categoryTbl.createIndex("image", "image", {unique: false});
      //sending authorization credentials
      categoryTbl.createIndex("loginId", "loginId", {unique: false});
      categoryTbl.createIndex("auth_token", "auth_token", {unique: false});
    }
  }

  //Convert image to BLOB for IndexedDB
  function set_img_res(inputfile) {
    return new Promise(function (resolve, reject) {
      if (inputfile.value) {
        var file = inputfile.files[0];
        reader = new FileReader();
        reader.onloadend = function () {
          resolve(reader.result);
        }
        reader.readAsDataURL(file);
      } else {
        resolve("");
      }
    })
  }

  async function saveData(FunctionType) {
    return new Promise(function (resolve, reject) {
      // TAKING IMAGE DATA TO BLOB
      let image_res = "";

      var inputfile = document.getElementById('image');

      set_img_res(inputfile).then(function (result) {
        image_res = result; //main image
        // console.log('img',image_res);
      }).then(function () {
        var msg = '';
        // console.log('img file checking');
        var tmpObj = {
          FunctionType: FunctionType,
          id: document.getElementById('id').value,
          name: document.getElementById('name').value,
          description: document.getElementById('description').value,
          main_catagory_id: document.getElementById('main_catagory_id').value,
          image: image_res,
          loginId: <?php echo "'".get_ses('loginID')."'"; ?>,
          auth_token: <?php echo "'".get_ses('auth_token')."'"; ?>
        };
        // console.log('file:', tmpObj);
        var myDB = window.indexedDB.open('categoryDB');
        myDB.onsuccess = function (event) {
          var objStore = this.result.transaction('categoryTbl', 'readwrite').objectStore('categoryTbl');
          objStore.add(tmpObj);
          msg = 'successfully';
          resolve();
        }

        myDB.onerror = function (err) {
          msg = 'error';
          reject(err);
        }
      })
    }) //promise
  } //function savedata()
  function checkInternet() {
    event.preventDefault();
    if (navigator.onLine) {
      console.log('online');
    } else {
      alert("You are offline! When your internet returns, we'll finish up your request.");
    }
  }
  window.addEventListener('offline', function () {
    alert('You have lost internet access!');
  });
  //previe image as thumbnail
  function pre_img() {
    readURL(document.getElementById("image"), "img_thumb");
    // $("#image").change(function() {readURL(this,"img_thumb");});
    function readURL(input, img_thumb) {
      if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
          $('#' + img_thumb).attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]); // convert to base64 string
      }
    }
  }
  </script>

<script src="scripts/vue.js"></script>
<script src="scripts/axios.min.js"></script>
<script type="text/javascript">
  var app = new Vue({

  el: "#root",
  data: {
    showingdeleteModal: false,
    showingBtnSave: true,
    showingBtnUpdate: true,
    catagories: [],
    clickedCategory: {},
    selected: '1',
    options: [
      { text: 'Main Category', value: '1' },
      { text: 'Alternative Category', value: '-1' }
    ]
  },
  mounted: 
  function () {
    this.getAllCategories();
    catagory = this.options;
  },
  
  methods: {
    resetValue(){
        document.getElementById('id').value = '';
    },
    getAllCategories: function(){
      axios.get("crud/category_c.php?FunctionType=read")
      .then(function(response){
        console.log("response:",response.data.catagories);
        if (response.data.error) {
          app.errorMessage = response.data.message;
        }else{
          app.catagories = response.data.catagories;
        }
      });
    },
    selectCategory(Category){
        app.clickedCategory = Category;
        console.log('src', 'images/uploads/' + Category.image);
        document.getElementById('img_thumb').src='images/uploads/' + Category.image;
        selected=Category.main_catagory_id;
        sel_catagory_id = selected;
        console.log('selected',selected);
      },
    deleteUser:function(){
      var formData = app.toFormData(app.clickedUser);
      axios.post("api.php?action=delete", formData)
        .then(function(response){
          console.log(response);
          app.clickedUser = {};
          if (response.data.error) {
            app.errorMessage = response.data.message;
          }else{
            app.successMessage = response.data.message;
            app.getAllCategories();
          }
        });
      },
      toFormData: function(obj){
        var form_data = new FormData();
            for ( var key in obj ) {
                form_data.append(key, obj[key]);
            } 
            return form_data;
      }    
  }
});
</script>
