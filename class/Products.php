<?php
class Products{   
    	
    private $productTable = "0_products";
	public $id;
	public $code;
	public $name;
	public $category;
	public $image;
	public $description;
	public $original_price;
	public $discount_amt;
	public $discount_per;
	public $net_price;
	public $status;
	public $image1;
	public $image2;
	public $image3;
	public $image4;
	public $created_by;
	public $creation_date;
	public $last_updated_by;
	public $last_update_date;
	private $conn;

    public function __construct($db){
        $this->conn = $db;
    }	
	
	function read(){	
		// print_r ('id:'.$this->id.'cate:'.$this->category_id.PHP_EOL);
		if($this->id && $this->category) {
			$stmt = $this->conn->prepare("SELECT * FROM ".$this->productTable." WHERE id = ? and category = ?");
			// print_r('case1');
			$stmt->bind_param("ii", $this->id ,$this->category);					
		} elseif($this->id) {
			$stmt = $this->conn->prepare("SELECT * FROM ".$this->productTable." WHERE id = ?");
			// print_r('case2');
			$stmt->bind_param("i", $this->id);					
		} elseif($this->category) {
			$stmt = $this->conn->prepare("SELECT * FROM ".$this->productTable." WHERE category = ?");
			// print_r('case3');
			$stmt->bind_param("i", $this->category);					
		} 

		else {
			// print_r('case4');
			$stmt = $this->conn->prepare("SELECT * FROM ".$this->productTable);		
		}		

		$stmt->execute();			
		$result = $stmt->get_result();		
		return $result;	
	}
	
	function create(){
		
		$stmt = $this->conn->prepare("
			INSERT INTO ".$this->productTable." `code`,`name`,`category`,`image`, `description`, `original_price`, `discount_amt`,`discount_per`,`net_price`, `status`, `image1`,`image2`,`image3`,`image4`,`created_by`, `creation_date`, `last_updated_by`,`last_update_date` )
			VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
		
		$this->code=htmlspecialchars(strip_tags($this->code));
		$this->name=htmlspecialchars(strip_tags($this->name));
		$this->category=htmlspecialchars(strip_tags($this->category));
		$this->image=htmlspecialchars(strip_tags($this->image));
		$this->description=htmlspecialchars(strip_tags($this->description));
		$this->original_price=htmlspecialchars(strip_tags($this->original_price));
		$this->discount_amt=htmlspecialchars(strip_tags($this->discount_amt));
		$this->discount_per=htmlspecialchars(strip_tags($this->discount_per));
		$this->net_price=htmlspecialchars(strip_tags($this->net_price));
		$this->status=htmlspecialchars(strip_tags($this->status));
		$this->image1=htmlspecialchars(strip_tags($this->image1));
		$this->image2=htmlspecialchars(strip_tags($this->image2));
		$this->image3=htmlspecialchars(strip_tags($this->image3));
		$this->image4=htmlspecialchars(strip_tags($this->image4));
		$this->created_by=htmlspecialchars(strip_tags($this->created_by));
		$this->creation_date=htmlspecialchars(strip_tags($this->creation_date));
		$this->last_updated_by=htmlspecialchars(strip_tags($this->last_updated_by));
		$this->last_update_date=htmlspecialchars(strip_tags($this->last_update_date));

		
		$stmt->bind_param("ssissddddssssssisi", $this->code,$this->name,$this->category,$this->image,$this->description,$this->original_price,$this->discount_amt,$this->discount_per,$this->net_price,$this->status,$this->image1,$this->image2,$this->image3,$this->image4,$this->created_by,$this->creation_date,$this->last_updated_by,$this->last_update_date);
		
		if($stmt->execute()){
			return true;
		}
	 
		return false;		 
	}
		
	function update(){
	 
		$stmt = $this->conn->prepare("
			UPDATE ".$this->productTable."code=?, name=?, category=?, image=?, description=?, original_price=?, discount_amt=?, discount_per=?, net_price=?, status=?, image1=?, image2=?, image3=?, image4=?, created_by=?, creation_date=?, last_updated_by=?, last_update_date=?
				WHERE id = ?");
	 
			$this->id = htmlspecialchars(strip_tags($this->id));
			$this->code=htmlspecialchars(strip_tags($this->code));
			$this->name=htmlspecialchars(strip_tags($this->name));
			$this->category=htmlspecialchars(strip_tags($this->category));
			$this->image=htmlspecialchars(strip_tags($this->image));
			$this->description=htmlspecialchars(strip_tags($this->description));
			$this->original_price=htmlspecialchars(strip_tags($this->original_price));
			$this->discount_amt=htmlspecialchars(strip_tags($this->discount_amt));
			$this->discount_per=htmlspecialchars(strip_tags($this->discount_per));
			$this->net_price=htmlspecialchars(strip_tags($this->net_price));
			$this->status=htmlspecialchars(strip_tags($this->status));
			$this->image1=htmlspecialchars(strip_tags($this->image1));
			$this->image2=htmlspecialchars(strip_tags($this->image2));
			$this->image3=htmlspecialchars(strip_tags($this->image3));
			$this->image4=htmlspecialchars(strip_tags($this->image4));
			$this->created_by=htmlspecialchars(strip_tags($this->created_by));
			$this->creation_date=htmlspecialchars(strip_tags($this->creation_date));
			$this->last_updated_by=htmlspecialchars(strip_tags($this->last_updated_by));
			$this->last_update_date=htmlspecialchars(strip_tags($this->last_update_date));
	 
		$stmt->bind_param("ssissddddssssssisii", $this->code,$this->name,$this->category,$this->image,$this->description,$this->original_price,$this->discount_amt,$this->discount_per,$this->net_price,$this->status,$this->image1,$this->image2,$this->image3,$this->image4,$this->created_by,$this->creation_date,$this->last_updated_by,$this->last_update_date, $this->id);
		
		if($stmt->execute()){
			return true;
		}
	 
		return false;
	}
	
	function delete(){
		
		$stmt = $this->conn->prepare("
			DELETE FROM ".$this->productTable." 
			WHERE id = ?");
			
		$this->id = htmlspecialchars(strip_tags($this->id));
	 
		$stmt->bind_param("i", $this->id);
	 
		if($stmt->execute()){
			return true;
		}
	 
		return false;		 
	}
}
?>