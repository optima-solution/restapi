<?php include 'header.php';?>
<?php
include_once 'config/Database.php';
$database = new Database();
$db = $database->getConnection();
$id = $_GET['id'];
$cuser=get_ses('loginID');
$rating_id=0;
$comments='';
$rating=0;
if (isset($_POST['save_comment'])){
	// print_r('rating id:'.$_GET['rating_id']);die();
	$rating_id=$_GET['rating_id'];
	$comments=$_POST['comments'];
	$rating=$_POST['rating'];
	$updated_at = date('Y-m-d H:i:s');
	$sql =" UPDATE 0_ratings set rating='".$rating."' , comments='".$comments."', updated_at='".$updated_at."' WHERE id = '" . $_GET['rating_id'] . "'";
	// print_r($sql); die();
	// $result = $db->query($sql);
	if ($db->query($sql) === true){
		echo "<script>alert('Successfully updated');</script>";
	}
}

if (isset($_POST['add_comment'])){
	// print_r('rating id:'.$_GET['rating_id']);die();
	if (!get_ses('login')){
		echo "<script>alert('Please login first');</script>";
	}
	else{
		$rating_id=$_GET['rating_id'];
		$comments=$_POST['comments'];
		$rating=$_POST['rating'];
		$created_at = date('Y-m-d H:i:s');
		$updated_at = date('Y-m-d H:i:s');
		$sql =" INSERT INTO 0_ratings (rating, comments,product_id,user_id,created_at,updated_at) VALUES('".$rating."' , '".$comments."', '".$id."', '".$cuser."', '".$created_at."', '".$updated_at."')";
		// print_r($sql); die();
		// $result = $db->query($sql);
		if ($db->query($sql) === true){
			echo "<script>alert('Successfully added');</script>";
		}
	}
}


//credential check start
$sql = "SELECT *, 0_ratings.updated_at as dt, 0_ratings.user_id as user_id, 0_ratings.id as id FROM 0_ratings left Join 0_users";
$sql .=" on  0_ratings.user_id=0_users.id";
$sql .=" WHERE product_id = '" . $id . "'";
// print_r($sql); die();
$result = $db->query($sql);
// print_r($result);die();
// $num_rows= $result -> num_rows;
$html = '<ul id="list-1">';
if ($result = $db -> query($sql)) {
	while ($row = $result -> fetch_assoc()) {
		$userRating=$row["rating"];
		$user_name=$row["name"];
		$img=$row["image"];
		if($cuser==$row["user_id"]){
			$rating_id=$row["id"];
			$comments=$row["comments"];
			$rating=$row["rating"];
		}
		// $html .='loginid'.$cuser.'userid'.$row["user_id"].'rating'.$rating.'comments'.$comments;
		$html .='<img class="circle" style="width:30px; height:30px; object-fit:fill;" src="images/uploads/users/'.$img.'">&nbsp;'.$user_name.'&nbsp;&nbsp;&nbsp;&nbsp;'.date("d-M-y", strtotime($row["dt"])).'<br/>';
		for ($count = 1; $count <= 5; $count ++) {
			// $starRatingId = $row['id'] . '_' . $count;
			if ($count <= $userRating) {  
				$html .='<li value="'.$count.' " class="star"> <img src="./images/favourite-filled.png"></li>'; 
			}
			else {
				$html .='<li value="'. $count.'" class="star"> <img src="./images/favourite-open.png"></li>';
			}
		}
		$html .='</br>'.$row["comments"].'<br/><hr/>';
	}	
  }
  	$html .= '</ul>';
	$result -> free_result();

	$htmluser = '<ul id="list-1">';
	$CurrentUserRating = $rating>0 ? $rating : 5;
	
	for ($count = 1; $count <= 5; $count ++) {
		// $starRatingId = $row['id'] . '_' . $count;
		if ($count <= $CurrentUserRating) {  
			$htmluser .='<li value="'.$count.' " class="star"> <img id="urating_id'.$count.'" onclick=addrating('.$count.') src="./images/favourite-filled.png" ></li>'; 
		}
		else {
			$htmluser .='<li value="'. $count.'" class="star"> <img id="urating_id'.$count.'" onclick=addrating('.$count.') src="./images/favourite-open.png"></li>';
		}
	}
?>


<div id="app" class="row black white-text" style="padding-left: 10%; padding-right:10%" >
	<div id="prdimgdiv" img-zoom-container></div>
	<div v-for="products in products" :key="products.id">
		<!-- <img v-bind:src="'images/uploads/allproducts/'+ products.image" style="width:100%; height:100%; max-width:400px; max-height: 400px; object-fit:fill;" /> -->
		<div  v-if="products.net_price>0">
			<p>Product Code &nbsp;&nbsp;&nbsp;&nbsp;: {{products.code}}</p>
			<p>Product Name &nbsp;&nbsp;&nbsp;&nbsp;: {{products.name}}</p>
			<p>Product Description&nbsp;: {{products.description}}</p>
			<p>Product Original Price &nbsp;: {{products.original_price|formatPrice}}</p>
			<p>Product Discount Amt &nbsp;: {{products.discount_amt|formatPrice}}</p>
			<p>Product Net Price &nbsp;: {{products.net_price|formatPrice}}</p>	
			<p><button @click="addToCart(products)" class="btn light-blue"><i class="material-icons">add_shopping_cart</i></button></p>
		</div>
		<div  v-else>
			<p>Product Code &nbsp;&nbsp;&nbsp;&nbsp;: {{products.code}}</p>
			<p>Product Name &nbsp;&nbsp;&nbsp;&nbsp;: {{products.name}}</p>
			<p>Product Description&nbsp;: {{products.description}}</p>
			<p>Product Net Price &nbsp;: Please Call for details</p>	
			<p><button @click="addToCart(products)" class="btn light-blue"><i class="material-icons">add_shopping_cart</i></button></p>
		</div>
	</div>	
	<br/><h5>Rating and reviews</h5><hr/>
	<?php echo $html; ?>
	<div>
		<br/><h5>Add Rating and Comment</h5><hr/>
		<form  method="post" action="<?php echo $_SERVER['PHP_SELF'].'?id='.$id.'&&rating_id='.$rating_id; ?>" enctype="multipart/form-data">
			<div class="row">
				<div class="input-field  col l6 m6 s12">
					<input class="white-text" id="rating"  name="rating" type="text" class="validate" value=<?php if ($rating==0){echo "5";}
						else{echo $rating;} ?>> </input>
					<label for="rating">rating</label>
					<?php echo $htmluser; ?>
				</div>
				<div class="input-field  col l6 m6 s12">
					<input class="white-text" id="comments"  name="comments" type="text" class="validate" value="<?php echo $comments; ?>"> </input>
					<label for="comments">comment</label>
				</div>
				<?php 
					if ($rating_id==0){
						echo '<button id="add_comment" name="add_comment" class="waves-effect waves-light btn-small">Add Comment</button>';
					}
					else {
						echo '<button id="save_comment" name="save_comment" class="waves-effect waves-light btn-small">Save Comment</button>';
					}
				?>
			</div>
		</form>
	</div>
</div>
	

<?php include 'footer.php'; ?>  

<script src="scripts/vue.js"></script>
<script src="scripts/axios.min.js"></script>
<script>
	var app = new Vue({
	  el: '#app',
	  data () {
	    return {
	      products: null,
	      errored: false,
		  url:''
	    }
	  },
	  filters: {
			formatPrice(value) {
				    var formatter = new Intl.NumberFormat('en-US', {
			        style: 'currency',
			        currency: 'BDT',
			        minimumFractionDigits: 0
			    });
			    return formatter.format(value);
			}
		},
		methods:{
			addToCart: function (products) {
			// now we have access to the native event
			console.log('product',products);
			// const itmes = {
			// 	id: id,
			// 	location: "Lagos",
			// }
			products.quantity=1;
			window.localStorage.setItem('product'.concat(products.id), JSON.stringify(products));
			showcart();
			// for(var i in obj)
			// alert(i+"--"+obj[i]["firstName"]);
			  },
			printimage:function(img){
				vimg = `<a href="images/uploads/allproducts/${img}" ><img id="prd_img" src="images/uploads/allproducts/${img}" style="width:100%; height:100%; max-width:400px; max-height: 400px; object-fit:fill;"></a>`;
				// vimg =vimg.concat(`<div id="prd_img_zoom" class="img-zoom-result"></div>`);
        		$('#prdimgdiv').html(vimg);
				console.log(`${img}`);
			}
		},
	  mounted () {
	  		url="crud/product_c.php?FunctionType=read&&id=".concat(
			  <?php 
			  //when request come from home page
			  $id = (isset($_GET['id'])) ? $_GET['id'] : '0';
			  echo "'".$id."'";
			  ?>) ;
			  console.log(url);
			axios.get(url)
	        .then(response => {
	            console.log('response:',response.data.products);
	            this.products = response.data.products;
				console.log('this product ',this.products[0].image);
				this.printimage(this.products[0].image);
	        }).catch(err => {
	          console.log(err)
	        });
	      }
	})
</script>
<script type="text/javascript">
	// Register service worker.
	if ('serviceWorker' in navigator) {
	  window.addEventListener('load', () => {
	    navigator.serviceWorker.register('service-worker.js')
	        .then((reg) => {
	          console.log('Service worker registered.', reg);
	        });
	  });
	}
</script>
<script>
	function addrating(id){
		// console.log('usertaing',id);
		document.getElementById('rating').value=id;
		for (i = 1; i <= 5; i++) {
			imgid="urating_id".concat(i);
			// console.log('imgid',imgid);
			if (i>id){
				document.getElementById(imgid).src = "./images/favourite-open.png";
			}
			else{
				document.getElementById(imgid).src = "./images/favourite-filled.png";
			}
		}
		
	}

</script>
<?php include 'endfooter.php'; ?>  